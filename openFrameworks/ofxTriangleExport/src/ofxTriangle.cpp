#include "ofxTriangle.h"

void ofxTriangle::triangulate(ofxCvBlob &cvblob, int resolution, int rdmPoints)
{
    blob = &cvblob;

    int bSize = blob->pts.size();
    float maxi = min(resolution, bSize);

    Delaunay::Point tempP;
    vector< Delaunay::Point > v;

    int i;

    for( i=0; i< maxi; i++)
    {
        int id = (int)( (float)i/maxi*bSize );

        tempP[0] = blob->pts[id].x;
        tempP[1] = blob->pts[id].y;

        v.push_back(tempP);
    }
	
//	if (rdmPoints > 0) {
//		vector<float> xs, ys;
//		for (int j = 0; j < blob->pts.size(); j++) {
//			xs.push_back(blob->pts[j].x);
//			ys.push_back(blob->pts[j].y);
//		}
//		float minx = *min_element(xs.begin(), xs.end());
//		float maxx = *max_element(xs.begin(), xs.end());
//		float miny = *min_element(ys.begin(), ys.end());
//		float maxy = *max_element(ys.begin(), ys.end());
//		for (int k = 0; k < rdmPoints; k++) {
//			int rndx = ofRandom(minx, maxx);
//			int rndy = ofRandom(miny, maxy);
//			ofPoint rndPos = ofPoint(rndx, rndy, 0);
//			if(isPointInsidePolygon(&blob->pts[0], blob->pts.size(), rndPos)) {
//				tempP[0] = rndPos.x;
//				tempP[1] = rndPos.y;
//				v.push_back(tempP);
//			}
//		}
//	}

    delobject = new Delaunay(v);
    delobject->Triangulate();

    //triangles.clear();
    //nTriangles = 0;

    Delaunay::fIterator fit;
    for ( fit = delobject->fbegin(); fit != delobject->fend(); ++fit )
    {
        int pta = delobject->Org(fit);
        int ptb = delobject->Dest(fit);
        int ptc = delobject->Apex(fit);

        int pta_id = (int)( ((float)pta/resolution)*bSize );
        int ptb_id = (int)( ((float)ptb/resolution)*bSize );
        int ptc_id = (int)( ((float)ptc/resolution)*bSize );

        ofPoint tr[3];
        tr[0] = ofPoint(blob->pts[pta_id].x, blob->pts[pta_id].y);
        tr[1] = ofPoint(blob->pts[ptb_id].x, blob->pts[ptb_id].y);
        tr[2] = ofPoint(blob->pts[ptc_id].x, blob->pts[ptc_id].y);

        if( isPointInsidePolygon(&blob->pts[0], blob->pts.size(), getTriangleCenter(tr) ) )
        {
            ofxTriangleData td;
            td.a = ofPoint(tr[0].x, tr[0].y);
            td.b = ofPoint(tr[1].x, tr[1].y);
            td.c = ofPoint(tr[2].x, tr[2].y);

            td.area = delobject->area(fit);

            triangles.push_back(td);
			
            nTriangles++;
        }
    }
	
//	if (rdmPoints > 0 && triangles.size() > 0) {
////		vector<float> xs, ys;
////		for (int j = 0; j < triangles.size(); j++) {
////			ofxTriangleData td = triangles[j];
////			xs.push_back(td.a.x);
////			xs.push_back(td.b.x);
////			xs.push_back(td.c.x);
////			ys.push_back(td.a.y);
////			ys.push_back(td.b.y);
////			ys.push_back(td.c.y);		
////		}
////		float minx = *min_element(xs.begin(), xs.end());
////		float maxx = *max_element(xs.begin(), xs.end());
////		float miny = *min_element(ys.begin(), ys.end());
////		float maxy = *max_element(ys.begin(), ys.end());
//		for (int j = 0; j < rdmPoints; j++) {
//			
//			int rdmTriIdx = ofRandom(0, triangles.size());
//			ofxVec2f corners[3] = {triangles[rdmTriIdx].a, triangles[rdmTriIdx].b, triangles[rdmTriIdx].c};
//			float dista = corners[0].distance(corners[1]);
//			float distb = corners[1].distance(corners[2]);
//			float distc = corners[2].distance(corners[0]);
//			float s = (dista + distb + distc);
//			float ss = sqrt(s * (s - dista) * (s - distb) * (s - distc));	
//			
//			vector<float> xs, ys;
//			xs.push_back(corners[0].x);
//			xs.push_back(corners[1].x);
//			xs.push_back(corners[2].x);
//			ys.push_back(corners[0].y);
//			ys.push_back(corners[1].y);
//			ys.push_back(corners[2].y);		
//
//			float minx = *min_element(xs.begin(), xs.end());
//			float maxx = *max_element(xs.begin(), xs.end());
//			float miny = *min_element(ys.begin(), ys.end());
//			float maxy = *max_element(ys.begin(), ys.end());			
//			
//			ofPoint tr[3];
//			tr[0] = ofPoint(ofRandom(minx, maxx), ofRandom(miny, maxy));
//			tr[1] = ofPoint(ofRandom(minx, maxx), ofRandom(miny, maxy));
//			tr[2] = ofPoint(ofRandom(minx, maxx), ofRandom(miny, maxy));
//			//if(isPointInsidePolygon(&blob->pts[0], blob->pts.size(), getTriangleCenter(tr))) {
//				ofxTriangleData td;
//				td.a = ofPoint(tr[0].x, tr[0].y);
//				td.b = ofPoint(tr[1].x, tr[1].y);
//				td.c = ofPoint(tr[2].x, tr[2].y);
//				triangles.push_back(td);
//				nTriangles++;			
//			//}
//		}
//	}
	
    delete delobject;
}

void ofxTriangle::triangulate(ofTTFContour &fontBlob, int resolution, int rdmPoints)
{
    fBlob = &fontBlob;
	
    int bSize = fBlob->pts.size();
    float maxi = min(resolution, bSize);
	
    Delaunay::Point tempP;
    vector< Delaunay::Point > v;
	
    int i;
	
    for( i=0; i< maxi; i++)
    {
        //int id = (int)( (float)i/maxi*bSize );
		
        tempP[0] = fBlob->pts[i].x;
        tempP[1] = fBlob->pts[i+1].y;
		//i++;
		
        v.push_back(tempP);
    }
	
    delobject = new Delaunay(v);
    delobject->Triangulate();
	
    //triangles.clear();
    //nTriangles = 0;
	
    Delaunay::fIterator fit;
    for ( fit = delobject->fbegin(); fit != delobject->fend(); ++fit )
    {
        int pta = delobject->Org(fit);
        int ptb = delobject->Dest(fit);
        int ptc = delobject->Apex(fit);
		
        int pta_id = (int)( ((float)pta/resolution)*bSize );
        int ptb_id = (int)( ((float)ptb/resolution)*bSize );
        int ptc_id = (int)( ((float)ptc/resolution)*bSize );
		
        ofPoint tr[3];
        tr[0] = ofPoint(fBlob->pts[pta_id].x, fBlob->pts[pta_id].y);
        tr[1] = ofPoint(fBlob->pts[ptb_id].x, fBlob->pts[ptb_id].y);
        tr[2] = ofPoint(fBlob->pts[ptc_id].x, fBlob->pts[ptc_id].y);
		
        if( isPointInsidePolygon(&fBlob->pts[0], fBlob->pts.size(), getTriangleCenter(tr) ) )
        {
            ofxTriangleData td;
            td.a = ofPoint(tr[0].x, tr[0].y);
            td.b = ofPoint(tr[1].x, tr[1].y);
            td.c = ofPoint(tr[2].x, tr[2].y);
			
            td.area = delobject->area(fit);
			
            triangles.push_back(td);
			
            nTriangles++;
        }
    }
	
    delete delobject;
}

void ofxTriangle::clear()
{
    triangles.clear();
    nTriangles = 0;
}

void ofxTriangle::addRdmPoint(vector<Delaunay::Point> * v)
{
    
    Delaunay::Point tempP;

    int px = ofRandom( blob->boundingRect.x, blob->boundingRect.width );
    int py = ofRandom( blob->boundingRect.y, blob->boundingRect.height );

    while ( !isPointInsidePolygon( &blob->pts[0], blob->pts.size(), ofPoint( px, py ) ) )
    {
        px = ofRandom( blob->boundingRect.x, blob->boundingRect.width );
        py = ofRandom( blob->boundingRect.y, blob->boundingRect.height );

        cout << px << " " << py << endl;
    }

    tempP[0] = px;
    tempP[1] = py;
    v->push_back(tempP);
    
}

ofPoint ofxTriangle::getTriangleCenter(ofPoint *tr)
{
    float c_x = (tr[0].x + tr[1].x + tr[2].x) / 3;
    float c_y = (tr[0].y + tr[1].y + tr[2].y) / 3;

    return ofPoint(c_x, c_y);
}

bool ofxTriangle::isPointInsidePolygon(ofPoint *polygon,int N, ofPoint p)
{
    int counter = 0;
    int i;
    double xinters;
    ofPoint p1,p2;

    p1 = polygon[0];

    for (i=1;i<=N;i++)
    {
        p2 = polygon[i % N];
        if (p.y > MIN(p1.y,p2.y)) {
            if (p.y <= MAX(p1.y,p2.y)) {
                if (p.x <= MAX(p1.x,p2.x)) {
                    if (p1.y != p2.y) {
                        xinters = (p.y-p1.y)*(p2.x-p1.x)/(p2.y-p1.y)+p1.x;
                        if (p1.x == p2.x || p.x <= xinters)
                            counter++;
                    }
                }
            }
        }
        p1 = p2;
    }

  if (counter % 2 == 0)
    return false;
  else
    return true;
}

void ofxTriangle::draw(float x, float y)
{
    ofPushMatrix();
    ofTranslate(x, y, 0);
        draw();
    ofPopMatrix();
}

void ofxTriangle::draw()
{
    //ofFill();

    for (int i=0; i<nTriangles; i++)
    {
        ofSetColor(ofRandom(0, 0xffffff));
        ofTriangle( triangles[i].a.x, triangles[i].a.y,
                    triangles[i].b.x, triangles[i].b.y,
                    triangles[i].c.x, triangles[i].c.y);
    }

}
