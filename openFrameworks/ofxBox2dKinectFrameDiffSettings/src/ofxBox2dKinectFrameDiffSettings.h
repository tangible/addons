/*
 *  ofxBox2dKinectFrameDiffSettings.h
 *  VoiceIt
 *
 *  Created by Pat Long (plong0) on 11-02-16.
 *  Copyright 2011 Tangible Interaction. All rights reserved.
 *
 */
#pragma once

#include "ofxSimpleGuiToo.h"
#include "ofxBox2d.h"
#include "ofxKinect.h"
#include "ofxKinectCalibrator.h"
#include "ofxBox2dImage.h"
#include "ofxBox2dFrameDiff.h"
#include "ofxKinectFrameDiff.h"
#include "ofxScalableImage.h"

#define DEFAULT_B2D_KFD_FRAMEDIFF_XMLFILE	"box2dKinectFrameDiff_settings.xml"
#define DEFAULT_B2D_KFD_FRAMEDIFF_XMLROOT	"B2DFD"
#define DEFAULT_B2D_KFD_FRAMEDIFF_MINSAVE	250

class ofxBox2dKinectFrameDiffSettings{
	protected:
		ofxBox2d*				box2d;
		ofxBox2dImage*			b2dbg;
		ofxBox2dFrameDiff*		b2dfd;
		ofxKinect*				kinect;
		ofxKinectCalibrator*	kc;
		ofxKinectFrameDiff*		kfd;
	
		ofPoint b2dGravity, b2dGravity_set;
		float	b2dShape_Density, b2dShape_Density_set;
		float	b2dShape_Restitution, b2dShape_Restitution_set;
		float	b2dShape_Friction, b2dShape_Friction_set;
	
		bool	b2dbgOffsetInvertX, b2dbgOffsetInvertX_set;
		bool	b2dbgOffsetInvertY, b2dbgOffsetInvertY_set;
		ofPoint b2dbgOffset, b2dbgOffset_set;
		ofPoint b2dbgScale, b2dbgScale_set;
		bool	b2dbg_reset;
	
		int		b2dfd_triRes, b2dfd_triRes_set;
		float	b2dfdCP_Density, b2dfdCP_Density_set;
		float	b2dfdCP_Restitution, b2dfdCP_Restitution_set;
		float	b2dfdCP_Friction, b2dfdCP_Friction_set;
	
		int		b2dfd_optFlowSize, b2dfd_optFlowSize_set;
		float	b2dfd_optFlowForceMax, b2dfd_optFlowForceMax_set;
		float	b2dfd_optFlowForceMultiplier, b2dfd_optFlowForceMultiplier_set;
	
		ofRectangle kc_calibration;
		bool	kc_mirrorHorizontal, kc_mirrorHorizontal_set;
		bool	kc_mirrorVertical, kc_mirrorVertical_set;
		bool	kc_drawDepth, kc_drawDepth_set;
		bool	kc_reset;
	
		bool	kfd_bThreshWithOpenCV, kfd_bThreshWithOpenCV_set;
		int		kfd_nearThreshold, kfd_nearThreshold_set;
		int		kfd_farThreshold, kfd_farThreshold_set;
	
		bool	wakeupShapes;
	
		bool	drawBackgroundFD;
		bool	drawBackgroundFDC;
		bool	drawBackgroundKC;
		bool	drawBackgroundKD;
		bool	drawBackgroundKS;
	
		string	xmlFileName, xmlRootName;
		int		lastSaveTime, minSaveDelay;
		bool	doSave;
	
		void update_b2dGravity();
		void update_b2dShapeDensity();
		void update_b2dShapeFriction();
		void update_b2dShapeRestitution();
	
		void update_b2dbgShapeData();
		void update_b2dbgOffset(bool doRefresh=true);
		void update_b2dbgScale(bool doRefresh=true);
		void refresh_b2dbgShapes();
		void reset_b2dbg();
	
		void update_b2dfdCP_Physics();
		void update_b2dfdTriRes();	
		void update_b2dfdOptFlowSize();
		void update_b2dfdOptFlowForceMax();
		void update_b2dfdOptFlowForceMultiplier();
	
		void update_kinectThresholdOpenCV();
		void update_kinectThresholdNear();
		void update_kinectThresholdFar();
	
		void update_kinectCalibration();
		void update_kinectMirror();
		void update_kinectPreview();
		void reset_kinectCalibration();
	
		bool isOnBox2dPage();
		bool isOnDrawingPage();
		bool isOnKinectPage();
		bool isOnSettingsPage();
	
		virtual void readXML(ofxXmlSettings& xml);
		virtual void writeXML(ofxXmlSettings& xml);
		
	public:
		ofxBox2dKinectFrameDiffSettings();
		~ofxBox2dKinectFrameDiffSettings();
	
		virtual void initGUI(bool doLoad=true);
	
		virtual void draw();
		virtual void update();
	
		bool shouldWakeupShapes();
	
		void updateBox2dSettings();
		void updateBox2dBackgroundSettings();
		void updateBox2dFrameDiffSettings();
		void updateKinectSettings();
		void updateKinectCalibratorSettings();
		void updateKinectFrameDiffSettings();
	
		bool toggleGUI();
	
		float getShapeDensity();
		float getShapeFriction();
		float getShapeRestitution();
	
		bool shouldDrawBackgroundOutline();
		bool shouldDrawContours();
		bool shouldDrawFrameDifference();
		bool shouldDrawKinectColour();
		bool shouldDrawKinectDepth();
		bool shouldDrawKinectShapes();
	
		void setBox2d(ofxBox2d* box2d);
		void setBox2dBackground(ofxBox2dImage* b2dbg);
		void setBox2dFrameDifferencer(ofxBox2dFrameDiff* b2dfd);
		void setKinect(ofxKinect* kinect);
		void setKinectCalibrator(ofxKinectCalibrator* kc);
		void setKinectFrameDifferencer(ofxKinectFrameDiff* kfd);
		
		void loadFromXML();
		void saveToXML();
		void setXmlFileName(string xmlFileName=DEFAULT_B2D_KFD_FRAMEDIFF_XMLFILE, string xmlRootName=DEFAULT_B2D_KFD_FRAMEDIFF_XMLROOT);
		void setXmlMinSaveDelay(int minSaveDelay=DEFAULT_B2D_KFD_FRAMEDIFF_MINSAVE);
	
		virtual void mouseMoved(int x, int y );
		virtual void mouseDragged(int x, int y, int button);
		virtual void mousePressed(int x, int y, int button);
		virtual void mouseReleased(int x, int y, int button);
};
