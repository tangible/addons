/*
 *  ofxBox2dKinectFrameDiffSettings.cpp
 *  VoiceIt
 *
 *  Created by Pat Long (plong0) on 11-02-16.
 *  Copyright 2011 Tangible Interaction. All rights reserved.
 *
 */

#include "ofxBox2dKinectFrameDiffSettings.h"

ofxBox2dKinectFrameDiffSettings::ofxBox2dKinectFrameDiffSettings(){
	this->setBox2d(NULL);
	this->setBox2dBackground(NULL);
	this->setBox2dFrameDifferencer(NULL);
	this->setKinect(NULL);
	this->setKinectCalibrator(NULL);
	this->setKinectFrameDifferencer(NULL);
	
	b2dGravity.x = b2dGravity_set.x = 0;
	b2dGravity.y = b2dGravity_set.y = 0;
	
	b2dbgOffsetInvertX = b2dbgOffsetInvertX_set = false;
	b2dbgOffsetInvertY = b2dbgOffsetInvertY_set = false;
	b2dbgOffset.x = b2dbgOffset_set.x = 0.0;
	b2dbgOffset.y = b2dbgOffset_set.y = 0.0;
	b2dbgScale.x = b2dbgScale_set.x = 1.0;
	b2dbgScale.y = b2dbgScale_set.y = 1.0;
	b2dbg_reset = false;
	
	b2dShape_Density = b2dShape_Density_set = 0.0;
	b2dShape_Restitution = b2dShape_Restitution_set = 0.0;
	b2dShape_Friction = b2dShape_Friction_set = 0.0;
	
	b2dfd_triRes = b2dfd_triRes_set = 50;
	b2dfdCP_Density = b2dfdCP_Density_set = 0.0;
	b2dfdCP_Restitution = b2dfdCP_Restitution_set = 0.0;
	b2dfdCP_Friction = b2dfdCP_Friction_set = 0.0;
	b2dfd_optFlowSize = b2dfd_optFlowSize_set = 24;
	b2dfd_optFlowForceMax = b2dfd_optFlowForceMax_set = 10.0;
	b2dfd_optFlowForceMultiplier = b2dfd_optFlowForceMultiplier_set = 2.0;
	
	kc_mirrorHorizontal = kc_mirrorHorizontal_set = false;
	kc_mirrorVertical = kc_mirrorVertical_set = false;
	kc_drawDepth = kc_drawDepth_set = false;
	kc_reset = false;
	kc_calibration.x = kc_calibration.y = 0.0;
	kc_calibration.width = kc_calibration.height = 1.0;
	
	kfd_bThreshWithOpenCV = kfd_bThreshWithOpenCV_set = true;
	kfd_nearThreshold = kfd_nearThreshold_set = 0;
	kfd_farThreshold = kfd_farThreshold_set = 150;
	
	drawBackgroundFD = false;
	drawBackgroundFDC = false;
	drawBackgroundKC = false;
	drawBackgroundKD = false;
	drawBackgroundKS = true;
	
	doSave = false;
	lastSaveTime = -1;
	setXmlMinSaveDelay();
	setXmlFileName();
	
	wakeupShapes = false;
}

ofxBox2dKinectFrameDiffSettings::~ofxBox2dKinectFrameDiffSettings(){
}

void ofxBox2dKinectFrameDiffSettings::initGUI(bool doLoad){
	gui.addPage("Kinect");
	gui.addTitle("Frame Differencer");
	gui.addToggle("OpenCV Threshold", kfd_bThreshWithOpenCV);
	gui.addSlider("Near Threshold", kfd_nearThreshold, 0, 255);
	gui.addSlider("Far Threshold", kfd_farThreshold, 0, 255);
	gui.addSlider("Triangle Resolution", b2dfd_triRes, 25, 125);
	
	gui.addTitle("Optical Flow");
	gui.addSlider("Region Size", b2dfd_optFlowSize, 5, 100);
	gui.addSlider("Force Multiplier", b2dfd_optFlowForceMultiplier, 0.0, 25.0);
	gui.addSlider("Force Max", b2dfd_optFlowForceMax, 0.0, 100.0);
	
	gui.addTitle("Input Image").setNewColumn(true);
	gui.addToggle("Mirror Horizontal", kc_mirrorHorizontal);
	gui.addToggle("Mirror Vertical", kc_mirrorVertical);
	
	gui.addTitle("Calibration");
	gui.addToggle("Depth Preview", kc_drawDepth);
	gui.addButton("Reset Calibration", kc_reset);
	
	gui.addPage("Box2D");
	gui.addTitle("Background");
	gui.addToggle("Offset X Invert", b2dbgOffsetInvertX);
	gui.addSlider("Offset X", b2dbgOffset.x, 0.0, ofGetWidth());
	gui.addToggle("Offset Y Invert", b2dbgOffsetInvertY);
	gui.addSlider("Offset Y", b2dbgOffset.y, 0.0, ofGetHeight());
	gui.addSlider("Scale X", b2dbgScale.x, 0.0, 2.0);
	gui.addSlider("Scale Y", b2dbgScale.y, 0.0, 2.0);
	gui.addButton("Reset Background", b2dbg_reset);
	
	gui.addTitle("Environment").setNewColumn(true);
	gui.addSlider("Gravity X", b2dGravity.x, -10.0, 10.0);
	gui.addSlider("Gravity Y", b2dGravity.y, -10.0, 10.0);
	
	gui.addTitle("Box2D Shapes").setNewColumn(true);
	gui.addSlider("B2D Density", b2dShape_Density, 0.0, 25.0);
	gui.addSlider("B2D Bounciness", b2dShape_Restitution, 0.0, 1.0);
	gui.addSlider("B2D Friction", b2dShape_Friction, 0.0, 5.0);
	
	gui.addTitle("Frame Differencer Shapes").setNewColumn(true);
	gui.addSlider("FD Density", b2dfdCP_Density, 0.0, 25.0);
	gui.addSlider("FD Bounciness", b2dfdCP_Restitution, 0.0, 1.0);
	gui.addSlider("FD Friction", b2dfdCP_Friction, 0.0, 5.0);

	gui.addPage("Drawing");
	gui.addTitle("Background");
	gui.addToggle("Kinect Shapes", drawBackgroundKS);
	gui.addToggle("Frame Difference", drawBackgroundFD);
	gui.addToggle("Contours", drawBackgroundFDC);
	gui.addToggle("Colour Image", drawBackgroundKC);
	gui.addToggle("Depth Map", drawBackgroundKD);
	
	if(doLoad)
		gui.loadFromXML();
}

void ofxBox2dKinectFrameDiffSettings::readXML(ofxXmlSettings& xml){
	if(xml.tagExists("kinect_calibration")){
		xml.pushTag("kinect_calibration");
		
		kc_calibration.x = xml.getValue("x", kc_calibration.x);
		kc_calibration.y = xml.getValue("y", kc_calibration.y);
		kc_calibration.width = xml.getValue("width", kc_calibration.width);
		kc_calibration.height = xml.getValue("height", kc_calibration.height);
		
		xml.popTag();
	}
}

void ofxBox2dKinectFrameDiffSettings::writeXML(ofxXmlSettings& xml){
	if(!xml.tagExists("kinect_calibration"))
		xml.addTag("kinect_calibration");
	
	xml.pushTag("kinect_calibration");
	
	xml.setValue("x", kc_calibration.x);
	xml.setValue("y", kc_calibration.y);
	xml.setValue("width", kc_calibration.width);
	xml.setValue("height", kc_calibration.height);
	
	xml.popTag();
}

void ofxBox2dKinectFrameDiffSettings::loadFromXML(){
	ofxXmlSettings xml;
	if(xml.loadFile(this->xmlFileName)){
		bool inRoot = false;
				
		if(xml.tagExists(this->xmlRootName)){
			xml.pushTag(this->xmlRootName);
			inRoot = true;
		}
		
		readXML(xml);
		
		if(inRoot){
			xml.popTag();
			inRoot = false;
		}
	}
}

void ofxBox2dKinectFrameDiffSettings::saveToXML(){
	int cTime = ofGetElapsedTimeMillis();
	if(lastSaveTime == -1 || (cTime - lastSaveTime) >= this->minSaveDelay){
		ofxXmlSettings xml;
		
		bool loaded = xml.loadFile(this->xmlFileName);
		bool inRoot = false;
		if(!xml.tagExists(this->xmlRootName))
			xml.addTag(this->xmlRootName);
		
		xml.pushTag(this->xmlRootName);
		inRoot = true;
		
		writeXML(xml);
		
		if(inRoot){
			xml.popTag();
			inRoot = false;
		}
		
		xml.saveFile(this->xmlFileName);
		doSave = false;
		lastSaveTime = cTime;
	}
}

void ofxBox2dKinectFrameDiffSettings::setXmlFileName(string xmlFileName, string xmlRootName){
	this->xmlFileName = xmlFileName;
	this->xmlRootName = xmlRootName;
	if(ofGetFrameNum() > 0)
		this->loadFromXML();
}

void ofxBox2dKinectFrameDiffSettings::setXmlMinSaveDelay(int minSaveDelay){
	this->minSaveDelay = minSaveDelay;
}

void ofxBox2dKinectFrameDiffSettings::draw(){
	gui.draw();
	
	if(isOnKinectPage()){
		if(kc != NULL){
			if(kfd != NULL){
				if(kc->getDrawMode() == KC_COLOR)
					kfd->drawColor(547, 437, 320, 240); //screen.width-340, 200, 320, 240);
				else if(kc->getDrawMode() == KC_DEPTH)
					kfd->drawDepth(547, 437, 320, 240);
				
				ofSetColor(255, 128, 0);
				ofNoFill();
				ofRect(547, 437, 320, 240);
				ofFill();
				ofSetColor(255, 255, 255);
				
				if(b2dfd != NULL)
					b2dfd->drawContours(547, 437, 320, 240); //screen.width-340, 460, 320, 240);
			}
			
			kc->draw(467, 115, 400, 300);
		}
	}
	

}

void ofxBox2dKinectFrameDiffSettings::update(){
	if(b2dGravity.x != 	b2dGravity_set.x || b2dGravity.y != b2dGravity_set.y){
		update_b2dGravity();
		wakeupShapes = true;
	}
	
	if(b2dShape_Density != b2dShape_Density_set){
		update_b2dShapeDensity();
		wakeupShapes = true;
	}
	
	if(b2dShape_Friction != b2dShape_Friction_set){
		update_b2dShapeFriction();
		wakeupShapes = true;
	}
	
	if(b2dShape_Restitution != b2dShape_Restitution_set){
		update_b2dShapeRestitution();
		wakeupShapes = true;
	}
	
	if(b2dbg_reset)
		reset_b2dbg();
	
	bool refreshBGshapes = false;
	if(b2dbgOffset.x != b2dbgOffset_set.x || b2dbgOffset.y != b2dbgOffset_set.y || b2dbgOffsetInvertX != b2dbgOffsetInvertX_set || b2dbgOffsetInvertY != b2dbgOffsetInvertY_set){
		update_b2dbgOffset(false);
		refreshBGshapes = true;
	}
	
	if(b2dbgScale.x != b2dbgScale_set.x || b2dbgScale.y != b2dbgScale_set.y){
		update_b2dbgScale(false);
		refreshBGshapes = true;
	}
		
	if(refreshBGshapes){
		refresh_b2dbgShapes();
		refreshBGshapes = false;
	}
	
	if(b2dfdCP_Density != b2dfdCP_Density_set || b2dfdCP_Restitution != b2dfdCP_Restitution_set || b2dfdCP_Friction != b2dfdCP_Friction_set)
		update_b2dfdCP_Physics();
	
	if(b2dfd_triRes != b2dfd_triRes_set)
		update_b2dfdTriRes();
	
	if(b2dfd_optFlowSize != b2dfd_optFlowSize_set)
		update_b2dfdOptFlowSize();
	
	if(b2dfd_optFlowForceMax != b2dfd_optFlowForceMax_set)
		update_b2dfdOptFlowForceMax();
	
	if(b2dfd_optFlowForceMultiplier != b2dfd_optFlowForceMultiplier_set)
		update_b2dfdOptFlowForceMultiplier();
	
	if(kc_mirrorHorizontal != kc_mirrorHorizontal_set || kc_mirrorVertical != kc_mirrorVertical_set)
		update_kinectMirror();
	
	if(kc_drawDepth != kc_drawDepth_set)
		update_kinectPreview();
	
	if(kc_reset)
		reset_kinectCalibration();
	
	if(kfd_bThreshWithOpenCV != kfd_bThreshWithOpenCV_set)
		update_kinectThresholdOpenCV();
	
	if(kfd_nearThreshold != kfd_nearThreshold_set)
		update_kinectThresholdNear();
		
	if(kfd_farThreshold != kfd_farThreshold_set)
		update_kinectThresholdFar();
	
	
	if(kc != NULL && kc->isScaleChanged()){
		kc_calibration = kc->getScaledNormals();
		doSave = true;
	}
	
	if(doSave)
		saveToXML();
}

bool ofxBox2dKinectFrameDiffSettings::shouldWakeupShapes(){
	bool result = wakeupShapes;
	wakeupShapes = false;
	return result;
}

void ofxBox2dKinectFrameDiffSettings::update_b2dGravity(){
	if(box2d != NULL){
		box2d->setGravity(b2dGravity.x, b2dGravity.y);
		b2dGravity_set.x = b2dGravity.x;
		b2dGravity_set.y = b2dGravity.y;
	}
}

void ofxBox2dKinectFrameDiffSettings::update_b2dShapeDensity(){
	b2dShape_Density_set = b2dShape_Density;
}

void ofxBox2dKinectFrameDiffSettings::update_b2dShapeFriction(){
	b2dShape_Friction_set = b2dShape_Friction;
}

void ofxBox2dKinectFrameDiffSettings::update_b2dShapeRestitution(){
	b2dShape_Restitution_set = b2dShape_Restitution;
}

void ofxBox2dKinectFrameDiffSettings::update_b2dbgShapeData(){
	if(b2dbg != NULL){
		vector<ofxBox2dLine*> bgShapes = b2dbg->getSceneShapes();
		for(int i=0; i < bgShapes.size(); i++){
			if(bgShapes[i] != NULL && bgShapes[i]->body != NULL)
				bgShapes[i]->body->SetUserData(new ofxBox2dPVUserData(STATIC));
		}
	}
}

void ofxBox2dKinectFrameDiffSettings::update_b2dbgOffset(bool doRefresh){
	if(b2dbg != NULL){
		b2dbg->setOffset(b2dbgOffset.x * (b2dbgOffsetInvertX?-1.0:1.0), b2dbgOffset.y * (b2dbgOffsetInvertY?-1.0:1.0), doRefresh);
		if(doRefresh) update_b2dbgShapeData();
		b2dbgOffset_set.x = b2dbgOffset.x;
		b2dbgOffset_set.y = b2dbgOffset.y;
		b2dbgOffsetInvertX_set = b2dbgOffsetInvertX;
		b2dbgOffsetInvertY_set = b2dbgOffsetInvertY;
	}
}

void ofxBox2dKinectFrameDiffSettings::update_b2dbgScale(bool doRefresh){
	if(b2dbg != NULL){
		b2dbg->setScale(b2dbgScale.x, b2dbgScale.y, doRefresh);
		if(doRefresh) update_b2dbgShapeData();
		b2dbgScale_set.x = b2dbgScale.x;
		b2dbgScale_set.y = b2dbgScale.y;
	}
}

void ofxBox2dKinectFrameDiffSettings::refresh_b2dbgShapes(){
	this->b2dbg->refreshShapes();
	update_b2dbgShapeData();
}

void ofxBox2dKinectFrameDiffSettings::reset_b2dbg(){
	b2dbgScale.x = 1.0;
	b2dbgScale.y = 1.0;
	b2dbgOffset.x = 0.0;
	b2dbgOffset.y = 0.0;
}

void ofxBox2dKinectFrameDiffSettings::update_b2dfdCP_Physics(){
	if(b2dfd != NULL){
		b2dfd->setComplexPolyPhysics(b2dfdCP_Density, b2dfdCP_Restitution, b2dfdCP_Friction);
	
		b2dfdCP_Density_set = b2dfdCP_Density;
		b2dfdCP_Restitution_set = b2dfdCP_Restitution;
		b2dfdCP_Friction_set = b2dfdCP_Friction;
	}
}

void ofxBox2dKinectFrameDiffSettings::update_b2dfdOptFlowSize(){
	if(b2dfd != NULL){
		b2dfd->setCLOptFlowSize(b2dfd_optFlowSize);
		b2dfd_optFlowSize_set = b2dfd_optFlowSize;
	}
}

void ofxBox2dKinectFrameDiffSettings::update_b2dfdOptFlowForceMax(){
	if(b2dfd != NULL){
		b2dfd->setCLOptFlowForceMax(b2dfd_optFlowForceMax);
		b2dfd_optFlowForceMax_set = b2dfd_optFlowForceMax;
	}
}

void ofxBox2dKinectFrameDiffSettings::update_b2dfdOptFlowForceMultiplier(){
	if(b2dfd != NULL){
		b2dfd->setCLOptFlowForceMultiplier(b2dfd_optFlowForceMultiplier);
		b2dfd_optFlowForceMultiplier_set = b2dfd_optFlowForceMultiplier;
	}
}

void ofxBox2dKinectFrameDiffSettings::update_b2dfdTriRes(){
	if(b2dfd != NULL){
		b2dfd->setTriRes(b2dfd_triRes);
		b2dfd_triRes_set = b2dfd_triRes;
	}
}

void ofxBox2dKinectFrameDiffSettings::update_kinectThresholdOpenCV(){
	if(kfd != NULL){
		kfd->setThresholdWithOpenCV(kfd_bThreshWithOpenCV);
		kfd_bThreshWithOpenCV_set = kfd_bThreshWithOpenCV;
	}
}

void ofxBox2dKinectFrameDiffSettings::update_kinectThresholdNear(){
	if(kfd != NULL){
		if(kfd_nearThreshold > kfd_farThreshold)
			kfd_nearThreshold = kfd_farThreshold;
		
		kfd->setThresholdNear(kfd_nearThreshold);
		kfd_nearThreshold_set = kfd_nearThreshold;
	}
}

void ofxBox2dKinectFrameDiffSettings::update_kinectThresholdFar(){
	if(kfd != NULL){
		if(kfd_farThreshold < kfd_nearThreshold)
			kfd_farThreshold = kfd_nearThreshold;
		
		kfd->setThresholdFar(kfd_farThreshold);
		kfd_farThreshold_set = kfd_farThreshold;
	}
}

void ofxBox2dKinectFrameDiffSettings::update_kinectCalibration(){
	if(kc != NULL){
		kc->setScaledSize(kc_calibration.x, kc_calibration.y, kc_calibration.width, kc_calibration.height);
	}
}

void ofxBox2dKinectFrameDiffSettings::update_kinectMirror(){
	if(kc != NULL){
		kc->setMirror(kc_mirrorHorizontal, kc_mirrorVertical);
		kc_mirrorHorizontal_set = kc_mirrorHorizontal;
		kc_mirrorVertical_set = kc_mirrorVertical;
	}
}

void ofxBox2dKinectFrameDiffSettings::update_kinectPreview(){
	if(kc != NULL){
		if(kc_drawDepth)
			kc->setDrawMode(KC_DEPTH);
		else
			kc->setDrawMode(KC_COLOR);
		kc_drawDepth_set = kc_drawDepth;
	}
}

void ofxBox2dKinectFrameDiffSettings::reset_kinectCalibration(){
	if(kc != NULL)
		kc->resetScale();
}

void ofxBox2dKinectFrameDiffSettings::updateBox2dSettings(){
	update_b2dGravity();
	update_b2dShapeDensity();
	update_b2dShapeFriction();
	update_b2dShapeRestitution();
}

void ofxBox2dKinectFrameDiffSettings::updateBox2dBackgroundSettings(){
	update_b2dbgOffset(false);
	update_b2dbgScale(true);
}

void ofxBox2dKinectFrameDiffSettings::updateBox2dFrameDiffSettings(){
	update_b2dfdCP_Physics();
	update_b2dfdTriRes();
	update_b2dfdOptFlowSize();
	update_b2dfdOptFlowForceMax();
	update_b2dfdOptFlowForceMultiplier();
}

void ofxBox2dKinectFrameDiffSettings::updateKinectSettings(){
}

void ofxBox2dKinectFrameDiffSettings::updateKinectCalibratorSettings(){
	update_kinectCalibration();
	update_kinectPreview();
	update_kinectMirror();
}

void ofxBox2dKinectFrameDiffSettings::updateKinectFrameDiffSettings(){
	update_kinectThresholdOpenCV();
	update_kinectThresholdNear();
	update_kinectThresholdFar();
}

bool ofxBox2dKinectFrameDiffSettings::isOnBox2dPage(){
	return (gui.isOn() && gui.currentPage().name == "Box2D");
}

bool ofxBox2dKinectFrameDiffSettings::isOnDrawingPage(){
	return (gui.isOn() && gui.currentPage().name == "Drawing");
}

bool ofxBox2dKinectFrameDiffSettings::isOnKinectPage(){
	return (gui.isOn() && gui.currentPage().name == "Kinect");
}

bool ofxBox2dKinectFrameDiffSettings::isOnSettingsPage(){
	return (gui.isOn() && gui.currentPage().name == "SETTINGS");
}

bool ofxBox2dKinectFrameDiffSettings::toggleGUI(){
	if(gui.isOn())
		gui.hide();
	else
		gui.show();
	return gui.isOn();
}

float ofxBox2dKinectFrameDiffSettings::getShapeDensity(){
	return b2dShape_Density;
}

float ofxBox2dKinectFrameDiffSettings::getShapeFriction(){
	return b2dShape_Friction;
}

float ofxBox2dKinectFrameDiffSettings::getShapeRestitution(){
	return b2dShape_Restitution;
}

bool ofxBox2dKinectFrameDiffSettings::shouldDrawBackgroundOutline(){
	return (this->isOnBox2dPage());
}

bool ofxBox2dKinectFrameDiffSettings::shouldDrawContours(){
	return drawBackgroundFDC;
}

bool ofxBox2dKinectFrameDiffSettings::shouldDrawFrameDifference(){
	return drawBackgroundFD;
}

bool ofxBox2dKinectFrameDiffSettings::shouldDrawKinectColour(){
	return drawBackgroundKC;
}

bool ofxBox2dKinectFrameDiffSettings::shouldDrawKinectDepth(){
	return drawBackgroundKD;
}

bool ofxBox2dKinectFrameDiffSettings::shouldDrawKinectShapes(){
	return drawBackgroundKS;
}

void ofxBox2dKinectFrameDiffSettings::setBox2d(ofxBox2d* box2d){
	this->box2d = box2d;
	updateBox2dSettings();
}

void ofxBox2dKinectFrameDiffSettings::setBox2dBackground(ofxBox2dImage* b2dbg){
	this->b2dbg = b2dbg;
	updateBox2dBackgroundSettings();
}

void ofxBox2dKinectFrameDiffSettings::setBox2dFrameDifferencer(ofxBox2dFrameDiff* b2dfd){
	this->b2dfd = b2dfd;
	updateBox2dFrameDiffSettings();
}

void ofxBox2dKinectFrameDiffSettings::setKinect(ofxKinect* kinect){
	this->kinect = kinect;
	updateKinectSettings();
}

void ofxBox2dKinectFrameDiffSettings::setKinectCalibrator(ofxKinectCalibrator* kc){
	this->kc = kc;
	updateKinectCalibratorSettings();
}

void ofxBox2dKinectFrameDiffSettings::setKinectFrameDifferencer(ofxKinectFrameDiff* kfd){
	this->kfd = kfd;
	updateKinectFrameDiffSettings();
}

void ofxBox2dKinectFrameDiffSettings::mouseMoved(int x, int y ){
	if(this->kc != NULL && isOnKinectPage()){
		this->kc->mouseMoved(x, y);
	}
}

void ofxBox2dKinectFrameDiffSettings::mouseDragged(int x, int y, int button){
	if(this->kc != NULL && isOnKinectPage()){
		this->kc->mouseDragged(x, y, button);
	}
}

void ofxBox2dKinectFrameDiffSettings::mousePressed(int x, int y, int button){
	if(this->kc != NULL && isOnKinectPage()){
		this->kc->mousePressed(x, y, button);
	}
}

void ofxBox2dKinectFrameDiffSettings::mouseReleased(int x, int y, int button){
	if(this->kc != NULL && isOnKinectPage()){
		this->kc->mouseReleased(x, y, button);
	}
}
