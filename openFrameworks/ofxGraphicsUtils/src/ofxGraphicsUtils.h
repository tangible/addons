/*
 *  ofxGraphicsUtils
 *
 *  An empty addon.
 *  
 *  Created by Pat Long (plong0) on 16/06/10.
 *  Copyright 2010 Spiral Sense Media. All rights reserved.
 *
 */
#ifndef _OFX_GRAPHICS_UTILS
#define _OFX_GRAPHICS_UTILS

#include "ofMain.h"

#define OFX_GRAPHICS_UTILS_CIRC_RESOLUTION	360

void ofxCircleSlice(float x,float y, float radius, float lowAngle, float highAngle, bool radians=false, bool closed=false, bool adjustRes=true);

void ofxSetupCircle(int res=OFX_GRAPHICS_UTILS_CIRC_RESOLUTION);
void ofxSetCircleResolution(int res);

void ofSetHexColor( int hexColor ); // hex, like web 0xFF0033;


#endif
