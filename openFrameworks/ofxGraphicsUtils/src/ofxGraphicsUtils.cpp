/*
 *  ofxGraphicsUtils.cpp
 *
 *  Created by Pat Long (plong0) on 16/06/10.
 *  Copyright 2010 Spiral Sense Media. All rights reserved.
 *
 */
#include "ofxGraphicsUtils.h"

//----------------------------------------------------------
// static
static bool ofxbSetupCircle		= false;
static int	ofxNumCirclePts		= 0;

static float ofxCirclePts[OF_MAX_CIRCLE_PTS*2];
static float ofxCirclePtsScaled[OF_MAX_CIRCLE_PTS*2];

// copied from the core ofGraphics.cpp file
// since we can't access ofStartSmoothing and ofEndSmoothing
//----------------------------------------------------------
void ofxStartSmoothing();
void ofxStartSmoothing(){
#ifndef TARGET_OPENGLES
	glPushAttrib(GL_COLOR_BUFFER_BIT | GL_ENABLE_BIT);
#endif
	
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	glEnable(GL_LINE_SMOOTH);
	
	//why do we need this?
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}


//----------------------------------------------------------
void ofxEndSmoothing();
void ofxEndSmoothing(){
#ifndef TARGET_OPENGLES
	glPopAttrib();
#endif
}

//----------------------------------------------------------
void ofxCircleSlice(float x,float y, float radius, float lowAngle, float highAngle, bool radians, bool closed, bool adjustRes){
	ofStyle ofstyle = ofGetStyle();
	
	ofstyle.bFill = true;
	closed = true;
	
	if (!ofxbSetupCircle) ofxSetupCircle(ofstyle.circleResolution);
	
	int circleRes = ofxNumCirclePts;
	if(adjustRes)
		ofxSetCircleResolution(360);
	
	// use smoothness, if requested:
	if (ofstyle.smoothing && !ofstyle.bFill) ofxStartSmoothing();
	
	bool angleWrap = (lowAngle > highAngle); // are we doing the 0/360 wrap?
	
/**	if(!radians){
		lowAngle = ofDegToRad(lowAngle);
		highAngle = ofDegToRad(highAngle);
	}*/
	
	int res = ofxNumCirclePts;
	float angle = lowAngle;
	float angleRange = ((!angleWrap)?(highAngle - lowAngle):(M_TWO_PI - lowAngle + highAngle));
	float angleAdder = angleRange / (float)res;
	int k = 0;
	for (int i = 0; i < ofxNumCirclePts; i++){
		float cAngle = angle;
		if(!radians)
			cAngle = ofDegToRad(angle);
		
		ofxCirclePtsScaled[k] = x + cos(cAngle) * radius;
		ofxCirclePtsScaled[k+1] = y - sin(cAngle) * radius;
		angle += angleAdder;
		k+=2;
	}
	
	// we draw the circle points ourself (vs. glDrawArrays) because it allows us to draw the center point, and have the triangles fan around it
	k = 0;
	glBegin((ofstyle.bFill) ? GL_TRIANGLE_FAN : (closed?GL_LINE_LOOP:GL_LINE_STRIP));
	if(closed || (ofstyle.bFill))
		glVertex2f(x, y); // center vertex
	
	// now all the points around the circumference
	for (int i = 0; i < ofxNumCirclePts; i++){
		glVertex2f(ofxCirclePtsScaled[k], ofxCirclePtsScaled[k+1]);
		k+=2;
	}
	glEnd();
	
	// back to normal, if smoothness is on
	if (ofstyle.smoothing && !ofstyle.bFill) ofxEndSmoothing();
	
	if(adjustRes)
		ofxSetCircleResolution(circleRes);
}

//----------------------------------------------------------
void ofxSetupCircle(int res){
	ofSetCircleResolution(res);
}

//----------------------------------------------------------
void ofxSetCircleResolution(int res){
	res = MIN( MAX(1, res), OF_MAX_CIRCLE_PTS);
	
	if (res > 1 && res != ofxNumCirclePts){
		ofxNumCirclePts = res;
		
		float angle = 0.0f;
		float angleAdder = M_TWO_PI / (float)res;
		int k = 0;
		for (int i = 0; i < ofxNumCirclePts; i++){
			ofxCirclePts[k] = cos(angle);
			ofxCirclePts[k+1] = sin(angle);
			angle += angleAdder;
			k+=2;
		}
		ofxbSetupCircle = true;
	}
}

//----------------------------------------------------------
void ofSetHexColor(int hexColor){
	int r = (hexColor >> 16) & 0xff;
	int g = (hexColor >> 8) & 0xff;
	int b = (hexColor >> 0) & 0xff;
	ofSetColor(r,g,b);
}
