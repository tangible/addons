/*
 *  ColourRGBA.h
 *
 *  Created by Pat Long (plong0) on 14/01/09.
 *  Copyright 2009 Tangible Interaction Inc. All rights reserved.
 *
 */
#ifndef _COLOUR_RGBA_H
#define _COLOUR_RGBA_H

#include "ofMain.h"

class ColourRGBA {
public:
	float r, g, b, a;
	string colourID;
	ColourRGBA(){this->r = this->g = this->b = 0.0; this->a = 255.0; this->colourID="default";}
	ColourRGBA(float r, float g, float b){this->r = r; this->g = g; this->b = b; this->a = 255.0; this->colourID="custom";}
	ColourRGBA(float r, float g, float b, float a){this->r = r; this->g = g; this->b = b; this->a = a; this->colourID="custom";}
	ColourRGBA(float r, float g, float b, float a, string colourID){this->r = r; this->g = g; this->b = b; this->a = a; this->colourID=colourID;}
	ColourRGBA(ColourRGBA* colour){this->setRGBA(colour);};
	void setRGBA(float r=255.0, float g=255.0, float b=255.0, float a=255.0){this->r = r; this->g = g; this->b = b; this->a = a;};
	void setRGBA(ColourRGBA* colour){if(colour != NULL) this->setRGBA(colour->r, colour->g, colour->b, colour->a);};
	void setRed(float r=255.0){this->r = r;};
	void setGreen(float g=255.0){this->g = g;};
	void setBlue(float b=255.0){this->b = b;};
	void setAlpha(float a=255.0){this->a = a;};
	void activateColour(){ofSetColor(this->r, this->g, this->b, this->a);};
	string getColourID(){return this->colourID;};
	void setColourID(string colourID){this->colourID = colourID;};
	string getRGBAString(bool withAlpha=true){
		stringstream builder;
		builder << r << "," << g << "," << b;
		if(withAlpha)
			builder << "," << a;
		return builder.str();
	};
};

#endif
