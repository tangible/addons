/*
 *  ofxTI_Colours
 *
 *	Utility classes for working with colours.
 *
 *  Created by Pat Long (plong0) on 14/01/09.
 *  Copyright 2009 Tangible Interaction Inc. All rights reserved.
 *
 */
#ifndef _OFX_TI_COLOURS
#define _OFX_TI_COLOURS

#include "ColourRGBA.h"
#include "ColourFader.h"

#endif
