#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){
	ofSetWindowTitle("ofxTI_Colours Example by Pat Long (plong0)");
	ofSetLogLevel(OF_LOG_NOTICE);
	ofEnableAlphaBlending();
	ofSetFrameRate(30);
	int fadeTime = 50;
	this->fader.addTransition(new ColourRGBA(0, 0, 0, 255), new ColourRGBA(255, 255, 255, 255), fadeTime);
	this->fader.addTransition(new ColourRGBA(255, 255, 255, 255), new ColourRGBA(255, 0, 0, 255), fadeTime);
	this->fader.addTransition(new ColourRGBA(255, 0, 0, 255), new ColourRGBA(255, 255, 0, 255), fadeTime);
	this->fader.addTransition(new ColourRGBA(255, 255, 0, 255), new ColourRGBA(0, 255, 0, 255), fadeTime);
	this->fader.addTransition(new ColourRGBA(0, 255, 0, 255), new ColourRGBA(0, 255, 255, 255), fadeTime);
	this->fader.addTransition(new ColourRGBA(0, 255, 255, 255), new ColourRGBA(0, 0, 255, 255), fadeTime);
	this->fader.addTransition(new ColourRGBA(0, 0, 255, 255), new ColourRGBA(255, 0, 255, 255), fadeTime);
	this->fader.addTransition(new ColourRGBA(255, 0, 255, 255), new ColourRGBA(0, 0, 0, 255), fadeTime);
}

//--------------------------------------------------------------
void testApp::update(){

}

//--------------------------------------------------------------
void testApp::draw(){
	ColourRGBA* cColour = this->fader.getNextColour();
//	cout << "Colour:" << cColour->r << "," << cColour->g << "," << cColour->b << ":" << endl;
	ofBackground(cColour->r, cColour->g, cColour->b);
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){

}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

