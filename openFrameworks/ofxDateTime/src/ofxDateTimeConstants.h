/*
 *  ofxDateTimeConstants.h
 *  emptyExample
 *
 *  Created by Pat Long (plong0) on 29/09/10.
 *  Copyright 2010 Tangible Interaction. All rights reserved.
 *
 */
#ifndef _OFX_DATE_TIME_CONSTANTS
#define _OFX_DATE_TIME_CONSTANTS

#include <string>
using namespace std;

#define OFX_DATE_TIME_MONTH_JANUARY		1
#define OFX_DATE_TIME_MONTH_FEBRUARY	2
#define OFX_DATE_TIME_MONTH_MARCH		3
#define OFX_DATE_TIME_MONTH_APRIL		4
#define OFX_DATE_TIME_MONTH_MAY			5
#define OFX_DATE_TIME_MONTH_JUNE		6
#define OFX_DATE_TIME_MONTH_JULY		7
#define OFX_DATE_TIME_MONTH_AUGUST		8
#define OFX_DATE_TIME_MONTH_SEPTEMBER	9
#define OFX_DATE_TIME_MONTH_OCTOBER		10
#define OFX_DATE_TIME_MONTH_NOVEMBER	11
#define OFX_DATE_TIME_MONTH_DECEMBER	12

const string monthStrings[] = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
const string monthShortStrings[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

#endif
