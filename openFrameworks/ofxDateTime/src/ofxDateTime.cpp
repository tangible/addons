/*
 *  ofxDateTime.cpp
 *
 *  Created by Pat Long (plong0) on 29/09/10.
 *  Copyright 2010 Spiral Sense. All rights reserved.
 *
 */
#include "ofxDateTime.h"

//------------------------------------
ofxDateTime::ofxDateTime(){
	this->hasDateTime = false;
	this->refreshDateTime();
}

//------------------------------------
ofxDateTime::~ofxDateTime(){
}

struct tm* ofxDateTime::getMySystemTM(int epoch){
	struct tm* dt = this->getSystemTM(epoch);
	if(this->hasDateTime){
		dt->tm_sec = this->dateTime.tm_sec;
		dt->tm_min = this->dateTime.tm_min;
		dt->tm_hour = this->dateTime.tm_hour;
		dt->tm_mday = this->dateTime.tm_mday;
		dt->tm_mon = this->dateTime.tm_mon;
		dt->tm_year = this->dateTime.tm_year;
		dt->tm_wday = this->dateTime.tm_wday;
		dt->tm_yday = this->dateTime.tm_yday;
		dt->tm_isdst = this->dateTime.tm_isdst;
		dt->tm_gmtoff = this->dateTime.tm_gmtoff;
		dt->tm_zone = this->dateTime.tm_zone;
	}
	mktime(dt);
	return dt;
}

struct tm* ofxDateTime::getSystemTM(int epoch){
	time_t rawtime;
	if(epoch >= 0)
		rawtime = epoch;
	else
		time(&rawtime); // set rawtime to current system time
	return localtime(&rawtime);
}

//------------------------------------
string ofxDateTime::formatDateTime(string formatStr){
	string result = "";
	if(formatStr != ""){
		size_t bufferSize = formatStr.length()*256;	
		char* buffer = new char[bufferSize];
		strftime (buffer, bufferSize, formatStr.c_str(), this->getMySystemTM());
		result.assign(buffer);
		delete [] buffer;
		buffer = NULL;
	}
	else{
		result.assign(asctime(this->getMySystemTM()));
	}
	
	return result;
}

//------------------------------------
int ofxDateTime::getDay(){
	return this->dateTime.tm_mday;
}

//------------------------------------
int ofxDateTime::getMonth(){
	return this->dateTime.tm_mon + 1;
}

//------------------------------------
int ofxDateTime::getYear(){
	return this->dateTime.tm_year + 1900;
}

//------------------------------------
int ofxDateTime::getHour(){
	return this->dateTime.tm_hour;
}

//------------------------------------
int ofxDateTime::getMinute(){
	return this->dateTime.tm_min;
}

//------------------------------------
int ofxDateTime::getSecond(){
	return this->dateTime.tm_sec;
}

//------------------------------------
int ofxDateTime::getWeekday(){
	return this->dateTime.tm_wday;
}

//------------------------------------
float ofxDateTime::getGMTOffset(){
	return (this->dateTime.tm_gmtoff / 3600.0);
}

//------------------------------------
int ofxDateTime::getEpoch(){
	return mktime(this->getMySystemTM());
}

//------------------------------------
string ofxDateTime::getDateString(bool shortForm){
	return this->formatDateTime((shortForm?"%m/%d/%y":"%B %d, %Y"));
}

//------------------------------------
string ofxDateTime::getDateTimeString(bool shortForm){
	if(shortForm)
		return this->formatDateTime("%m/%d/%y @ %H:%M:%S");
	return this->formatDateTime();
}

//------------------------------------
string ofxDateTime::getMonthString(bool shortForm){
	return this->formatDateTime((shortForm?"%b":"%B"));
}

//------------------------------------
string ofxDateTime::getTimeString(){
	return this->formatDateTime("%H:%M:%S");
}

//------------------------------------
string ofxDateTime::getWeekdayString(bool shortForm){
	return this->formatDateTime((shortForm?"%a":"%A"));
}

//------------------------------------
void ofxDateTime::refreshDateTime(int epoch){
	struct tm* dt = this->getMySystemTM(epoch);
	this->dateTime.tm_sec = dt->tm_sec;
	this->dateTime.tm_min = dt->tm_min;
	this->dateTime.tm_hour = dt->tm_hour;
	this->dateTime.tm_mday = dt->tm_mday;
	this->dateTime.tm_mon = dt->tm_mon;
	this->dateTime.tm_year = dt->tm_year;
	this->dateTime.tm_wday = dt->tm_wday;
	this->dateTime.tm_yday = dt->tm_yday;
	this->dateTime.tm_isdst = dt->tm_isdst;
	this->dateTime.tm_gmtoff = dt->tm_gmtoff;
	this->dateTime.tm_zone = dt->tm_zone;
	this->hasDateTime = true;
}

//------------------------------------
void ofxDateTime::setFromEpoch(int epoch){
	this->refreshDateTime(epoch);
//	time_t rawtime = epoch;
//	this->dateTime = localtime(&rawtime);
}

//------------------------------------
void ofxDateTime::setDate(int day, int month, int year){
	this->setDay(day);
	this->setMonth(month);
	this->setYear(year);
}

//------------------------------------
void ofxDateTime::setDay(int day){
//	if(day < 1) day = 1;
//	if(day > 31) day = 31;
	this->dateTime.tm_mday = day;
}

//------------------------------------
void ofxDateTime::setMonth(int month){
//	if(month < 1) month = 1;
//	if(month > 12) month = 12;
	this->dateTime.tm_mon = month - 1;
}

//------------------------------------
void ofxDateTime::setYear(int year){
	if(year < 1900) year = 1900;
	this->dateTime.tm_year = year - 1900;
}

//------------------------------------
void ofxDateTime::setTime(int hour, int minute, int second){
	this->setHour(hour);
	this->setMinute(minute);
	this->setSecond(second);
}

//------------------------------------
void ofxDateTime::setHour(int hour){
//	if(hour < 0) hour = 0;
//	if(hour > 23) hour = 23;
	this->dateTime.tm_hour = hour;
}

//------------------------------------
void ofxDateTime::setMinute(int minute){
//	if(minute < 0) minute = 0;
//	if(minute > 59) minute = 59;
	this->dateTime.tm_min = minute;
}

//------------------------------------
void ofxDateTime::setSecond(int second){
//	if(second < 0) second = 0;
//	if(second > 61) second = 61; // tm_sec is generally 0-59. Extra range to accommodate for leap seconds in certain systems
	this->dateTime.tm_sec = second;
}

//------------------------------------
void ofxDateTime::setGMTOffset(float gmtOffset){
	this->dateTime.tm_gmtoff = (long)(gmtOffset * 3600.0);
}
