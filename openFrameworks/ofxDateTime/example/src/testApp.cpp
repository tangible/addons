#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){
	ofxDateTime dateTime;
	
	cout << "current date and time: " << dateTime.getDateTimeString(true) << endl;
	cout << "\tepoch:" << dateTime.getEpoch() << endl;
	
	dateTime.setDate(13, 12, 1985);
	dateTime.setTime(21, 51, 13);
	dateTime.refreshDateTime();
	
	cout << dateTime.getDateString(true) << ": " << dateTime.getDateTimeString() << endl;
	cout << "\tepoch:" << dateTime.getEpoch() << endl;
	

}

//--------------------------------------------------------------
void testApp::update(){

}

//--------------------------------------------------------------
void testApp::draw(){

}

//--------------------------------------------------------------
void testApp::keyPressed(int key){

}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

