#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){
#ifdef FULLSCREEN
	ofSetFullscreen(true);
#endif
	int windowMode = ofGetWindowMode();
	this->windowBounds.x = this->windowBounds.y = 0;
	if(windowMode == OF_FULLSCREEN){
		this->windowBounds.width = ofGetScreenWidth();
		this->windowBounds.height = ofGetScreenHeight();
	}
	else if(windowMode == OF_WINDOW){
		this->windowBounds.width = ofGetWidth();
		this->windowBounds.height = ofGetHeight();
	}
	
	ofSetFrameRate(60);
	ofSetBackgroundAuto(true);
	ofEnableAlphaBlending();
	ofBackground(0, 0, 0);									// TRY: default theme with black background
//	ofBackground(255, 255, 255);
	
#if TOUCH_MODE == TOUCH_MODE_TUIO
	this->tuio = new myTuioClient();
	ofAddListener(this->tuio->cursorAdded,this,&testApp::tuioAdded);
	ofAddListener(this->tuio->cursorRemoved,this,&testApp::tuioRemoved);
	ofAddListener(this->tuio->cursorUpdated,this,&testApp::tuioUpdated);
	this->tuio->start(3333);
#elif TOUCH_MODE == TOUCH_MODE_NCORE
	TouchEvents.addListener(this);
	ofSetDataPathRoot(ofToDataPath("nCore/"));
	this->nCore = new ofxNCoreVision();
	this->appEnabled = true;
#endif
	
	/** if the container or panel is too small to hold newly added elements, it will be automatically resized
	 If it's in SHISHA_FLOW_MODE_HORIZONTAL flow mode, it will scale vertically, adding new rows as needed
	 If it's in SHISHA_FLOW_MODE_VERTICAL flow mode, it will scale horizontally, adding new columns as needed
	 */
	float width = 420;
	float height = 420;

	myShisha.init(0, 0, this->windowBounds.width, this->windowBounds.height);
//	myShisha.setTheme();									// TRY: implementing your own theme
//	myShisha.setTheme(new TI_Theme());
	myShisha.setHorizontalSpacing(10);
	myShisha.setVerticalSpacing(10);
	
/**	ShishaElement* newElement;
	newElement = new BasicButton();
	newElement->init(10, 10, 420, 420);
	newElement->setName("button1");
	myShisha.addElement(newElement, false);
	
	newElement = new BasicButton();
	newElement->init(440, 10, 420, 420);
	newElement->setName("button2");
	myShisha.addElement(newElement, false);*/
	
/**	ShishaElement* newElement;
	newElement = new ToggleButton();
	newElement->init(0, 0, 50, 50);
	newElement->setName("toggle1");
	myShisha.addElement(newElement);*/

	ShishaTabManager* newTabs;
	newTabs = new ShishaTabManager();
	newTabs->init(0, 0, 25, 100);
	newTabs->setName("Tabs");
	newTabs->setFlowMode(SHISHA_FLOW_MODE_VERTICAL);
	newTabs->setTabAnimationMode(TABCONTAINER_ANIMATE_MODE_SLIDE_LEFT);
	myShisha.addElement(newTabs);
	
	ShishaTabContainer* newTab;
	newTab = new ShishaTabContainer();
	newTab->init(100, 0, 420, 420);
	newTab->setName("Tab1");
	newTab->setLabel("First");
	
	ShishaElement* newElement;
	newElement = new BasicButton();
	newElement->init(0, 0, 100, 20);
	newElement->setName("button1");
	newElement->setLabel("Press Me!");
	newTab->addElement(newElement);
	
	newElement = new TextOutput();
	newElement->init(0, 0, 300, 40);
	newElement->setName("output1");
	newElement->setLabel("");
	newElement->setString("The quick brown fox jumps over the lazy dog!");
	newTab->addElement(newElement);
	
	newElement = new BasicButton();
	newElement->init(0, 0, 100, 20);
	newElement->setName("button2");
	newElement->setLabel("Me Too!");
	newTab->addElement(newElement);
	
	newElement = new TextInput();
	newElement->init(0, 0, 300, 80);
	newElement->setName("input1");
	newElement->setLabel("Input");
	newTab->addElement(newElement);
	
	newElement = new BasicButton();
	newElement->init(0, 0, 100, 20);
	newElement->setName("button3");
	newElement->setLabel("Me 3!");
	newTab->addElement(newElement);
	
	newElement = new TextInput();
	newElement->init(0, 0, 300, 20);
	newElement->setName("input2");
	newElement->setLabel("Input");
	((TextInput*)newElement)->setMaxLineCount(1);
	newTab->addElement(newElement);
	
	newTabs->addContainer(newTab);
	
	newTab = new ShishaTabContainer();
	newTab->init(100, 0, 420, 420);
	newTab->setName("Tab2");
	newTab->setLabel("Second");
	
	RadioSet* newRadio;
	newRadio = new RadioSet();
	newRadio->init(0, 0, 400, 100);
	newRadio->setName("radios1");
	newRadio->addButton(100, 25, "Output 1", "radio1");
	newRadio->addButton(100, 25, "Output 2", "radio2");
	newRadio->addButton(100, 25, "Output 3", "radio3");
	newTab->addElement(newRadio);
	
	newElement = new TextOutput();
	newElement->init(0, 0, 400, 80);
	newElement->setName("output1");
	newElement->setLabel("Output 1: ");
	newTab->addElement(newElement);
	
	newElement = new TextOutput();
	newElement->init(0, 0, 400, 80);
	newElement->setName("output2");
	newElement->setLabel("Output 2: ");
	newTab->addElement(newElement);
	
	newElement = new TextOutput();
	newElement->init(0, 0, 400, 80);
	newElement->setName("output3");
	newElement->setLabel("Output 3: ");
	newTab->addElement(newElement);
	 
	newTabs->addContainer(newTab);
	
	
	newTab = new ShishaTabContainer();
	newTab->init(100, 0, 420, 420);
	newTab->setName("Tab3");
	newTab->setLabel("Third");
	
	newElement = new BasicBar();
	newElement->init(0, 0, 400, 20);
	newElement->setName("bar1");
	newElement->setLabel("Bar #1: ");
	((BasicBar*)newElement)->enableSlider(true, GUI_ELEMENT_SHAPE_CIRCLE);
	newTab->addElement(newElement);
	
	newElement = new BasicBar();
	newElement->init(0, 0, 42, 42);
	newElement->setName("dial1");
	newElement->setLabel("Dial #1: ");
	newElement->setGUIShape(GUI_ELEMENT_SHAPE_CIRCLE);
	newTab->addElement(newElement);
	
	newElement = new BasicBar();
	newElement->init(0, 0, 42, 42);
	newElement->setName("dial2");
	newElement->setLabel("Dial #2: ");
	newElement->setGUIShape(GUI_ELEMENT_SHAPE_CIRCLE);
	newTab->addElement(newElement);
	
	newElement = new BasicBar();
	newElement->init(0, 0, 42, 42);
	newElement->setName("dial3");
	newElement->setLabel("Dial #3: ");
	newElement->setGUIShape(GUI_ELEMENT_SHAPE_CIRCLE);
	newTab->addElement(newElement);
	
	newElement = new BasicBar();
	newElement->init(0, 0, 42, 42);
	newElement->setName("dial4");
	newElement->setLabel("Dial #4: ");
	newElement->setGUIShape(GUI_ELEMENT_SHAPE_CIRCLE);
	newTab->addElement(newElement);
	
	newElement = new BasicBar();
	newElement->init(0, 0, 42, 42);
	newElement->setName("dial5");
	newElement->setLabel("Dial #5: ");
	newElement->setGUIShape(GUI_ELEMENT_SHAPE_CIRCLE);
	newTab->addElement(newElement);
	
	newElement = new BasicBar();
	newElement->init(0, 0, 42, 200);
	newElement->setName("meter1");
	newElement->setLabel("Meter #1: ");
	newElement->setGUIStyle(BAR_STYLE_VERTICAL);
	((BasicBar*)newElement)->enableSlider();
	newTab->addElement(newElement);
	
	newElement = new BasicBar();
	newElement->init(0, 0, 42, 200);
	newElement->setName("meter2");
	newElement->setLabel("Meter #2: ");
	newElement->setGUIStyle(BAR_STYLE_VERTICAL);
	newTab->addElement(newElement);
	
	newTabs->addContainer(newTab);
	
	
	ShishaKeyboard* newKB;
	newKB = new ShishaKeyboard();
#if TOUCH_MODE == TOUCH_MODE_NCORE
	newKB->setImageBase("../ShishaKeyboard/");
#else
	newKB->setImageBase("ShishaKeyboard/");
#endif
	newKB->init(10, 420, 200, 200);
	myShisha.addElement(newKB, false);

	
/**	ShishaElement* newElement;
	newElement = new TextInput();
	newElement->init(0, 0, 240, 80);
	newElement->setName("textInput");
	newElement->setLabel("");
	newElement->setString("Testing 123... Coolio boolio hoolio! Beastial devastation will rock the earth upon which we dwell in madness!");
//	newElement->setString("The quick brown fox jumps over the lazy dog. The brown fox sleeps lazily while quick sheep jump and bound above it! Testing testing 123...\n456...\n...789 Testing.");
	*/
/**	ShishaElement* newRadio;
	newRadio = newElement->addButton(25, 25, "Radio 1", "radio1");
	newRadio = newElement->addButton(25, 25, "Radio 2", "radio2");
	newRadio = newElement->addButton(25, 25, "Radio 3", "radio3");*/
	
	newElement = new ToggleButton();
	newElement->init(0, 0, 134, 68);
	
	ShishaImageMap* newImageMap = new ShishaImageMap();
	newImageMap->init(540, 50, 200, 100);
	newImageMap->loadImageMap("imageMapSample.png");
	newImageMap->mapElement(newElement, 32, 16, 135, 68);
	
	myShisha.addElement(newImageMap, false);

	float x = (this->windowBounds.width - width*2.0 + 10)/ 2.0;
	float y = (this->windowBounds.height - height) / 2.0 - 50;
//	myShisha.setLocation(x, y, -1, -1, 50); // check it out, it can move around!
}

//--------------------------------------------------------------
void testApp::exit(){
#if TOUCH_MODE == TOUCH_MODE_TUIO
	delete this->tuio;
#elif TOUCH_MODE == TOUCH_MODE_NCORE
//	delete this->nCore;
#endif
}

//--------------------------------------------------------------
void testApp::update(){
#if TOUCH_MODE == TOUCH_MODE_TUIO
	this->tuio->getMessage();
#endif
	myShisha.update();
	
	bool button1 = myShisha.getBool("Tabs.Tab1.button1");
	bool button2 = myShisha.getBool("Tabs.Tab1.button2");
	bool button3 = myShisha.getBool("Tabs.Tab1.button3");
	
	if(button1 || button2 || button3){
		string outputSelect = myShisha.getString("Tabs.Tab2.radios1");
		string outputSelector = "";
		if(outputSelect == "radio1")
			outputSelector = "Tabs.Tab2.output1";
		else if(outputSelect == "radio2")
			outputSelector = "Tabs.Tab2.output2";
		else if(outputSelect == "radio3")
			outputSelector = "Tabs.Tab2.output3";
		
		if(outputSelector != ""){
			string input = "";
			if(button1)
				input = myShisha.getString("Tabs.Tab1.output1");
			if(button2)
				input = myShisha.getString("Tabs.Tab1.input1");
			if(button3)
				input = myShisha.getString("Tabs.Tab1.input2");
			myShisha.setStringValue(outputSelector, input);
		}
	}
	
	ShishaElement* inputElement = NULL;
	if(myShisha.getBool("Tabs.Tab1.input1"))
		inputElement = myShisha.selectElement("Tabs.Tab1.input1");
	else if(myShisha.getBool("Tabs.Tab1.input2"))
		inputElement = myShisha.selectElement("Tabs.Tab1.input2");
	myShisha.setElementValue("ShishaKeyboard", inputElement);
}

//--------------------------------------------------------------
void testApp::draw(){
#if TOUCH_MODE == TOUCH_MODE_TUIO
	this->tuio->drawCursors();
#elif TOUCH_MODE == TOUCH_MODE_NCORE
	//draw blobs
	std::map<int, Blob> blobs;
	std::map<int, Blob>::iterator iter;
    blobs = this->nCore->getBlobs(); //get blobs from tracker
	for(iter=blobs.begin(); iter!=blobs.end(); iter++)
	{
		Blob drawBlob;
		drawBlob = iter->second;
		ofSetColor(drawBlob.color);
		ofFill();
		ofEllipse( (drawBlob.centroid.x - drawBlob.boundingRect.width/2) * ofGetWidth() , 
				  (drawBlob.centroid.y - drawBlob.boundingRect.height/2) * ofGetHeight(),
				  drawBlob.boundingRect.width * ofGetWidth(), 
				  drawBlob.boundingRect.height * ofGetHeight());
	}
#endif
	myShisha.draw();
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){
	cout << "keyPress:" << key << endl;
#if TOUCH_MODE == TOUCH_MODE_NCORE
	if ( key == '~' || key == '`' ){
		if(this->appEnabled){
			// disable GW, and enable the nCore config
			this->appEnabled = false;
#ifdef FULLSCREEN
			ofSetFullscreen(false);
#endif
			ofSetBackgroundAuto(true);
		}
		else if(!this->nCore->bMiniMode && !this->nCore->bCalibration){
			// enable the GW, and disable the nCore config
			this->appEnabled = true;
#ifdef FULLSCREEN
			ofSetFullscreen(true);
#endif
			ofSetBackgroundAuto(false);
		}
	}
	else{
		this->myShisha.checkKeyPressed(key);
	}
#else
	this->myShisha.checkKeyPressed(key);
#endif
}

//--------------------------------------------------------------
void testApp::keyReleased(int key){
	this->myShisha.checkKeyReleased(key);
}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){
	this->myShisha.checkCursorHover(x, y, MOUSE_CURSOR_ID);
}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){
	this->myShisha.checkCursorDrag(x, y, button, MOUSE_CURSOR_ID);
}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){
	this->myShisha.checkCursorPress(x, y, button, MOUSE_CURSOR_ID);
}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){
	this->myShisha.checkCursorRelease(x, y, button, MOUSE_CURSOR_ID);
}

#if TOUCH_MODE == TOUCH_MODE_TUIO
//--------------------------------------------------------------
void testApp::tuioAdded(ofxTuioCursor & tuioCursor){
	tuioCursor.scaleTo(&this->windowBounds);
	this->myShisha.checkCursorPress(tuioCursor.getX(), tuioCursor.getY(), 0, tuioCursor.getCursorID());
	tuioCursor.scaleFrom(&this->windowBounds);
}

//--------------------------------------------------------------
void testApp::tuioRemoved(ofxTuioCursor & tuioCursor){
	tuioCursor.scaleTo(&this->windowBounds);
	this->myShisha.checkCursorRelease(tuioCursor.getX(), tuioCursor.getY(), 0, tuioCursor.getCursorID());
	tuioCursor.scaleFrom(&this->windowBounds);
}

//--------------------------------------------------------------
void testApp::tuioUpdated(ofxTuioCursor & tuioCursor){
	tuioCursor.scaleTo(&this->windowBounds);
	this->myShisha.checkCursorDrag(tuioCursor.getX(), tuioCursor.getY(), 0, tuioCursor.getCursorID());
	tuioCursor.scaleFrom(&this->windowBounds);
}
#elif TOUCH_MODE == TOUCH_MODE_NCORE
string testApp::getBlobID(Blob* b){
	stringstream idBuilder;
	idBuilder << "nCore:" << b->id;
	return idBuilder.str();
}

void testApp::TouchDown( Blob b){
	if(this->appEnabled)
		this->myShisha.checkCursorPress(b.centroid.x*this->windowBounds.width+this->windowBounds.x, b.centroid.y*this->windowBounds.height+this->windowBounds.y, 0, this->getBlobID(&b), b.boundingRect.width*this->windowBounds.width, b.boundingRect.height*this->windowBounds.height);
}

void testApp::TouchUp( Blob b){
	if(this->appEnabled)
		this->myShisha.checkCursorRelease(b.centroid.x*this->windowBounds.width+this->windowBounds.x, b.centroid.y*this->windowBounds.height+this->windowBounds.y, 0, this->getBlobID(&b), b.boundingRect.width*this->windowBounds.width, b.boundingRect.height*this->windowBounds.height);
}

void testApp::TouchMoved( Blob b){
	if(this->appEnabled)
		this->myShisha.checkCursorDrag(b.centroid.x*this->windowBounds.width+this->windowBounds.x, b.centroid.y*this->windowBounds.height+this->windowBounds.y, 0, this->getBlobID(&b), b.boundingRect.width*this->windowBounds.width, b.boundingRect.height*this->windowBounds.height);
}
#endif

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){
	int windowMode = ofGetWindowMode();
	this->windowBounds.x = this->windowBounds.y = 0;
	if(windowMode == OF_FULLSCREEN){
		this->windowBounds.width = ofGetScreenWidth();
		this->windowBounds.height = ofGetScreenHeight();
	}
	else if(windowMode == OF_WINDOW){
		this->windowBounds.width = ofGetWidth();
		this->windowBounds.height = ofGetHeight();
	}
}

