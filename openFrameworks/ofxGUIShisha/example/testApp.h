#ifndef _TEST_APP
#define _TEST_APP

#define TOUCH_MODE_NONE		-1
#define TOUCH_MODE_TUIO		0
#define TOUCH_MODE_NCORE	1

#define TOUCH_MODE			TOUCH_MODE_NONE
//#define FULLSCREEN

#include "ofMain.h"
#include "ofxGUIShisha.h"

#if TOUCH_MODE == TOUCH_MODE_TUIO
	#include "ofxTuio.h"
#elif TOUCH_MODE == TOUCH_MODE_NCORE
	#include "ofxNCore.h"
#endif

#define MOUSE_CURSOR_ID "mouse"

class testApp : public ofBaseApp
#if TOUCH_MODE == TOUCH_MODE_NCORE
	, public TouchListener
#endif
{
	private:
		ofRectangle windowBounds;
		ShishaContainer myShisha;
	
	#if TOUCH_MODE == TOUCH_MODE_TUIO
		myTuioClient* tuio;
	#elif TOUCH_MODE == TOUCH_MODE_NCORE
		ofxNCoreVision* nCore;
		bool appEnabled;
	
		string getBlobID(Blob* b);
	#endif

	public:
		void setup();
		void exit();
		void update();
		void draw();

		void keyPressed  (int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
	
#if TOUCH_MODE == TOUCH_MODE_TUIO
		void tuioAdded(ofxTuioCursor & tuioCursor);
		void tuioRemoved(ofxTuioCursor & tuioCursor);
		void tuioUpdated(ofxTuioCursor & tuioCursor);
#elif TOUCH_MODE == TOUCH_MODE_NCORE
		void TouchDown(Blob b);
		void TouchMoved(Blob b);
		void TouchUp(Blob b);
#endif
		void windowResized(int w, int h);

};

#endif
