#ifndef _OFX_GUISHISHA
#define _OFX_GUISHISHA

#include "ElementTypes.h"
#include "Base/BaseIndex.h"
#include "Hookah/HookahIndex.h"
#include "Themes/ThemeIndex.h"

#include "Addons/ofxGUIShishaAddonsIndex.h"

#endif
