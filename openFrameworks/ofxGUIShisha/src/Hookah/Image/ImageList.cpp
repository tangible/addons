/*
 *  ImageList.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 10/03/10.
 *  Copyright 2010 Tangible Interaction. All rights reserved.
 *
 */
#include "ImageList.h"

ImageList::ImageList(){
}

ImageList::~ImageList(){
	for(map<string,ofxImage*>::iterator it=this->images.begin(); it != this->images.end(); it++){
		delete (*it).second;
		this->images[(*it).first] = NULL;
	}
	this->images.clear();
}

void ImageList::init(float x, float y, float width, float height, int elementID){
	ShishaElement::init(x, y, width, height, elementID);
	this->elementType = SHISHA_TYPE_HOOKAH_IMAGE_LIST;
	this->setName("ImageList");
	this->setActive(this->images.end());
	this->allowEmptyActive = true;
}

void ImageList::draw(){
	this->draw(this->getX(), this->getY());
}

void ImageList::draw(float x, float y){
	this->draw(x, y, -1.0, -1.0);
}

void ImageList::draw(float x, float y, float w, float h){
	this->draw(x, y, w, h, this->getTheme()->drawImageBorders());
}

void ImageList::draw(float x, float y, float w, float h, bool borders){
	ofxImage* activeImage = this->getActiveImage();
	if(activeImage != NULL){
		if(w == -1.0)
			w = activeImage->getWidth();
		if(h == -1.0)
			h = activeImage->getHeight();
		activeImage->draw(x, y, w, h);
		if(borders){
			this->getTheme()->setColour(SHISHA_COLOUR_BORDER, this);
			ofNoFill();
			ofRect(x, y, activeImage->getWidth(), activeImage->getHeight());
			ofFill();
			this->getTheme()->setColour();
		}
	}
}

void ImageList::setTheme(ShishaTheme* theme, ShishaTheme* doNotDelete, bool themeInherited){
	ShishaElement::setTheme(theme, doNotDelete, themeInherited);
	ShishaTheme* cTheme = this->getTheme();
	if(cTheme != NULL)
		this->drawBorders = cTheme->drawImageBorders();
}

int ImageList::addImage(string imageFileName, string imageName, bool activate){
	ofxImage* newImage = new ofxImage();
	if(newImage->loadImage(imageFileName))
		return this->addImage(newImage, imageName, activate);
	else{
		delete newImage;
		newImage = NULL;
	}
	return -1;
}

int ImageList::addImage(ofxImage* image, string imageName, bool activate){
	if(image == NULL)
		return -1;
	bool activeWasEnd = (this->active == this->images.end());
	if(imageName == ""){
		if(image->getFileName() != "")
			imageName = image->getFileName();
		else
			imageName = "image_"+ofToString(this->images.size(), 0);
	}
	this->images[imageName] = image;
	if(activeWasEnd){
		if(this->allowEmptyActive)
			this->setActive(this->images.end());
		else
			activate = true;
	}
	if(activate)
		this->setActiveImage(imageName);
	return this->images.size();
}

int ImageList::loadDirectory(string imageDir){
	if(imageDir != ""){
		ofxDirList dirList;
		int listCount;
		dirList.allowExt("jpg");
		dirList.allowExt("png");
		dirList.allowExt("gif");
		listCount = dirList.listDir(imageDir);
		for(int i=0; i < listCount; i++)
			this->addImage(dirList.getPath(i), dirList.getName(i));
		return listCount;
	}
	return 0;
}

ofxImage* ImageList::getActiveImage(){
	if(this->active == this->images.end())
		return NULL;
	else
		return (*this->active).second;
}

string ImageList::getActiveImageName(){
	if(this->active == this->images.end())
		return "";
	else
		return (*this->active).first;
}

void ImageList::setAllowEmptyActive(bool allowEmptyActive){
	this->allowEmptyActive = allowEmptyActive;
}

int ImageList::setNextActiveImage(){
	if(this->active == this->images.end())
		this->setActive(this->images.begin());
	else{
		this->setNextActive();
		if(!this->allowEmptyActive && this->active == this->images.end())
			this->setActive(this->images.begin());
	}
}

int ImageList::setPreviousActiveImage(){
	if(this->active == this->images.begin()){
		this->setActive(this->images.end());
		if(!this->allowEmptyActive)
			this->setPrevActive();
	}
	else
		this->setPrevActive();
}

bool ImageList::setActive(map<string,ofxImage*>::iterator active){
	this->active = active;
	this->setDimensionsToActive();
}

void ImageList::setNextActive(){
	this->active++;
	this->setDimensionsToActive();
}

void ImageList::setPrevActive(){
	this->active--;
	this->setDimensionsToActive();
}

void ImageList::setDimensionsToActive(){
	ofxImage* activeImage = this->getActiveImage();
	if(activeImage != NULL)
		this->setDimensions(activeImage->getWidth(), activeImage->getHeight());
}

bool ImageList::setActiveImage(int imageIndex){
	if(imageIndex < 0)
		imageIndex = this->allowEmptyActive?-1:0;
	else if(imageIndex > this->images.size())
		imageIndex = this->allowEmptyActive?-1:this->images.size()-1;
	if(imageIndex == -1 && this->allowEmptyActive)
		this->setActiveImage("");
	else{
		int cCount = 0;
		map<string,ofxImage*>::iterator it;
		for(it = this->images.begin(); cCount < imageIndex && it != this->images.end(); it++)
			cCount++;
		this->setActive(it);
	}
}

bool ImageList::setActiveImage(string imageName){
	if(imageName == ""){
		this->setActive(this->images.end());
		return true;
	}
	else{
		map<string,ofxImage*>::iterator temp = this->images.find(imageName);
		if(temp != this->images.end()){
			this->setActive(temp);
			return true;
		}
	}
	return false;
}
