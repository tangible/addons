/*
 *  TextInput.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 06/12/09.
 *  Copyright 2009 Tangible Interaction. All rights reserved.
 *
 */

#include "TextInput.h"

TextInput::TextInput(){
}

TextInput::~TextInput(){
}

void TextInput::init(float x, float y, float width, float height, int elementID){
	this->cursorIndex = -1;
	this->oldCursorIndex = this->lastClickIndex = -1;
	this->cursorOffsetX = this->cursorOffsetY = 0;
	this->maxTextLength = -1;
	this->cursorHeight = 0;
	this->setUserCursorHeight();
	this->keyboardEnabled = true;
	
	TextOutput::init(x, y, width, height, elementID);
	this->elementType = SHISHA_TYPE_HOOKAH_TEXT_INPUT;
	this->setName("Input");
	this->setLabel("Input");
	this->setStringValue("Click to change");
	this->setCursorIndex();
	this->cursorBlinkSpeed = DEFAULT_CURSOR_BLINK_SPEED;
}

void TextInput::onPress(string cursorID){
	this->setCursorFromCursor(cursorID);
}

void TextInput::onPressOff(string cursorID){
	this->oldCursorIndex = this->cursorIndex;
	this->setCursorIndex();
}

void TextInput::onDragOff(string cursorID){
	this->setCursorIndex();
}

void TextInput::onRelease(string cursorID){
	//	ShishaElement::onRelease(cursorID);
	this->setCursorFromCursor(cursorID);
}

void TextInput::update(){
	if(this->cursorBlinkSpeed > 0 && ofGetFrameNum() % this->cursorBlinkSpeed == 0){
		this->cursorBlinkState = !this->cursorBlinkState;
	}
}

void TextInput::draw(float x, float y, float w, float h){
	this->draw(x, y, w, h, this->getTheme()->drawTextInputBorders());
}

void TextInput::draw(float x, float y, float w, float h, bool borders){
	if(borders){
		float labelOffSet = this->getTextWidth(this->getLabel()); // - 4;
		
		// only draw the background if we're also drawing borders
		this->getTheme()->setColour(SHISHA_COLOUR_BACKGROUND, this);
		ofFill();
		ofRect(x+labelOffSet, y, w-labelOffSet, h);
		
		this->getTheme()->setColour(SHISHA_COLOUR_BORDER, this);	
		ofNoFill();
		ofRect(x+labelOffSet, y, w-labelOffSet, h);
		
		this->getTheme()->setColour();
		ofFill();
	}
	
	TextOutput::draw(x, y, w, h, false);
	if(this->cursorBlinkState && this->cursorIndex != -1 && this->cursorOffsetX != -1 && this->cursorOffsetY != -1){
		this->getTheme()->setColour(SHISHA_COLOUR_BORDER, this);
		float x = this->x+this->getPaddingL()+this->cursorOffsetX;
		float y = this->getCursorYOffset();
		float h = this->getCursorHeight();
		ofLine(x, y, x, y+h);
		this->getTheme()->setColour();
	}
}

void TextInput::appendCharacter(char newChar){
	if(this->canInsert(newChar)){
		int cCursorIndex = this->cursorIndex;
		if(cCursorIndex < 0){
			cCursorIndex = 0;
		}
		else if(cCursorIndex > this->textValue.length()){
			cCursorIndex = this->textValue.length();
		}
//		string::iterator inIt = this->textValue.begin()+this->cursorIndex;
		this->textValue.insert(cCursorIndex, 1, newChar);
//		this->nextCursorIndex();
		this->wrapLines();
		this->setCursorIndex(cCursorIndex+1);
//		this->setCursorIndex(this->textValue.length());
	}
}

void TextInput::appendString(string newString){
	if(this->canInsert(newString)){
//		string::iterator inIt = this->textValue.begin()+this->cursorIndex;
		int cCursorIndex = this->cursorIndex;
		if(cCursorIndex < 0){
			cCursorIndex = 0;
		}
		else if(cCursorIndex > this->textValue.length()){
			cCursorIndex = this->textValue.length();
		}
		this->textValue.insert(cCursorIndex, newString);
		//		this->nextCursorIndex();
		this->wrapLines();
		this->setCursorIndex(cCursorIndex+newString.length());
//		this->setCursorIndex(this->textValue.length());
	}
}

void TextInput::eraseCharacter(){
	if(this->cursorIndex > 0){
		int cCursorIndex = this->cursorIndex;
		if(cCursorIndex > this->textValue.length()){
			cCursorIndex = this->textValue.length();
		}
		string::iterator delIt = this->textValue.begin()+cCursorIndex-1;
		this->textValue.erase(delIt);
//		this->previousCursorIndex();
		this->wrapLines();
		this->setCursorIndex(cCursorIndex-1);
//		this->setCursorIndex(this->textValue.length());
	}
}

int TextInput::calculateMaxTextLength(){
	this->maxTextLength = (this->getMaxLineWidth()-this->getTextWidth(this->getLabel())) / (this->getTextWidth(" ")*1.1);
}

void TextInput::wrapLines(){
	if(this->textValue == ""){
		this->textLines.clear();
		this->setCursorIndex(0);
	}
	else{
		TextOutput::wrapLines();
	}
}

bool TextInput::validChar(char newCheck){
	return true;
}

bool TextInput::canInsert(char newCheck){
	if(!this->validChar(newCheck))
		return false;
	stringstream checker;
	checker << newCheck;
	return this->canInsert(checker.str());
}

bool TextInput::canInsert(string newCheck){
	for(int i=0; i < newCheck.length(); i++){
		if(!this->validChar(newCheck.at(i)))
		   return false;
	}
	
	if(this->maxTextLength == -1 && this->maxLineCount == 1){
		if(this->textLines.size() == 0)
			return true;
		stringstream builder;
		if(this->textLines.size() > 0)
			builder << this->getOutputLine(0); //this->textLines[0];
		builder << newCheck;
		return (this->getTextWidth(builder.str()) <= this->getMaxLineWidth());
	}	
	return (this->maxTextLength == -1 || (this->textValue.length() + newCheck.length()) <= this->maxTextLength);
}

int TextInput::getClosestLetterIndex(float x, float y){
	int result = -2;
	
	int lineHeight = this->getCursorHeight()+3; //this->getTextHeight()+3;
	int checkY = y-this->getCursorYOffset(); //-this->getY()-this->getPaddingT();
	
	int line = 0;
	line = checkY/lineHeight;
//	if(checkY % lineHeight == 0)
//		line--;
	if(line < 0)
		line = 0;
	
	if(line >= this->textLines.size())
		return this->textValue.length();
	
	int i;
	
	int xOffset = this->getX() + this->getTextWidth(this->getLabel());
	string cLine = this->getOutputLine(line);
	for(i=0; i < cLine.length(); i++){
		if((xOffset + this->getTextWidth(this->textLines[line].substr(0, i))) >= x){
			result = i;
			break;
		}
	}
	if(i == cLine.length())
		result = cLine.length();
	for(int i=0; i < line && i < this->textLines.size(); i++)
		result += this->textLines[i].length();
	if(result < 0)
		result = -2;
	return result;
}

int TextInput::getCursorYOffset(){
	return (this->y+this->getPaddingT()+this->cursorOffsetY+this->userCursorOffsetY);
}

int TextInput::getCursorHeight(){
	return ((this->userCursorHeight == 0.0)?this->cursorHeight:this->userCursorHeight);
}

int TextInput::getGuiDisplayState(){
	if(this->cursorIndex == -1)
		return (this->hasCursors()?GUI_ELEMENT_DISPLAY_STATE_INACTIVE_HOVER:GUI_ELEMENT_DISPLAY_STATE_INACTIVE);
	else
		return (this->hasCursors()?GUI_ELEMENT_DISPLAY_STATE_ACTIVE_HOVER:GUI_ELEMENT_DISPLAY_STATE_ACTIVE);
}

bool TextInput::getBool(string selector){
	if(this->selectSelf(selector))
		return (this->cursorIndex != -1);
	return TextOutput::getBool(selector);
}

void TextInput::setBool(bool value){
	if(value){
		int index = this->lastClickIndex;
		if(index == -1)
			index = this->oldCursorIndex;
		if(index == -1)
			index = this->textValue.length();
		this->setCursorIndex(index);
	}
}

ShishaElement* TextInput::setBoolValue(string selector, bool value){
	if(selector == "keyboard"){
		this->setKeyboardEnabled(value);
		return this;
	}
	else
		return TextOutput::setBoolValue(selector, value);
}

ShishaElement* TextInput::setStringValue(string selector, string value){
	if(selector == "input"){
		this->appendString(value);
		return this;
	}
	else if(selector == "enter"){
		this->appendCharacter('\n');
	}
	else if(selector == "erase"){
		this->eraseCharacter();
		return this;
	}	
	else
		return TextOutput::setStringValue(selector, value);
}

void TextInput::nextCursorIndex(){
	this->setCursorIndex(this->cursorIndex+1);
}

void TextInput::previousCursorIndex(){
	if(this->cursorIndex > 0){
		this->setCursorIndex(this->cursorIndex-1);
	}
}

void TextInput::nextCursorYIndex(){
	this->setCursorIndex(this->getClosestLetterIndex(this->getX()+this->getPaddingL()+this->cursorOffsetX, this->getY()+this->getPaddingT()+this->cursorOffsetY+this->getTextHeight()*2.0+5));
}

void TextInput::previousCursorYIndex(){
	this->setCursorIndex(this->getClosestLetterIndex(this->getX()+this->getPaddingL()+this->cursorOffsetX, this->getY()+this->getPaddingT()+this->cursorOffsetY));
}

void TextInput::setCursorIndex(int index){
	if(this->textValue.length() == 0 && index > 0){
		index = 0;
	}
	else if(this->textValue.length() > 0 && index > (int)this->textValue.length()){
		index = this->textValue.length();
	}
	this->cursorIndex = index;
	if(this->lastClickIndex != -1)
		this->lastClickIndex = index;
	if(index == 0){
		this->cursorOffsetX = this->getTextWidth(this->getLabel())+1;
		this->cursorOffsetY = 0;
		this->cursorBlinkState = true;
	}
	else if(index > 0){
		int cIndex = 0;
		int cLine = 0;
		int cChar = 0;
		while(cIndex < index && cLine < this->textLines.size()){
			if((cIndex + this->textLines[cLine].length()) < index){
				cIndex += this->textLines[cLine].length();
				cLine++;
			}
			else{
				cChar = index-cIndex;
				cIndex = index;
			}
		}
		if(cLine >= this->textLines.size() && cLine > 0)
			cLine--;
		this->cursorOffsetX = this->getTextWidth(this->getLabel());
		if(this->textLines.size() > 0){
			if(cChar == 0)
				this->cursorOffsetX += 1;
			else{
				string cLineVal = this->getOutputLine(cLine);
				this->cursorOffsetX += this->getTextWidth(cLineVal.substr(0, cChar));
			}
				
			this->cursorOffsetY = this->getTextHeight()*cLine;
		}
		this->cursorBlinkState = true;
	}
}

void TextInput::setCursorLocation(float x, float y){
	if(x != -1 && y != -1){
		this->setCursorIndex(this->getClosestLetterIndex(x, y));
	}
}

void TextInput::setCursorFromCursor(string cursorID){
	float cX = this->cursors.getX(cursorID);
	float cY = this->cursors.getY(cursorID);
	if(this->isHovered(cursorID, cX, cY)){
		float labelOffSet = this->getTextWidth(this->getLabel()); // - 4;
		if(cX >= this->getX()+labelOffSet && cX <= this->getX()+labelOffSet+this->getWidth()
		   && cY >= this->getY() && cY <= this->getY()+this->getHeight()){
			this->setCursorLocation(cX, cY);
		}
		else{
			this->setCursorIndex(this->textValue.length());
		}
		this->lastClickIndex = this->cursorIndex;
	}
}

void TextInput::setDimensions(float width, float height){
	TextOutput::setDimensions(width, height);
	this->calculateMaxTextLength();
}

void TextInput::setKeyboardEnabled(bool keyboardEnabled){
	this->keyboardEnabled = keyboardEnabled;
}

void TextInput::setPadding(float l, float t, float r, float b){
	TextOutput::setPadding(l, t, r, b);
	this->calculateMaxTextLength();
}

void TextInput::setLabel(string label){
	TextOutput::setLabel(label);
	
	this->setCursorIndex(this->cursorIndex);
	this->cursorOffsetY = (this->height+this->cursorHeight)/4.0-5;
}

void TextInput::setTheme(ShishaTheme* theme, ShishaTheme* doNotDelete, bool themeInherited){
	TextOutput::setTheme(theme, doNotDelete, themeInherited);
	this->cursorHeight = this->getTextHeight("|")*1.5;
	this->cursorOffsetY = (this->height+this->cursorHeight)/4.0-5;
}

void TextInput::setUserCursorHeight(int userCursorHeight, int userCursorOffsetY){
	this->userCursorHeight  = userCursorHeight;
	this->userCursorOffsetY = userCursorOffsetY;
}

bool TextInput::checkKeyPressed(int key){
	if(!this->keyboardEnabled)
		return false;
	if(this->cursorIndex != -1){
		if(key == OF_KEY_BACKSPACE){
			this->eraseCharacter();
		}
		else if(key == OF_KEY_LEFT){
			this->previousCursorIndex();
		}
		else if(key == OF_KEY_RIGHT){
			this->nextCursorIndex();
		}
		else if(key == OF_KEY_HOME){
			this->setCursorIndex(0);
		}
		else if(key == OF_KEY_UP){
			this->previousCursorYIndex();
		}
		else if(key == OF_KEY_DOWN){
			this->nextCursorYIndex();
		}
		else if(key == OF_KEY_END){
			this->setCursorIndex(this->textValue.length());
		}
		else if(key == OF_KEY_ESC || key == OF_KEY_DEL 
				|| key == OF_KEY_F1 || key == OF_KEY_F2 || key == OF_KEY_F3 || key == OF_KEY_F4 || key == OF_KEY_F5 || key == OF_KEY_F6 || key == OF_KEY_F7 || key == OF_KEY_F8 || key == OF_KEY_F9 || key == OF_KEY_F10 || key == OF_KEY_F11 || key == OF_KEY_F12
			 || key == OF_KEY_UP || key == OF_KEY_DOWN || key == OF_KEY_PAGE_UP || key == OF_KEY_PAGE_DOWN || key == OF_KEY_INSERT){
		}
		else if(key == '\t'){
			// how to handle a tab key
		}
		else if(key == OF_KEY_RETURN){
			this->appendCharacter('\n');
		}
		else{
			this->appendCharacter((char)key);
		}
	}
	return true;
}


