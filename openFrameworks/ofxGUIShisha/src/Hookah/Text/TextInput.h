/*
 *  TextInput.h
 *  openFrameworks
 *
 *  Created by Pat Long on 06/12/09.
 *  Copyright 2009 Tangible Interaction. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_TEXT_INPUT
#define _OFX_GUISHISHA_TEXT_INPUT

#include "TextOutput.h"

#define DEFAULT_CURSOR_BLINK_SPEED	30

class TextInput : public TextOutput{
	protected:
		bool keyboardEnabled;
		int cursorIndex, cursorOffsetX, cursorOffsetY, cursorHeight, cursorBlinkSpeed, lastClickIndex, oldCursorIndex;
		int userCursorHeight, userCursorOffsetY;
		bool cursorBlinkState;
	
		virtual void wrapLines();
	
		virtual bool validChar(char newCheck);
	
		int calculateMaxTextLength();
		bool canInsert(char newCheck=' ');
		bool canInsert(string newCheck="");
	
		int getClosestLetterIndex(float x, float y);
		virtual void setCursorLocation(float x=-1, float y=-1);
	
		void setCursorFromCursor(string cursorID);
	
		virtual void onPress(string cursorID);
		virtual void onPressOff(string cursorID);
		virtual void onDragOff(string cursorID);
		virtual void onRelease(string cursorID);
	
		virtual int getCursorYOffset();
		virtual int getCursorHeight();
	
	public:
		TextInput();
		~TextInput();
	
		virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT, int elementID=-1);
		virtual void update();
		virtual void draw(float x, float y, float w, float h);
		virtual void draw(float x, float y, float w, float h, bool borders);
	
		virtual void appendCharacter(char newChar);
		virtual void appendString(string newString);
		virtual void eraseCharacter();	
	
		virtual int getGuiDisplayState();
	
		virtual bool getBool(string selector=DEFAULT_SELECTOR);
		virtual void setBool(bool value=false);
	
		virtual ShishaElement* setBoolValue(string selector=DEFAULT_SELECTOR, bool value=false);
		virtual ShishaElement* setStringValue(string selector=DEFAULT_SELECTOR, string value="");
	
		virtual void nextCursorIndex();
		virtual void previousCursorIndex();
		virtual void nextCursorYIndex();
		virtual void previousCursorYIndex();
		virtual void setCursorIndex(int index=-1);
		virtual void setDimensions(float width, float height);
		virtual void setKeyboardEnabled(bool keyboardEnabled);
		virtual void setPadding(float l=0.0, float t=0.0, float r=0.0, float b=0.0);
		virtual void setLabel(string label);
		virtual void setTheme(ShishaTheme* theme=NULL, ShishaTheme* doNotDelete=NULL, bool themeInherited=false);
		virtual void setUserCursorHeight(int userCursorHeight=0, int userCursorOffsetY=0);

		virtual bool checkKeyPressed(int key);
};

#endif
