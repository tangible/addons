/*
 *  BasicBar.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 09/04/10.
 *  Copyright 2010 Tangible Interaction. All rights reserved.
 *
 */

#include "BasicBar.h"

BasicBar::BasicBar(){
}

BasicBar::~BasicBar(){
}

void BasicBar::init(float x, float y, float width, float height, int elementID){
	ShishaContainer::init(x, y, width, height, elementID);
	this->elementType = SHISHA_TYPE_HOOKAH_BAR;
	this->slider = NULL;
	this->lockToSlider = false;
	this->setMinValue();
	this->setMaxValue();
	this->setTickInc();
	this->setTickLock(true);
	this->setDrawTicks();
	this->setCurrentValue(0.0);
	this->setInputEnabled(true);
}

int BasicBar::getGuiState(){
	if(this->getValue() > this->minValue)
		return (this->hasCursors()?GUI_ELEMENT_DISPLAY_STATE_ACTIVE_HOVER:GUI_ELEMENT_DISPLAY_STATE_ACTIVE);
	else
		return (this->hasCursors()?GUI_ELEMENT_DISPLAY_STATE_INACTIVE_HOVER:GUI_ELEMENT_DISPLAY_STATE_INACTIVE);
}

BasicButton* BasicBar::getSlider(){
	return this->slider;
}

void BasicBar::disableSlider(){
	if(this->slider != NULL){
		ShishaContainer::dropElement(this->slider);
		delete this->slider;
		this->slider = NULL;
		this->lockToSlider = false;
		if(this->elementType == SHISHA_TYPE_HOOKAH_SLIDER_BAR)
			this->elementType = SHISHA_TYPE_HOOKAH_BAR;
	}
}

void BasicBar::enableSlider(bool lockToSlider, int sliderShape){
	this->lockToSlider = lockToSlider;
	if(this->slider == NULL){
		this->slider = new BarSlider(this);
		float x = this->getX();
		float y = this->getY();
		float w = this->getWidth()*0.05;
		float h = this->getHeight();
		if(this->guiShape == GUI_ELEMENT_SHAPE_RECTANGLE){
			if(this->guiStyle == BAR_STYLE_VERTICAL){
				w = this->getWidth();
				h = this->getHeight()*0.05;
			}
			else{
				//default values are for horizontal rectangle
			}
		}
		else if(this->guiStyle == GUI_ELEMENT_SHAPE_CIRCLE){
			
		}
		this->slider->init(x, y, w, h);
		this->slider->setLabel("");
		this->slider->setName("Slider");
		this->slider->setGUIShape(sliderShape);
		this->addElement(this->slider, false, true);
		this->positionSlider();
		if(this->elementType == SHISHA_TYPE_HOOKAH_BAR)
			this->elementType = SHISHA_TYPE_HOOKAH_SLIDER_BAR;
	}
}

void BasicBar::positionSlider(){
	if(this->slider == NULL)
		return;
	float newX = 0;
	float newY = 0;
	float cPercent = this->getValueAsPercent();
	if(this->guiShape == GUI_ELEMENT_SHAPE_RECTANGLE){
		if(this->getGuiStyle() == BAR_STYLE_VERTICAL){
			newY = this->getY() + this->getHeight() - cPercent*this->getHeight() - this->slider->getHeight()/2.0;
			newX = this->getX() + (this->getWidth()-this->slider->getWidth())/2.0;
		}
		else{
			newX = this->getX() + cPercent*this->getWidth() - this->slider->getWidth()/2.0;
			newY = this->getY() + (this->getHeight()-this->slider->getHeight())/2.0;
		}
/**		if(newX < this->getX())
			newX = this->getX();
		else if(newX > (this->getX()+this->getWidth()-this->slider->getWidth()))
			newX = this->getX()+this->getWidth()-this->slider->getWidth();
		if(newY < this->getY())
			newY = this->getY();
		else if(newY > (this->getY()+this->getHeight()-this->slider->getHeight()))
			newY = this->getY()+this->getHeight()-this->slider->getHeight();*/
	}
	else if(this->guiShape == GUI_ELEMENT_SHAPE_CIRCLE){
	}
	this->slider->setPosition(newX, newY);
}

BasicBar::BarSlider::BarSlider(BasicBar* bar){
	this->bar = bar;
}

BasicBar::BarSlider::~BarSlider(){
}

bool BasicBar::BarSlider::doGlobalDrag(string cursorID){
	return (this->globalDragger == cursorID);
}

bool BasicBar::BarSlider::isHovered(string cursorID, float x, float y){
	return (this->doGlobalDrag(cursorID) || BasicButton::isHovered(cursorID, x, y));
}

void BasicBar::BarSlider::onDrag(string cursorID){
	BasicButton::onDrag(cursorID);
	this->bar->sliderDragged(cursorID, this->cursors.getX(cursorID), this->cursors.getY(cursorID));
}

void BasicBar::BarSlider::onPress(string cursorID){
	BasicButton::onPress(cursorID);
	if(this->globalDragger == "")
		this->globalDragger = cursorID;
}

void BasicBar::BarSlider::onRelease(string cursorID){
	BasicButton::onRelease(cursorID);
	if(this->globalDragger == cursorID)
		this->globalDragger = "";
}

void BasicBar::BarSlider::drawAsRectangle(float x, float y, float w, float h, bool borders){
	if(this->isActive())
		this->getTheme()->setColour(SHISHA_COLOUR_FOREGROUND_2, this);
	else
		this->getTheme()->setColour(SHISHA_COLOUR_FOREGROUND_1, this);
	ofRect(x, y, w, h);
	ShishaElement::drawAsRectangle(x, y, w, h, borders);
}

void BasicBar::BarSlider::drawAsCircle(float x, float y, float w, float h, bool borders){
	if(this->isActive())
		this->getTheme()->setColour(SHISHA_COLOUR_FOREGROUND_2, this);
	else
		this->getTheme()->setColour(SHISHA_COLOUR_FOREGROUND_1, this);
	float radius = w/2.0;
	ofCircle(x+radius, y+radius, radius);
	ShishaElement::drawAsCircle(x, y, w, h, borders);
}

void BasicBar::drawAsRectangle(float x, float y, float w, float h, bool borders){
	this->getTheme()->setColour(SHISHA_COLOUR_BACKGROUND, this);
	ofFill();
	ofRect(x, y, w, h);
	
	this->getTheme()->setColour(SHISHA_COLOUR_FOREGROUND_2, this);
	if(this->getGuiStyle() == BAR_STYLE_VERTICAL)
		ofRect(x, y+h-this->getValueAsPercent()*h, w, this->getValueAsPercent()*h);
	else
		ofRect(x, y, this->getValueAsPercent()*w, h);
	
	if(this->drawTicks && this->tickInc > 0.0){
		this->getTheme()->setColour(SHISHA_COLOUR_BORDER, this);
		float scale = ((this->getGuiStyle() == BAR_STYLE_VERTICAL)?h:w) / (float)this->getRangeSize();
		for(float tickI=this->minValue; tickI <= this->maxValue; tickI += this->tickInc){
			if(this->getGuiStyle() == BAR_STYLE_VERTICAL){
				float tickY = y + h - scale*tickI;
				ofLine(x, tickY, x+w*0.25, tickY);
			}
			else{
				float tickX = x + scale*tickI;
				ofLine(tickX, y+h*(1.0-0.25), tickX, y+h);
			}
		}
	}
	
	if(borders){
		this->getTheme()->setColour(SHISHA_COLOUR_BORDER, this);
		ofNoFill();
		ofRect(x, y, w, h);
		ofFill();
	}
}

void BasicBar::drawAsCircle(float x, float y, float w, float h, bool borders){
	float radius = w/2.0;
	float innerRadius = w/4.0;
	
	this->getTheme()->setColour(SHISHA_COLOUR_BACKGROUND, this);
	ofFill();
	ofCircle(x+radius, y+radius, radius);
	
	this->getTheme()->setColour(SHISHA_COLOUR_FOREGROUND_2, this);
	ofxCircleSlice(x+radius, y+radius, radius, 0.0, 360.0*this->getValueAsPercent());
	
	this->getTheme()->setColour(SHISHA_COLOUR_FOREGROUND_1, this);
	ofCircle(x+radius, y+radius, innerRadius);
	
	if(this->drawTicks && this->tickInc > 0.0){
		this->getTheme()->setColour(SHISHA_COLOUR_BORDER, this);
		float scale = 360.0 / (float)this->getRangeSize();
		for(float tickI=this->minValue; tickI <= this->maxValue; tickI += this->tickInc){
			float tickX1 = x+radius+tiAngleXComponent(scale*tickI, radius*0.75);
			float tickY1 = y+radius+tiAngleYComponent(scale*tickI, radius*0.75);
			float tickX2 = x+radius+tiAngleXComponent(scale*tickI, radius);
			float tickY2 = y+radius+tiAngleYComponent(scale*tickI, radius);
			ofLine(tickX1, tickY1, tickX2, tickY2);
		}
	}
	
	if(borders){
		this->getTheme()->setColour(SHISHA_COLOUR_BORDER, this);
		ofNoFill();
		ofCircle(x+radius, y+radius, radius);
		ofFill();
	}
}

void BasicBar::sliderDragged(string cursorID, float x, float y){
	if(this->slider != NULL)
		this->setValueFromInput(x, y);
}

void BasicBar::onDrag(string cursorID){
	this->onPress(cursorID);
}

void BasicBar::onDragOn(string cursorID){
	this->onPress(cursorID);
}

void BasicBar::onPress(string cursorID){
	if(this->inputEnabled && !this->lockToSlider)
		this->setValueFromInput(this->cursors.getX(cursorID), this->cursors.getY(cursorID));
}

void BasicBar::onMove(float xMove, float yMove){
	this->positionSlider();
}

float BasicBar::getTickLockedValue(float value){
	if(!this->tickLock || this->tickInc <= 0.0)
		return value;
	float tick;
	for(tick = this->minValue; tick <= this->maxValue; tick += this->tickInc){
		if(tick + this->tickInc/2.0 > value)
			break;
	}
	if((tick + this->tickInc/2.0) >= this->maxValue)
		tick = this->maxValue;
	return tick;
}

float BasicBar::getRangeSize(){
	return (this->maxValue-this->minValue);
}

float BasicBar::getValue(){
	return this->cValue;
}

float BasicBar::getValueAsPercent(){
	float rangeSize = this->getRangeSize();
	return ((rangeSize == 0.0)?0.0:((this->cValue-this->minValue)/rangeSize));
}

float BasicBar::nextTick(){
	this->setCurrentValue(this->getValue()+this->tickInc);
	return this->getValue();
}

float BasicBar::prevTick(){
	this->setCurrentValue(this->getValue()-this->tickInc);
	return this->getValue();
}

void BasicBar::setValueFromInput(float x, float y){
	if(!this->inputEnabled || this->maxValue <= this->minValue)
		return;
	float localX = x - this->getX();
	float localY = y - this->getY();
	if(this->guiShape == GUI_ELEMENT_SHAPE_RECTANGLE)
		this->setValueFromInputRectangle(localX, localY);
	else if(this->guiShape == GUI_ELEMENT_SHAPE_CIRCLE)
		this->setValueFromInputCircle(localX, localY);
}

void BasicBar::setValueFromInputRectangle(float x, float y){
	if(this->getGuiStyle() == BAR_STYLE_VERTICAL)
		this->setCurrentValueByPercent(1.0-y/this->getHeight());
	else
		this->setCurrentValueByPercent(x/this->getWidth());
}

void BasicBar::setValueFromInputCircle(float x, float y){
	float midX = this->getWidth()/2.0;
	float midY = this->getHeight()/2.0;
	this->setCurrentValueByPercent(tiNormalizeAngle(tiRadiansToAngle(tiAngle(midX, midY, x, y)))/360.0);
}

void BasicBar::setCurrentValue(float cValue){
	if(cValue < this->minValue)
		cValue = this->minValue;
	if(cValue > this->maxValue)
		cValue = this->maxValue;
	if(this->tickLock)
		cValue = this->getTickLockedValue(cValue);
	this->cValue = cValue;
	this->positionSlider();
}

void BasicBar::setCurrentValueByPercent(float valuePercent){		
	if(valuePercent < 0.0) valuePercent = 0.0;
	if(valuePercent > 1.0) valuePercent = 1.0;
	this->setCurrentValue(this->minValue+this->getRangeSize()*valuePercent);
}

void BasicBar::setDrawTicks(bool drawTicks){
	this->drawTicks = drawTicks;
}

void BasicBar::setInputEnabled(bool inputEnabled){
	this->inputEnabled = inputEnabled;
}

void BasicBar::setMinValue(float minValue){
	this->minValue = minValue;
}

void BasicBar::setMaxValue(float maxValue){
	this->maxValue = maxValue;
}

void BasicBar::setTickCount(int tickCount){
	if(tickCount > 0.0)
		this->setTickInc(this->getRangeSize()/(float)tickCount);
}

void BasicBar::setTickInc(float tickInc){
	this->tickInc = tickInc;
}

void BasicBar::setTickLock(bool tickLock){
	this->tickLock = tickLock;
}

ShishaElement* BasicBar::setIntValue(string selector, int value){
	if(this->selectSelf(selector))
		this->setCurrentValue((float)value);
	else if(selector == "minValue")
		this->setMinValue((float)value);
	else if(selector == "maxValue")
		this->setMaxValue((float)value);
}

ShishaElement* BasicBar::getElement(string selector, string& subSelector){
	if(selector == "slider")
		return this->getSlider();
	return ShishaContainer::getElement(selector, subSelector);
}


float BasicBar::getFloat(string selector){
	if(this->selectSelf(selector))
		return this->getValue();
	else if(selector == "percent")
		return this->getValueAsPercent();
}

int BasicBar::getInt(string selector){
	if(this->selectSelf(selector))
		return (int)this->getValue();
	else if(selector == "minValue")
		return (int)this->minValue;
	else if(selector == "maxValue")
		return (int)this->maxValue;
}
