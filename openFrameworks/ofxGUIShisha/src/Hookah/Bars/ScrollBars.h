/*
 *  ScrollBars.h
 *  emptyExample
 *
 *  Created by Pat Long (plong0) on 30/09/10.
 *  Copyright 2010 Tangible Interaction. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_SCROLL_BARS
#define _OFX_GUISHISHA_SCROLL_BARS

#include "ShishaElement.h"
#include "ShishaContainer.h"
#include "BasicButton.h"
#include "BasicBar.h"

#define SHISHA_DEFAULT_SCROLLBAR_WIDTH		15
#define SHISHA_DEFAULT_SCROLLBAR_INCREMENT	1.0

class ScrollBar : public ShishaContainer{
protected:	
	class ScrollBarArea : public BasicBar{
	protected:
		virtual void drawAsRectangle(float x, float y, float w, float h, bool borders);
		
	public:
		ScrollBarArea();
		~ScrollBarArea();
	};
	
	ShishaElement* scrollElement;
	ScrollBarArea* scrollArea;
	float scrollPercent;
	
	virtual float getScrollAreaPercent(float fromPercent=-1.0);
	
public:
	ScrollBar(ShishaElement* scrollElement=NULL);
	~ScrollBar();
	
	virtual bool initScrollBar(float scrollBarWidth=SHISHA_DEFAULT_SCROLLBAR_WIDTH);
	virtual void update();
	virtual void draw(float x, float y, float w, float h, bool borders);
	
	virtual ShishaElement* setFloatValue(string selector=DEFAULT_SELECTOR, float value=0.0);
	virtual void setFloat(float value=0.0);
	
	virtual void setScrollPercent(float scrollPercent, bool updateScrollArea=true);
	virtual void setScrollIncrement(float scrollIncrement=SHISHA_DEFAULT_SCROLLBAR_INCREMENT);
};

class ScrollBarHorizontal : public ScrollBar{
protected:
	
	virtual void onPress(string cursorID);
	
public:
	ScrollBarHorizontal(ShishaElement* scrollElement=NULL);
	~ScrollBarHorizontal();
	
	virtual bool initScrollBar(float scrollBarWidth=SHISHA_DEFAULT_SCROLLBAR_WIDTH);
	virtual void update();
	
	virtual void setScrollPercent(float scrollPercent, bool updateScrollArea=true);
};

class ScrollBarVertical : public ScrollBar{
protected:
	
	virtual float getScrollAreaPercent(float fromPercent=-1.0);
	
	virtual void onPress(string cursorID);
	
public:
	ScrollBarVertical(ShishaElement* scrollElement=NULL);
	~ScrollBarVertical();
	
	virtual bool initScrollBar(float scrollBarWidth=SHISHA_DEFAULT_SCROLLBAR_WIDTH);
	virtual void update();
	
	virtual void setScrollPercent(float scrollPercent, bool updateScrollArea=true);
};


class ShishaScrollBars : public ShishaContainer{
protected:
	ShishaElement* scrollElement;
	
public:
	ShishaScrollBars();
	~ShishaScrollBars();
	virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT, int elementID=-1);
	
	virtual void draw(float x, float y, float w, float h, bool borders);
	
	virtual float getWidth();
	virtual float getHeight();
	
	bool setScrollElement(ShishaElement* scrollElement, bool scrollHorizontal=true, bool scrollVertical=true, float scrollBarWidth=SHISHA_DEFAULT_SCROLLBAR_WIDTH);
};

#endif
