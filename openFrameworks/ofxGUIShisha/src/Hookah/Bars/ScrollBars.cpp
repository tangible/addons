/*
 *  ScrollBars.cpp
 *  emptyExample
 *
 *  Created by Pat Long (plong0) on 30/09/10.
 *  Copyright 2010 Tangible Interaction. All rights reserved.
 *
 */

#include "ScrollBars.h"


ScrollBar::ScrollBar(ShishaElement* scrollElement){
	this->scrollElement = scrollElement;
}

ScrollBar::~ScrollBar(){
}

bool ScrollBar::initScrollBar(float scrollBarWidth){
	this->scrollPercent = 0.0;
	return true;
}

void ScrollBar::update(){
	ShishaContainer::update();
	if(this->scrollArea != NULL){
		float newPercent = this->getScrollAreaPercent();
		if(this->scrollPercent != newPercent)
			this->setScrollPercent(newPercent, false);
	}
}

void ScrollBar::draw(float x, float y, float w, float h, bool borders){
	ShishaContainer::draw(x, y, w, h, true);
}

ShishaElement* ScrollBar::setFloatValue(string selector, float value){
	if(selector == "increment"){
		this->setScrollIncrement(value);
		return this;
	}
	return ShishaContainer::setFloatValue(selector, value);
}

void ScrollBar::setFloat(float value){
	this->setScrollPercent(value);
}

float ScrollBar::getScrollAreaPercent(float fromPercent){
	if(fromPercent == -1.0){
		if(this->scrollArea != NULL)
			return this->scrollArea->getFloat("percent")*100.0;
	}
	else if(fromPercent >= 0.0 && fromPercent <= 100.0){
		return fromPercent/100.0;
	}
	
	return 0.0;
}

void ScrollBar::setScrollPercent(float scrollPercent, bool updateScrollArea){
	if(updateScrollArea && this->scrollArea != NULL){
		this->scrollArea->setCurrentValueByPercent(this->getScrollAreaPercent(scrollPercent));
		scrollPercent = this->getScrollAreaPercent();
	}
	this->scrollPercent = scrollPercent;
}

void ScrollBar::setScrollIncrement(float scrollIncrement){
	if(this->scrollArea != NULL)
		this->scrollArea->setTickInc(scrollIncrement);
}

ScrollBar::ScrollBarArea::ScrollBarArea(){
}

ScrollBar::ScrollBarArea::~ScrollBarArea(){
}

void ScrollBar::ScrollBarArea::drawAsRectangle(float x, float y, float w, float h, bool borders){
	// don't draw the bar at all (besides the slider). we use the bar to manage the slider and tick-locking
}


ScrollBarHorizontal::ScrollBarHorizontal(ShishaElement* scrollElement):ScrollBar(scrollElement){
}

ScrollBarHorizontal::~ScrollBarHorizontal(){
}

bool ScrollBarHorizontal::initScrollBar(float scrollBarWidth){
	ScrollBar::initScrollBar(scrollBarWidth);
	if(this->scrollElement != NULL){
		ShishaContainer::init(scrollElement->getX(), scrollElement->getY()+scrollElement->getHeight(), scrollElement->getWidth(), scrollBarWidth);
		
		BasicButton* newButton;
		newButton = new BasicButton();
		newButton->init(this->getX(), this->getY(), scrollBarWidth, scrollBarWidth);
		newButton->setName("left");
		newButton->setLabel("<");
		this->addElement(newButton, false);
		
		newButton = new BasicButton();
		newButton->init(this->getX()+this->getWidth()-scrollBarWidth, this->getY(), scrollBarWidth, scrollBarWidth);
		newButton->setName("right");
		newButton->setLabel(">");
		this->addElement(newButton, false);
		
		float sliderWidth = scrollBarWidth;
		float sliderHeight = scrollBarWidth;
		
		this->scrollArea = new ScrollBarArea();
		this->scrollArea->init(this->getX()+scrollBarWidth+sliderWidth/2.0, this->getY(), this->getWidth()-scrollBarWidth*2.0-sliderWidth, scrollBarWidth);
		this->scrollArea->setGUIStyle(BAR_STYLE_HORIZONTAL);
		this->scrollArea->setTickInc(SHISHA_DEFAULT_SCROLLBAR_INCREMENT);
		this->scrollArea->setDrawTicks(false);
		this->scrollArea->enableSlider();
		this->addElement(this->scrollArea, false);
		
		BasicButton* slider = this->scrollArea->getSlider();
		slider->setDimensions(sliderWidth, sliderHeight);
		
		this->setScrollPercent(0.0);
		
		return true;
	}
	return false;
}

void ScrollBarHorizontal::update(){
	ScrollBar::update();
	if(this->scrollArea != NULL){
		if(this->getBool("left"))
			this->scrollArea->prevTick();
		if(this->getBool("right"))
			this->scrollArea->nextTick();
	}
}

void ScrollBarHorizontal::onPress(string cursorID){
	if(this->scrollArea != NULL){
		float x = this->cursors.getX(cursorID);
		float y = this->cursors.getY(cursorID);
		if(x >= this->getX()+this->getHeight() && x <= this->scrollArea->getX())
			this->setScrollPercent(0.0);
		else if(x >= this->scrollArea->getX()+this->scrollArea->getWidth() && x <= this->getX()+this->getWidth()-this->getHeight())
			this->setScrollPercent(100.0);
	}
}

void ScrollBarHorizontal::setScrollPercent(float scrollPercent, bool updateScrollArea){
	ScrollBar::setScrollPercent(scrollPercent, updateScrollArea);
	if(this->scrollElement != NULL){
		this->scrollElement->setScrollOffsetX((this->scrollElement->getContentWidth()-this->scrollElement->getWidth()+15.0)*(this->scrollPercent/100.0));
	}
}


ScrollBarVertical::ScrollBarVertical(ShishaElement* scrollElement):ScrollBar(scrollElement){
}

ScrollBarVertical::~ScrollBarVertical(){
}

bool ScrollBarVertical::initScrollBar(float scrollBarWidth){
	ScrollBar::initScrollBar(scrollBarWidth);
	if(this->scrollElement != NULL){
		ShishaContainer::init(scrollElement->getX()+scrollElement->getWidth(), scrollElement->getY(), scrollBarWidth, scrollElement->getHeight());
		
		BasicButton* newButton;
		newButton = new BasicButton();
		newButton->init(this->getX(), this->getY(), scrollBarWidth, scrollBarWidth);
		newButton->setName("up");
		newButton->setLabel("/\\");
		this->addElement(newButton, false);
		
		newButton = new BasicButton();
		newButton->init(this->getX(), this->getY()+this->getHeight()-scrollBarWidth, scrollBarWidth, scrollBarWidth);
		newButton->setName("down");
		newButton->setLabel("\\/");
		this->addElement(newButton, false);
		
		float sliderWidth = scrollBarWidth;
		float sliderHeight = scrollBarWidth;
		
		this->scrollArea = new ScrollBarArea();
		this->scrollArea->init(this->getX(), this->getY()+scrollBarWidth+sliderHeight/2.0, scrollBarWidth, this->getHeight()-scrollBarWidth*2.0-sliderHeight);
		this->scrollArea->setGUIStyle(BAR_STYLE_VERTICAL);
		this->scrollArea->setTickInc(SHISHA_DEFAULT_SCROLLBAR_INCREMENT);
		this->scrollArea->setDrawTicks(false);
		this->scrollArea->enableSlider();
		this->addElement(this->scrollArea, false);
		
		BasicButton* slider = this->scrollArea->getSlider();
		slider->setDimensions(sliderWidth, sliderHeight);
		
		this->setScrollPercent(0.0);
		
		return true;
	}
	return false;
}

void ScrollBarVertical::update(){
	ScrollBar::update();
	if(this->scrollArea != NULL){
		if(this->getBool("up"))
			this->scrollArea->nextTick();
		if(this->getBool("down"))
			this->scrollArea->prevTick();
	}
}

void ScrollBarVertical::onPress(string cursorID){
	if(this->scrollArea != NULL){
		float x = this->cursors.getX(cursorID);
		float y = this->cursors.getY(cursorID);
		if(y >= this->getY()+this->getWidth() && y <= this->scrollArea->getY())
			this->setScrollPercent(0.0);
		else if(y >= this->scrollArea->getY()+this->scrollArea->getHeight() && y <= this->getY()+this->getHeight()-this->getWidth())
			this->setScrollPercent(100.0);
	}
}

void ScrollBarVertical::setScrollPercent(float scrollPercent, bool updateScrollArea){
	ScrollBar::setScrollPercent(scrollPercent, updateScrollArea);
	if(this->scrollElement != NULL){
		this->scrollElement->setScrollOffsetY((this->scrollElement->getContentHeight()-this->scrollElement->getHeight()+15)*(this->scrollPercent/100.0));
	}
}

float ScrollBarVertical::getScrollAreaPercent(float fromPercent){
	if(fromPercent == -1.0){
		if(this->scrollArea != NULL)
			return (100.0-this->scrollArea->getFloat("percent")*100.0);
	}
	else if(fromPercent >= 0.0 && fromPercent <= 100.0){
		return (100.0-fromPercent)/100.0;
	}
	
	return 0.0;
}


ShishaScrollBars::ShishaScrollBars(){
	this->scrollElement = NULL;
}

ShishaScrollBars::~ShishaScrollBars(){
}

void ShishaScrollBars::init(float x, float y, float width, float height, int elementID){
	ShishaContainer::init(x, y, width, height, elementID);
	this->setName("scrollBars");
}

void ShishaScrollBars::draw(float x, float y, float w, float h, bool borders){
	this->drawChildren();
}

float ShishaScrollBars::getWidth(){
	float scrollBarWidth = 0.0;
	
	ShishaElement* verticalBar = this->selectElement("vertical");
	if(verticalBar != NULL)
		scrollBarWidth = verticalBar->getWidth();
	
	return scrollBarWidth;
}

float ShishaScrollBars::getHeight(){
	float scrollBarHeight = 0.0;
	
	ShishaElement* horizontalBar = this->selectElement("horizontal");
	if(horizontalBar != NULL)
		scrollBarHeight = horizontalBar->getHeight();
	
	return scrollBarHeight;
}

bool ShishaScrollBars::setScrollElement(ShishaElement* scrollElement, bool scrollHorizontal, bool scrollVertical, float scrollBarWidth){
	this->scrollElement = scrollElement;
	if(this->scrollElement != NULL && scrollBarWidth > 0.0 && (scrollHorizontal || scrollVertical)){
		ShishaContainer::init(scrollElement->getX(), scrollElement->getY(), scrollElement->getWidth(), scrollElement->getHeight());
		this->elementType = SHISHA_TYPE_SCROLLBARS;
		
		bool hasHorizontal = false;
		bool hasVertical = false;
		
		ScrollBar* newScrollBar;
		
		if(scrollHorizontal){
			newScrollBar = new ScrollBarHorizontal(scrollElement);
			hasHorizontal = newScrollBar->initScrollBar(scrollBarWidth);
			if(hasHorizontal){
				newScrollBar->setName("horizontal");
				this->addElement(newScrollBar, false);
			}
			else{
				delete newScrollBar;
				newScrollBar = NULL;
			}
		}
		
		if(scrollVertical){
			newScrollBar = new ScrollBarVertical(scrollElement);
			hasVertical = newScrollBar->initScrollBar(scrollBarWidth);
			if(hasVertical){
				newScrollBar->setName("vertical");
				this->addElement(newScrollBar, false);
			}
			else{
				delete newScrollBar;
				newScrollBar = NULL;
			}
		}
		
		if((scrollHorizontal && hasHorizontal) || (scrollVertical && hasVertical)){
			this->scrollElement->enableScrolling(this);
			return true;
		}
	}
	return false;
}
