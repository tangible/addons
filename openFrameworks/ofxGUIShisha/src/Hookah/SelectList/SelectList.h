/*
 *  SelectList.h
 *  emptyExample
 *
 *  Created by Pat Long (plong0) on 26/09/10.
 *  Copyright 2010 Tangible Interaction. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_SELECT_LIST
#define _OFX_GUISHISHA_SELECT_LIST

#include "ShishaContainer.h"
#include "ScrollBars.h"
#include "TextOutput.h"

class SelectList;

class SelectListOption : public TextOutput{
protected:
	SelectList* parentList;
	bool selected;
	
	virtual string getOutputValue();
	virtual void drawWrapped(float x, float y, float w, float h);
	
	virtual void onDragOn(string cursorID);	
	virtual void onPress(string cursorID);
	
public:
	SelectListOption(SelectList* parentList=NULL);
	~SelectListOption();
	
	virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT, int elementID=-1);
	virtual void draw(float x, float y, float w, float h, bool borders);
	
	bool isSelected();
	
	virtual int getInt(string selector=DEFAULT_SELECTOR);
	virtual float getFloat(string selector=DEFAULT_SELECTOR);
	
	virtual void setSelected(bool selected, bool updateParent=false);
};

class SelectList : public ShishaContainer{
protected:
	vector<SelectListOption*> options;
	SelectListOption* selected;
	int toggleOutFrame;
	float toggleOutHeight;
	
	virtual void drawChildren();
	
	virtual void onDragOn(string cursorID);	
	virtual void onPress(string cursorID);
	virtual void onPressOff(string cursorID);
	
	virtual bool checkChildrenCursorHover(int x, int y, string cursorID, float w=1.0, float h=1.0);
	virtual bool checkChildrenCursorDrag(int x, int y, int button, string cursorID, float w=1.0, float h=1.0);
	virtual bool checkChildrenCursorPress(int x, int y, int button, string cursorID, float w=1.0, float h=1.0);
	virtual bool checkChildrenCursorRelease(int x, int y, int button, string cursorID, float w=1.0, float h=1.0);	
	
	virtual bool checkChildrenKeyPressed(int key);
	virtual bool checkChildrenKeyReleased(int key);
	
	int getSelectedIndex();
	virtual float calculateContentHeight();
	
public:
	SelectList();
	~SelectList();
	
	virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT, int elementID=-1);
	virtual void update();
	virtual void draw(float x, float y, float w, float h, bool borders);
	
	virtual ShishaElement* addElement(ShishaElement* element, bool updateLocation=true, bool inheritTheme=true);
	virtual ShishaElement* dropElement(ShishaElement* element, bool doDelete=false);
	
	virtual void addOption(string optionText, string optionValue="");
	virtual void addOptionRange(float minValue, float maxValue, float increment=1.0, bool asFloats=true);
	
	virtual SelectListOption* selectOption(SelectListOption* option, bool checkToggled=false);
	
	virtual bool canSelect();
	virtual bool isToggledOut();
	
	virtual void toggleIn();
	virtual void toggleOut();
	
	virtual int getInt(string selector=DEFAULT_SELECTOR);
	virtual float getFloat(string selector=DEFAULT_SELECTOR);
	virtual string getString(string selector=DEFAULT_SELECTOR);

	virtual void setInt(int value=0);
	virtual void setFloat(float value=0.0);
	virtual void setString(string value="");
	
};

#endif
