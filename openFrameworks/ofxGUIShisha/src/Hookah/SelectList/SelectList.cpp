/*
 *  SelectList.cpp
 *  emptyExample
 *
 *  Created by Pat Long (plong0) on 26/09/10.
 *  Copyright 2010 Tangible Interaction. All rights reserved.
 *
 */

#include "SelectList.h"

SelectListOption::SelectListOption(SelectList* parentList){
	this->parentList = parentList;
}

SelectListOption::~SelectListOption(){
}

void SelectListOption::init(float x, float y, float width, float height, int elementID){
	TextOutput::init(x, y, width, height, elementID);
	this->elementType = SHISHA_TYPE_HOOKAH_SELECT_LIST_OPTION;
	this->setPadding(2.0, 1.0+height/3.0);
	this->selected = false;
}

void SelectListOption::draw(float x, float y, float w, float h, bool borders){
	if(borders){
		// only draw the background if we're also drawing borders
		this->getTheme()->setColour(SHISHA_COLOUR_BACKGROUND, this);
		ofFill();
		ofRect(x, y, w, h);
		
		this->getTheme()->setColour(SHISHA_COLOUR_BORDER, this);	
		ofNoFill();
		ofRect(x, y, w, h);
		ofFill();
	}
	
	this->getTheme()->setColour(SHISHA_COLOUR_FOREGROUND_1, this);
	if(this->enableWrapping)
		this->drawWrapped(x, y, w, h);
	else
		this->drawUnwrapped(x, y, w, h);
	
	if(this->isSelected() && this->parentList != NULL && this->parentList->isToggledOut()){
		this->getTheme()->setColour(SHISHA_COLOUR_FOREGROUND_2, this);
		ofNoFill();
		ofRect(x, y, w, h);
		ofFill();
	}
	
	this->getTheme()->setColour();
}

void SelectListOption::drawWrapped(float x, float y, float w, float h){
	string label = this->getLabel();
	if(label != ""){
		this->getTheme()->drawText(label, x+this->getPaddingL(), y+this->getPaddingT(), false);
	}
	
	
//	string oldLabel = this->getLabel();
//	this->setLabel("");
/**	string oldValue = this->getString();
	this->setString("");
	TextOutput::drawWrapped(x, y, w, h);
	this->setString(oldValue);*/
//	this->setLabel(oldLabel);
}

string SelectListOption::getOutputValue(){
	return this->getLabel();
}
	   
bool SelectListOption::isSelected(){
	return this->selected;
}

int SelectListOption::getInt(string selector){
	int result;
	if(!from_string(result, this->getString(), std::dec))
		result = -1;
	return result;
}

float SelectListOption::getFloat(string selector){
	float result;
	if(!from_string(result, this->getString(), std::dec))
		result = -1.0;
	return result;
}

void SelectListOption::setSelected(bool selected, bool updateParent){
	if(selected && updateParent && this->parentList != NULL)
		this->selected = (this->parentList->selectOption(this, true) == this);
	else
		this->selected = selected;
}

void SelectListOption::onDragOn(string cursorID){
	TextOutput::onDragOn(cursorID);

/**	if(this->parentList != NULL){
		if(this->parentList->canSelect())
			this->setSelected(true, true);
	}*/
}

void SelectListOption::onPress(string cursorID){
	TextOutput::onPress(cursorID);
	
	if(this->parentList != NULL){
		if(this->parentList->canSelect())
			this->setSelected(true, true);
	}
}


SelectList::SelectList(){
	this->selected = NULL;
}

SelectList::~SelectList(){
}

void SelectList::init(float x, float y, float width, float height, int elementID){
	ShishaContainer::init(x, y, width, height, elementID);
	this->elementType = SHISHA_TYPE_HOOKAH_SELECT_LIST;
	this->setCropBounds(true);
	this->setVerticalSpacing(0.0);
	this->setHorizontalSpacing(0.0);
	this->selected = NULL;
	this->toggleOutFrame = -1;
	this->toggleOutHeight = height;
	this->toggleIn();
}

float SelectList::calculateContentHeight(){
	return ((float)this->options.size())*(20.0+this->verticalSpacing);
}

void SelectList::update(){
	ShishaContainer::update();
	
	float contentHeight = this->getContentHeight();
	
	if(this->isToggledOut() && contentHeight > this->getHeight() && !this->scrollingEnabled){
		ShishaScrollBars* newScrollBars = new ShishaScrollBars();
		if(!newScrollBars->setScrollElement(this, false, true)){
			delete newScrollBars;
			newScrollBars = NULL;
		}
		else{
			float optionSize = (20.0/contentHeight)*100.0;
			newScrollBars->setFloatValue("vertical.increment", optionSize);
			newScrollBars->setFloatValue("vertical", ((float)this->getSelectedIndex()/(float)this->options.size())*100.0);
		}
	}
	else if((!this->isToggledOut() || contentHeight <= this->getHeight()) && this->scrollingEnabled){
		this->disableScrolling();
	}
}

ShishaElement* SelectList::addElement(ShishaElement* element, bool updateLocation, bool inheritTheme){
	if(element->getElementType() == SHISHA_TYPE_HOOKAH_SELECT_LIST_OPTION){	
		SelectListOption* newOption = (SelectListOption*)element;
		this->options.push_back(newOption);
		if(this->selected == NULL)
			this->selectOption(newOption);
			
	}
	return ShishaContainer::addElement(element, updateLocation, inheritTheme);;
}

ShishaElement* SelectList::dropElement(ShishaElement* element, bool doDelete){
	if(element->getElementType() == SHISHA_TYPE_HOOKAH_SELECT_LIST_OPTION){	
		SelectListOption* dropOption = (SelectListOption*)element;
		for(int i=0; i < this->options.size(); i++){
			if(options[i] == dropOption){
				options.erase(options.begin()+i);
				break;
			}
		}
	}
	return ShishaContainer::dropElement(element, doDelete);
}

void SelectList::addOption(string optionText, string optionValue){
	if(optionValue == "")
		optionValue = optionText;
	
	int optionNum = this->options.size();
	float optionHeight = 20.0+this->verticalSpacing;
	
	stringstream nameBuilder;
	nameBuilder << "option_" << optionNum;
	
	SelectListOption* newOption = new SelectListOption(this);
	newOption->init(this->getX()+1.0, this->verticalSpacing+this->getY()+optionNum*optionHeight, this->getWidth()-2.0, optionHeight);
	newOption->setName(nameBuilder.str());
	nameBuilder.str("");
	newOption->setLabel(optionText);
	newOption->setString(optionValue);
	
	this->addElement(newOption, false);
}

void SelectList::addOptionRange(float minValue, float maxValue, float increment, bool asFloats){
	if(asFloats){
		for(float value = minValue; value <= maxValue; value += increment){
			this->addOption(ofToString(value, 2.0));
		}
	}
	else{
		if((int)increment < 1)
			increment = 1.0;
		for(int value = (int)minValue; value <= (int)maxValue; value += (int)increment){
			this->addOption(ofToString((int)value, 0));
		}
	}
}

SelectListOption* SelectList::selectOption(SelectListOption* option, bool checkToggled){
	if(option != this->selected && (!checkToggled || this->canSelect())){
		if(this->selected != NULL)
			this->selected->setSelected(false);
		this->selected = option;
		this->selected->setSelected(true);
	}
	if(this->isToggledOut())
		this->toggleIn();
	return this->selected;
}

void SelectList::draw(float x, float y, float w, float h, bool borders){
	ShishaContainer::draw(x+1.0, y+1.0, w-2.0, h-2.0, true);
}

void SelectList::drawChildren(){
	if(!this->isToggledOut() && this->selected != NULL){
		((ShishaElement*)this->selected)->draw(this->getX()+1.0, this->getY());
	}
	else{
		ShishaContainer::drawChildren();
		((ShishaElement*)this->selected)->draw(); // make sure selected gets drawn on top of everything else
	}
}

bool SelectList::checkChildrenCursorHover(int x, int y, string cursorID, float w, float h){
	if(!this->isToggledOut() && this->selected != NULL)
		return ((ShishaElement*)this->selected)->checkCursorHover(x, y, cursorID, w, h);
	else
		return ShishaContainer::checkChildrenCursorHover(x, y, cursorID, w, h);
}

bool SelectList::checkChildrenCursorDrag(int x, int y, int button, string cursorID, float w, float h){
	if(!this->isToggledOut() && this->selected != NULL)
		return ((ShishaElement*)this->selected)->checkCursorDrag(x, y, button, cursorID, w, h);
	else
		return ShishaContainer::checkChildrenCursorDrag(x, y, button, cursorID, w, h);
}

bool SelectList::checkChildrenCursorPress(int x, int y, int button, string cursorID, float w, float h){
	if(!this->isToggledOut() && this->selected != NULL){
		return ((ShishaElement*)this->selected)->checkCursorPress(x, y, button, cursorID, w, h);
	}
	else{
		return ShishaContainer::checkChildrenCursorPress(x, y, button, cursorID, w, h);
	}
}

bool SelectList::checkChildrenCursorRelease(int x, int y, int button, string cursorID, float w, float h){
	if(!this->isToggledOut() && this->selected != NULL)
		return ((ShishaElement*)this->selected)->checkCursorRelease(x, y, button, cursorID, w, h);
	else
		return ShishaContainer::checkChildrenCursorRelease(x, y, button, cursorID, w, h);
}

bool SelectList::checkChildrenKeyPressed(int key){
	if(!this->isToggledOut() && this->selected != NULL)
		return ((ShishaElement*)this->selected)->checkKeyPressed(key);
	else
		return ShishaContainer::checkChildrenKeyPressed(key);
}

bool SelectList::checkChildrenKeyReleased(int key){
	if(!this->isToggledOut() && this->selected != NULL)
		return ((ShishaElement*)this->selected)->checkKeyReleased(key);
	else
		return ShishaContainer::checkChildrenKeyReleased(key);
}


bool SelectList::canSelect(){
	return (this->selected == NULL || (this->isToggledOut() && this->toggleOutFrame != ofGetFrameNum()));
}

bool SelectList::isToggledOut(){
	return (this->toggleOutFrame != -1);
}

void SelectList::toggleIn(){
	this->toggleOutFrame = -1;
	this->toggleOutHeight = this->getHeight();
	this->setHeight(20.0);
}

void SelectList::toggleOut(){
	this->toggleOutFrame = ofGetFrameNum();
	this->setHeight(this->toggleOutHeight);
}

void SelectList::onDragOn(string cursorID){
	ShishaContainer::onDragOn(cursorID);
	if(!this->isToggledOut())
		this->toggleOut();
}

void SelectList::onPress(string cursorID){
	ShishaContainer::onPress(cursorID);
	if(!this->isToggledOut())
		this->toggleOut();
}

void SelectList::onPressOff(string cursorID){
	ShishaContainer::onPressOff(cursorID);
/**	float x = this->cursors.getX(cursorID);
	float y = this->cursors.getY(cursorID);
	float w = this->cursors.getWidth(cursorID);
	float h = this->cursors.getHeight(cursorID);
	int button = this->cursors.getButton(cursorID);
	if(this->scrollBars != NULL)
		cout << "here:" << (this->scrollBars->checkCursorPress(x, y, button, cursorID, w, h)?"sbPress":"sbNotPress") << ":" << endl;
//	if(this->scrollBars != NULL && !this->scrollBars->checkCursorPress(x, y, button, cursorID, w, h)){
 */
		if(this->isToggledOut())
			this->toggleIn();
//	}
}

int SelectList::getSelectedIndex(){
	for(int i=0; i < this->options.size(); i++){
		if(this->options[i] == this->selected)
			return i;
	}
	return -1;
}

int SelectList::getInt(string selector){
	if(this->selectSelf(selector) && this->selected != NULL)
		return this->selected->getInt();
	else if(selector == "index")
		return this->getSelectedIndex();
	return ShishaContainer::getInt(selector);
}

float SelectList::getFloat(string selector){
	if(this->selectSelf(selector) && this->selected != NULL)
		return this->selected->getFloat();
	else if(selector == "index")
		return (float)this->getSelectedIndex();
	return ShishaContainer::getFloat(selector);
}

string SelectList::getString(string selector){
	if(this->selectSelf(selector) && this->selected != NULL)
		return this->selected->getString();
	else if(selector == "index"){
		stringstream valueBuilder;
		valueBuilder << this->getSelectedIndex();
		return valueBuilder.str();
	}
	return ShishaContainer::getString(selector);
}

void SelectList::setInt(int value){
	for(int i=0; i < this->options.size(); i++){
		if(this->options[i]->getInt() == value){
			this->selectOption(this->options[i]);
			break;
		}	
	}
}

void SelectList::setFloat(float value){
	for(int i=0; i < this->options.size(); i++){
		if(this->options[i]->getFloat() == value){
			this->selectOption(this->options[i]);
			break;
		}	
	}
}

void SelectList::setString(string value){
	for(int i=0; i < this->options.size(); i++){
		if(this->options[i]->getString() == value){
			this->selectOption(this->options[i]);
			break;
		}	
	}
}
