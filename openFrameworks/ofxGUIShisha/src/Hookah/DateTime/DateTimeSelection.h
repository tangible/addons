/*
 *  DateTimeSelection.h
 *  emptyExample
 *
 *  Created by Pat Long (plong0) on 29/09/10.
 *  Copyright 2010 Tangible Interaction. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_DATETIME_SELECTION
#define _OFX_GUISHISHA_DATETIME_SELECTION

#include "ofxDateTime.h"
#include "ShishaContainer.h"
#include "SelectList.h"

class DateTimeSelection : public ShishaContainer{
protected:
	ofxDateTime dateTime;
	
public:
	DateTimeSelection();
	~DateTimeSelection();
	
	virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT, int elementID=-1);
};

#endif
