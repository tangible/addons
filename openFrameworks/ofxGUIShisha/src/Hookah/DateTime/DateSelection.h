/*
 *  DateSelection.h
 *  emptyExample
 *
 *  Created by Pat Long (plong0) on 29/09/10.
 *  Copyright 2010 Tangible Interaction. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_DATE_SELECTION
#define _OFX_GUISHISHA_DATE_SELECTION

#include "ofxDateTime.h"

#include "ShishaContainer.h"
#include "SelectList.h"

class DateSelection : public ShishaContainer{
protected:
	ofxDateTime* dateTime;
	bool manageDateTime;
	
	virtual bool refreshGUIFromDateTime();
	virtual bool refreshDateTimeFromGUI();
	
public:
	DateSelection();
	~DateSelection();
	
	virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT, int elementID=-1);
	
	virtual void update();
	
	ofxDateTime* getDateTime();
	
	void setDateTime(ofxDateTime* dateTime=NULL, bool manageDateTime=true);
	
	virtual ShishaElement* setIntValue(string selector=DEFAULT_SELECTOR, int value=0);
	
};

#endif
