/*
 *  DateSelection.cpp
 *  emptyExample
 *
 *  Created by Pat Long (plong0) on 29/09/10.
 *  Copyright 2010 Tangible Interaction. All rights reserved.
 *
 */

#include "DateSelection.h"

DateSelection::DateSelection(){
	this->manageDateTime = false;
	this->dateTime = NULL;
}

DateSelection::~DateSelection(){
	if(this->manageDateTime && this->dateTime != NULL){
		delete this->dateTime;
		this->dateTime = NULL;
	}
}

void DateSelection::init(float x, float y, float width, float height, int elementID){
	ShishaContainer::init(x, y, width, 20.0, elementID);
	
	SelectList* newSelectList;
	newSelectList = new SelectList();
	newSelectList->init(x, y, 100, height);
	newSelectList->setName("month");
	stringstream selectValue;
	for(int i=0; i < 12; i++){
		selectValue << i+1;
		newSelectList->addOption(monthStrings[i], selectValue.str());
		selectValue.str("");
	}
	this->addElement(newSelectList);
	
	this->setHorizontalSpacing(25.0);
	
	newSelectList = new SelectList();
	newSelectList->init(x, y, 30, height);
	newSelectList->setName("day");
	newSelectList->addOptionRange(1.0, 31.0, 1.0, false);
	this->addElement(newSelectList);
	
	newSelectList = new SelectList();
	newSelectList->init(x, y, 70, height);
	newSelectList->setName("year");
	newSelectList->addOptionRange(2010.0, 2050.0, 1.0, false);
	this->addElement(newSelectList);
	
	if(this->dateTime == NULL)
		this->setDateTime();
}

void DateSelection::update(){
	ShishaContainer::update();
	if(this->dateTime != NULL){
		bool updated = this->refreshDateTimeFromGUI();

		if(updated){
			this->dateTime->refreshDateTime();
			this->refreshGUIFromDateTime();
		}
	}
}

bool DateSelection::refreshGUIFromDateTime(){
	bool updated = false;
	
	// refreshDateTime will update month and day if the selected day does not exist in the month (ex. Feb. 30)
	int selectedMonth = this->dateTime->getMonth();
	if(this->getInt("month") != selectedMonth){
		this->setIntValue("month", selectedMonth);
		updated = true;
	}
	
	int selectedDay = this->dateTime->getDay();
	if(this->getInt("day") != selectedDay){
		this->setIntValue("day", selectedDay);
		updated = true;
	}
	
	int selectedYear = this->dateTime->getYear();
	if(this->getInt("year") != selectedYear){
		this->setIntValue("year", selectedYear);
		updated = true;
	}
	
	return updated;
}

bool DateSelection::refreshDateTimeFromGUI(){
	bool updated = false;
	
	int selectedMonth = this->getInt("month");
	if(this->dateTime->getMonth() != selectedMonth){
		this->dateTime->setMonth(selectedMonth);
		updated = true;
	}
	
	int selectedDay = this->getInt("day");
	if(this->dateTime->getDay() != selectedDay){
		this->dateTime->setDay(selectedDay);
		updated = true;
	}
	
	int selectedYear = this->getInt("year");
	if(this->dateTime->getYear() != selectedYear){
		this->dateTime->setYear(selectedYear);
		updated = true;
	}
	
	return updated;
}

ofxDateTime* DateSelection::getDateTime(){
	return this->dateTime;
}

void DateSelection::setDateTime(ofxDateTime* dateTime, bool manageDateTime){
	if(dateTime == NULL && manageDateTime)
		dateTime = new ofxDateTime();
	this->dateTime = dateTime;
	this->manageDateTime = manageDateTime;
	
	if(this->dateTime != NULL){
		this->setIntValue("month", this->dateTime->getMonth());
		this->setIntValue("day", this->dateTime->getDay());
		this->setIntValue("year", this->dateTime->getYear());
	}
}

ShishaElement* DateSelection::setIntValue(string selector, int value){
	bool handled = false;
	if(this->dateTime != NULL){
		if(selector == "epoch" && value >= 0){
			this->dateTime->setFromEpoch(value);
			handled = true;
		}
		if(handled)
			this->refreshGUIFromDateTime();
	}
	return (handled?this:ShishaContainer::setIntValue(selector, value));
}
