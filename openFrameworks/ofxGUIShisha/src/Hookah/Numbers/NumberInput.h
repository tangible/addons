/*
 *  NumberInput.h
 *  emptyExample
 *
 *  Created by Pat Long (plong0) on 25/09/10.
 *  Copyright 2010 Tangible Interaction. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_NUMBER_INPUT
#define _OFX_GUISHISHA_NUMBER_INPUT

#include "TextInput.h"

class NumberInput : public TextInput{
protected:
	float numberValue;
	float minValue, maxValue;
	bool floatsEnabled;
	bool isSetting;
	
	virtual void onPressOff(string cursorID);
	virtual void onDragOff(string cursorID);
	
	virtual bool validChar(char newCheck);
	
	virtual void numberToText();
	virtual void textToNumber();
	
public:
	NumberInput();
	~NumberInput();
	
	virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT, int elementID=-1);
	
	virtual void appendCharacter(char newChar);
	virtual void appendString(string newString);
	virtual void eraseCharacter();
	
	virtual void enableFloats(bool floatsEnabled=true);
	bool isFloatsEnabled();
	
	virtual int getInt(string selector=DEFAULT_SELECTOR);
	virtual float getFloat(string selector=DEFAULT_SELECTOR);
	
	virtual void setAllowedRange(float minValue=-1.0, float maxValue=-1.0);
	virtual void setInt(int value=0);
	virtual void setFloat(float value=0.0);
};

#endif
