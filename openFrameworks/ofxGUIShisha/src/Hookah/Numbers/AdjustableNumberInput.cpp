/*
 *  AdjustableNumberInput.cpp
 *  emptyExample
 *
 *  Created by Pat Long (plong0) on 25/09/10.
 *  Copyright 2010 Tangible Interaction. All rights reserved.
 *
 */

#include "AdjustableNumberInput.h"

AdjustableNumberInput::AdjustableNumberInput(){
}

AdjustableNumberInput::~AdjustableNumberInput(){
}

void AdjustableNumberInput::init(float x, float y, float width, float height, int elementID){
	ShishaContainer::init(x, y, width, height, elementID);
	this->elementType = SHISHA_TYPE_HOOKAH_ADJUSTABLE_NUMBER_INPUT;
	
	this->setAdjustmentAmount();
	
	float buttonWidth = height;
	float inputWidth = width - buttonWidth*2.0;
	float buttonScale = 0.8;
	
	this->decButton = new BasicButton();
	this->decButton->init(x, y+buttonWidth*(1.0-buttonScale)/2.0, buttonWidth*buttonScale, buttonWidth*buttonScale, elementID);
	this->decButton->setName("decButton");
	this->decButton->setLabel("-");
	this->addElement(this->decButton, false);
	
	this->incButton = new BasicButton();
	this->incButton->init(x+width-buttonWidth*buttonScale, y+buttonWidth*(1.0-buttonScale)/2.0, buttonWidth*buttonScale, buttonWidth*buttonScale, elementID);
	this->incButton->setName("incButton");
	this->incButton->setLabel("+");
	this->addElement(this->incButton, false);
	
	this->input = new NumberInput();
	this->input->init(x+buttonWidth, y, inputWidth, height, elementID);
	this->input->setName("input");
	this->input->setLabel("");
	this->input->setString("0.0");
	this->addElement(this->input, false);
}

void AdjustableNumberInput::draw(){
	this->draw(this->x, this->y, this->width, this->height);
}

void AdjustableNumberInput::draw(float x, float y){
	this->draw(x, y, this->width, this->height);
}

void AdjustableNumberInput::draw(float x, float y, float w, float h, bool borders){
	string label = this->getLabel();
	if(label != ""){
		this->getTheme()->setColour(SHISHA_COLOUR_FOREGROUND_1, this);
		this->getTheme()->drawText(this->getLabel(), x, y+h/1.5, false);
		this->getTheme()->setColour();
	}
	this->drawChildren();
}

void AdjustableNumberInput::update(){
	ShishaContainer::update();
	if(this->incButton->getBool()){
		if(this->isFloatsEnabled())
			this->setFloat(this->getFloat()+this->adjustmentAmount);
		else
			this->setInt(this->getInt()+this->adjustmentAmount);
	}
	if(this->decButton->getBool()){
		if(this->isFloatsEnabled())
			this->setFloat(this->getFloat()-this->adjustmentAmount);
		else
			this->setInt(this->getInt()-this->adjustmentAmount);
	}
}

void AdjustableNumberInput::enableFloats(bool floatsEnabled){
	this->input->enableFloats(floatsEnabled);
}

bool AdjustableNumberInput::isFloatsEnabled(){
	return this->input->isFloatsEnabled();
}

int AdjustableNumberInput::getInt(string selector){
	return this->input->getInt(selector);
}

float AdjustableNumberInput::getFloat(string selector){
	return this->input->getFloat(selector);
}

void AdjustableNumberInput::setInt(int value){
	this->input->setInt(value);
}

void AdjustableNumberInput::setFloat(float value){
	this->input->setFloat(value);
}

void AdjustableNumberInput::setAdjustmentAmount(float adjustmentAmount){
	this->adjustmentAmount = adjustmentAmount;
}

void AdjustableNumberInput::setAllowedRange(float minValue, float maxValue){
	this->input->setAllowedRange(minValue, maxValue);
}

/**
void AdjustableNumberInput::setLabel(string label){
	ShishaContainer::setLabel(label);
	float labelWidth = this->getTheme()->getTextWidth(label)*1.25;
	for(map<int,ShishaElement*>::iterator it=this->children.begin() ; it != this->children.end(); it++){
		((*it).second)->setPosition(((*it).second)->getX()+labelWidth, ((*it).second)->getY());
	}
	this->setWidth(this->getWidth()+labelWidth);
}
*/