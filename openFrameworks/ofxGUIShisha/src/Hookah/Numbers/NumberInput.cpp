/*
 *  NumberInput.cpp
 *  emptyExample
 *
 *  Created by Pat Long (plong0) on 25/09/10.
 *  Copyright 2010 Tangible Interaction. All rights reserved.
 *
 */
#include "NumberInput.h"

NumberInput::NumberInput(){
}

NumberInput::~NumberInput(){
}

void NumberInput::init(float x, float y, float width, float height, int elementID){
	TextInput::init(x, y, width, height, elementID);
	this->elementType = SHISHA_TYPE_HOOKAH_NUMBER_INPUT;
	this->setPadding(2.0, -(1.0+height/3.0));
	this->isSetting = false;
	this->enableFloats(true);
	this->setAllowedRange();
	this->setFloat(0.0);
	TextInput::setString("0.0");
	this->setMaxLineCount(1);
}

void NumberInput::appendCharacter(char newChar){
	TextInput::appendCharacter(newChar);
	this->textToNumber();
}

void NumberInput::appendString(string newString){
	TextInput::appendString(newString);
	this->textToNumber();
}

void NumberInput::eraseCharacter(){
	TextInput::eraseCharacter();
	this->textToNumber();
}

void NumberInput::onPressOff(string cursorID){
	TextInput::onPressOff(cursorID);
	this->setFloat(this->getFloat());
}

void NumberInput::onDragOff(string cursorID){
	TextInput::onDragOff(cursorID);
	this->setFloat(this->getFloat());
}

bool NumberInput::validChar(char newCheck){	
	return (newCheck >= '0' && newCheck <= '9' || newCheck == '-' || (this->isFloatsEnabled() && newCheck == '.' && this->textValue.find('.') == string::npos));
}

void NumberInput::textToNumber(){
	if(this->isSetting)
		return;
	float temp;
	if(EOF != sscanf(this->textValue.c_str(), "%f", &temp)){
		this->isSetting = true;
		if(this->isFloatsEnabled())
			this->setFloat(temp);
		else
			this->setInt(temp);
		this->isSetting = false;
	}
}

void NumberInput::numberToText(){
	if(this->isSetting)
		return;
	stringstream valueBuilder;
	if(this->isFloatsEnabled())
		valueBuilder << this->getFloat();
	else
		valueBuilder << this->getInt();
	this->isSetting = true;
	this->setString(valueBuilder.str());
	this->isSetting = false;
	valueBuilder.str("");
}

void NumberInput::enableFloats(bool floatsEnabled){
	this->floatsEnabled = floatsEnabled;
	if(!this->floatsEnabled)
		this->setInt(this->getInt());
	else
		this->setFloat(this->getFloat());
}

bool NumberInput::isFloatsEnabled(){
	return this->floatsEnabled;
}

int NumberInput::getInt(string selector){
	if(this->selectSelf(selector))
		return (int)(this->numberValue);
	return TextInput::getInt(selector);
}

float NumberInput::getFloat(string selector){
	if(this->selectSelf(selector))
		return this->numberValue;
	return TextInput::getFloat(selector);
}

void NumberInput::setAllowedRange(float minValue, float maxValue){
	this->minValue = minValue;
	this->maxValue = maxValue;
}

void NumberInput::setInt(int value){
	this->setFloat((float)value);
}

void NumberInput::setFloat(float value){
	if(!this->isFloatsEnabled())
		value = (float)((int)value);
	if(minValue != -1.0 && maxValue != -1.0){
		if(value < minValue)
			value = minValue;
		if(value > maxValue)
			value = maxValue;
	}
	this->numberValue = value;
	this->numberToText();
}
