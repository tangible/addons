/*
 *  AdjustableNumberInput.h
 *  emptyExample
 *
 *  Created by Pat Long (plong0) on 25/09/10.
 *  Copyright 2010 Tangible Interaction. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_ADJUSTABLE_NUMBER_INPUT
#define _OFX_GUISHISHA_ADJUSTABLE_NUMBER_INPUT

#include "ShishaContainer.h"
#include "BasicButton.h"
#include "NumberInput.h"

class AdjustableNumberInput : public ShishaContainer{
protected:
	NumberInput* input;
	BasicButton* incButton;
	BasicButton* decButton;
	float adjustmentAmount;
	
public:
	AdjustableNumberInput();
	~AdjustableNumberInput();
	
	virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT, int elementID=-1);
	
	virtual void draw();
	virtual void draw(float x, float y);
	virtual void draw(float x, float y, float w, float h, bool borders=false);
	
	virtual void update();
	
	virtual void enableFloats(bool floatsEnabled=true);
	virtual bool isFloatsEnabled();
	
	virtual int getInt(string selector=DEFAULT_SELECTOR);
	virtual float getFloat(string selector=DEFAULT_SELECTOR);
	
	virtual void setInt(int value=0);
	virtual void setFloat(float value=0.0);
	
	virtual void setAdjustmentAmount(float adjustmentAmount=1.0);
	virtual void setAllowedRange(float minValue=-1.0, float maxValue=-1.0);
//	virtual void setLabel(string label);
};

#endif
