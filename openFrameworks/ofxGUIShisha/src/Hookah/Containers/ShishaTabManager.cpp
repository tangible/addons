/*
 *  ShishaTabManager.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 07/12/09.
 *  Copyright 2009 Tangible Interaction. All rights reserved.
 *
 */

#include "ShishaTabManager.h"

ShishaTabManager::ShishaTabManager(){
}

ShishaTabManager::~ShishaTabManager(){
	for(int i=0; i < this->containers.size(); i++){
		delete this->containers[i];
		this->containers[i] = NULL;
	}
	this->containers.clear();
}

void ShishaTabManager::init(float x, float y, float width, float height, int elementID){
	ShishaContainer::init(x, y, width, height, elementID);
	this->elementType = SHISHA_TYPE_TAB_MANAGER;
	this->setName("ShishaTabManager");
	this->horizontalSpacing = 0;
	this->verticalSpacing = 0;
	this->cOffsetX = 0;
	this->cOffsetY = 0;
	this->activeContainer = NULL;
	this->allowEmptyTab = true;
	this->forceChildCursorCheck = true;
	this->shouldDrawChildren = true;
	this->tabAnimationMode = DEFAULT_TABCONTAINER_ANIMATE_MODE;
	this->tabAutoHideTime = -1;
}

void ShishaTabManager::drawActiveContainer(){
	if(this->activeContainer != NULL && this->activeContainer->isActive()){
		this->activeContainer->draw();
	}
	else{
		this->drawEmptyContainer();
	}
}

void ShishaTabManager::drawEmptyContainer(){
}

void ShishaTabManager::draw(){
	if(this->shouldDrawChildren)
		this->drawChildren();
	this->drawActiveContainer();
}

void ShishaTabManager::update(){
	ShishaContainer::update();
	this->updateContainers();
	if(this->activeContainer != NULL && !this->activeContainer->isActive())
		this->activeContainer = NULL;
	for(int i=0; i < this->containers.size(); i++){
		if(this->containers[i]->isWaitingToShow() && (this->containers[i] == this->activeContainer || this->activeContainer == NULL)){
			if(this->activeContainer == NULL)
				this->activeContainer = this->containers[i];
			this->activeContainer->doShow(true);
		}
		else if(this->containers[i]->isWaitingToShow() && this->containers[i] != this->activeContainer){
			if(this->activeContainer != NULL && this->activeContainer->isActive() && !this->activeContainer->isAnimatingHide()){
				this->activeContainer->doHide();
			}
			else if(this->activeContainer != NULL && this->activeContainer->isDoneAnimating()){
				this->activeContainer = this->containers[i];
				this->activeContainer->doShow(true);
			}
		}
	}
}

bool ShishaTabManager::canToggle(ShishaTabContainer* container){
	if(container == this->activeContainer && container->isActive() && !this->allowEmptyTab){
		return false;
	}
	return true;
}

void ShishaTabManager::setAllowEmptyTab(bool allowEmptyTab){
	this->allowEmptyTab = allowEmptyTab;
}

void ShishaTabManager::setTabAnimationMode(int tabAnimationMode, int tabAnimationSpeed){
	this->tabAnimationMode = tabAnimationMode;
	this->tabAnimationSpeed = tabAnimationSpeed;
}

void ShishaTabManager::setTabAutoHideTime(int tabAutoHideTime){
	this->tabAutoHideTime = tabAutoHideTime;
}

void ShishaTabManager::setShouldDrawChildren(bool shouldDrawChildren){
	this->shouldDrawChildren = shouldDrawChildren;
}

bool ShishaTabManager::checkCursorHover(int x, int y, string cursorID, float w, float h){
//	if(!this->checkContainersCursorHover(x, y, cursorID, w, h))
	bool check = this->checkContainersCursorHover(x, y, cursorID, w, h);
	if(!check)
		check = ShishaContainer::checkCursorHover(x, y, cursorID, w, h);
	return check;
}

bool ShishaTabManager::checkCursorDrag(int x, int y, int button, string cursorID, float w, float h){
//	if(!this->checkContainersCursorDrag(x, y, button, cursorID, w, h))
	bool check = this->checkContainersCursorDrag(x, y, button, cursorID, w, h);
	if(!check)
		check = ShishaContainer::checkCursorDrag(x, y, button, cursorID, w, h);
	return check;
}

bool ShishaTabManager::checkCursorPress(int x, int y, int button, string cursorID, float w, float h){
//	if(!this->checkContainersCursorPress(x, y, button, cursorID, w, h))
	bool check = this->checkContainersCursorPress(x, y, button, cursorID, w, h);
	if(!check)
		check = ShishaContainer::checkCursorPress(x, y, button, cursorID, w, h);
	return check;
}

bool ShishaTabManager::checkCursorRelease(int x, int y, int button, string cursorID, float w, float h){
//	if(!this->checkContainersCursorRelease(x, y, button, cursorID, w, h))
	bool check = this->checkContainersCursorRelease(x, y, button, cursorID, w, h);
	if(!check)
		check = ShishaContainer::checkCursorRelease(x, y, button, cursorID, w, h);
	return check;
}

bool ShishaTabManager::checkKeyPressed(int key){
	if(!this->checkContainersKeyPressed(key))
		return ShishaContainer::checkKeyPressed(key);
	return true;
}

bool ShishaTabManager::checkKeyReleased(int key){
	if(!this->checkContainersKeyReleased(key))
		return ShishaContainer::checkKeyPressed(key);
	return true;
}


void ShishaTabManager::addContainer(ShishaTabContainer* container, bool inheritAnimation, bool manageTab){
	container->setTheme(this->theme, NULL, true);
	this->containers.push_back(container);
	container->getContainerTab()->setElementID(-1);
	if(manageTab)
		ShishaContainer::addElement(container->getContainerTab(), true, true);
	container->setTabManager(this);
	if(inheritAnimation){
		container->setAnimationMode(this->tabAnimationMode, this->tabAnimationSpeed);
		container->setAutoHideTime(this->tabAutoHideTime);
	}
	container->doHide(true);
	if(this->activeContainer == NULL){
		if(!this->allowEmptyTab){
			this->activeContainer = container;
			this->activeContainer->doShow();
		}
	}
}

ShishaElement* ShishaTabManager::getElement(string selector, string& subSelector){
	ShishaElement* element = ShishaContainer::getElement(selector, subSelector);
	if(element == NULL){
		string elementName = "";
		int selectParse = selector.find('.');
		if(selectParse != string::npos){
			elementName = selector.substr(0, selectParse);
			subSelector = selector.substr(selectParse+1);
		}
		else{
			elementName = selector;
		}
		for(int i=0; i < this->containers.size(); i++){
			if(this->containers[i]->getName() == elementName){
				element = this->containers[i]->selectElement(subSelector);
				subSelector = "";
				break;
			}
		}
	}
	return element;
}

bool ShishaTabManager::getBool(string selector){
	if(this->selectSelf(selector))
		return (this->activeContainer != NULL);
	return ShishaContainer::getBool(selector);
}

void ShishaTabManager::updateContainers(){
	for(int i=0; i < this->containers.size(); i++){
		this->containers[i]->update();
	}
}

bool ShishaTabManager::checkContainersCursorHover(int x, int y, string cursorID, float w, float h){
	bool result = false;
	if(this->activeContainer != NULL && this->activeContainer->checkCursorHover(x, y, cursorID, w, h) && !this->activeContainer->isAnimating())
		result = true;
/**	for(int i=0; i < this->containers.size(); i++){
		if(this->containers[i]->checkCursorHover(x, y, cursorID, w, h))
			result = true;
	}*/
	return result;
}

bool ShishaTabManager::checkContainersCursorDrag(int x, int y, int button, string cursorID, float w, float h){
	bool result = false;
	if(this->activeContainer != NULL && this->activeContainer->checkCursorDrag(x, y, button, cursorID, w, h) && !this->activeContainer->isAnimating())
		result = true;
/**	for(int i=0; i < this->containers.size(); i++){
		if(this->containers[i]->checkCursorDrag(x, y, button, cursorID, w, h))
			result = true;
	}*/
	return result;
}

bool ShishaTabManager::checkContainersCursorPress(int x, int y, int button, string cursorID, float w, float h){
	bool result = false;
	if(this->activeContainer != NULL && this->activeContainer->checkCursorPress(x, y, button, cursorID, w, h) && !this->activeContainer->isAnimating())
		result = true;
/**	for(int i=0; i < this->containers.size(); i++){
		if(this->containers[i]->checkCursorPress(x, y, button, cursorID, w, h))
			result = true;
	}*/
	return result;
}

bool ShishaTabManager::checkContainersCursorRelease(int x, int y, int button, string cursorID, float w, float h){
	bool result = false;
	if(this->activeContainer != NULL && this->activeContainer->checkCursorRelease(x, y, button, cursorID, w, h) && !this->activeContainer->isAnimating())
		result = true;
/**	for(int i=0; i < this->containers.size(); i++){
		if(this->containers[i]->checkCursorRelease(x, y, button, cursorID, w, h))
			result = true;
	}*/
	return result;
}

bool ShishaTabManager::checkContainersKeyPressed(int key){
	bool result = false;
	if(this->activeContainer != NULL && this->activeContainer->checkKeyPressed(key) && !this->activeContainer->isAnimating())
		result = true;
/**	for(int i=0; i < this->containers.size(); i++){
		if(this->containers[i]->checkKeyPressed(key))
			result = true;
	}*/
	return result;
}

bool ShishaTabManager::checkContainersKeyReleased(int key){
	bool result = false;
	if(this->activeContainer != NULL && this->activeContainer->checkKeyReleased(key) && !this->activeContainer->isAnimating())
		result = true;
/**	for(int i=0; i < this->containers.size(); i++){
		if(this->containers[i]->checkKeyReleased(key))
			result = true;
	}*/
	return result;
}
