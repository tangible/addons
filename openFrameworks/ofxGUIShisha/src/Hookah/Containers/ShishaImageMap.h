/*
 *  ShishaImageMap.h
 *  openFrameworks
 *
 *  Created by Pat Long on 18/06/09.
 *  Copyright 2009 Tangible Interaction. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_SHISHAIMAGEMAP
#define _OFX_GUISHISHA_SHISHAIMAGEMAP

#include "ofxImage.h"

#include "ShishaContainer.h"
#include "../Bars/BarIndex.h" // we do some special stuff for bar image maps

class ShishaImageMap : public ShishaContainer{
	protected:
		class ImageMapElement{
			private:
				friend class ShishaImageMap;
				ShishaElement* element;
				vector<ofxImage*> stateImages;
				ofRectangle mapLocation;
				bool visible;
				bool drawElement;
			
			public:
				ImageMapElement(ShishaElement* element, vector<ofxImage*> stateImages, bool drawElement=false);
				~ImageMapElement();
			
				int getGuiState();
			
				virtual float getX();
				virtual float getY();
				virtual void setPosition(float x, float y);
			
				void setMapLocation(float x, float y, float w, float h);
				bool toggleVisibility();
			
				void draw();
		};
	
		ofxImage imageMap, mapInactive, mapInactiveHover, mapActiveHover, mapActive;
//		map<int, ofxImage*>stateImages; // imageMap, mapUp, mapUpHover, mapDownHover, mapDown;
		vector<ImageMapElement*> mapElements;
		bool shouldClearMapPixels;
		bool shouldDrawInactiveBG;
	
		ImageMapElement* getMapElement(ShishaElement* element);
		bool mapHasPixel(int x, int y);
	
		virtual void onMove(float xMove, float yMove);
	
		virtual void updateChildren();
		virtual bool checkChildrenCursorHover(int x, int y, string cursorID, float w=1.0, float h=1.0);
		virtual bool checkChildrenCursorDrag(int x, int y, int button, string cursorID, float w=1.0, float h=1.0);
		virtual bool checkChildrenCursorPress(int x, int y, int button, string cursorID, float w=1.0, float h=1.0);
		virtual bool checkChildrenCursorRelease(int x, int y, int button, string cursorID, float w=1.0, float h=1.0);
	
	public:
		ShishaImageMap();
		~ShishaImageMap();
		virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT, int elementID=-1);
	
		virtual void clearCursors();
	
		virtual bool loadImageMap(string filename, int stateCount=4);
	
		void clearArea(float x, float y, float w, float h);
		virtual ImageMapElement* mapElement(ShishaElement* element, float x, float y, float w, float h, bool containerAdd=true, bool drawElement=false);
		bool toggleElementVisibility(ShishaElement* element);
		void setShouldClearMapPixels(bool shouldClearMapPixels);
		void setShouldDrawInactiveBG(bool shouldDrawInactiveBG);
	
		virtual void draw();
		virtual void draw(float x, float y);
		virtual void draw(float x, float y, float w, float h, bool borders);
	
		virtual bool checkCursorHover(int x, int y, string cursorID, float w=1.0, float h=1.0);
		virtual bool checkCursorDrag(int x, int y, int button, string cursorID, float w=1.0, float h=1.0);
		virtual bool checkCursorPress(int x, int y, int button, string cursorID, float w=1.0, float h=1.0);
};

#endif
