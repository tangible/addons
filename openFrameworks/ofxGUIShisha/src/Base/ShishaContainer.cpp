/*
 *  ShishaContainer.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 27/04/09.
 *  Copyright 2009 Tangible Interaction. All rights reserved.
 *
 */

#include "ShishaContainer.h"

ShishaContainer::ShishaContainer(){
}

ShishaContainer::~ShishaContainer(){
	for(map<int,ShishaElement*>::iterator it=this->children.begin(); it != this->children.end(); it++){
		delete (*it).second;
		(*it).second = NULL;
	}
	this->children.clear();
}

void ShishaContainer::init(float x, float y, float width, float height, int elementID){
	ShishaElement::init(x, y, width, height, elementID);
	this->setName("ShishaContainer");
	this->elementType = SHISHA_TYPE_HOOKAH_PANEL;
	this->flowMode = SHISHA_FLOW_MODE_HORIZONTAL;
	this->horizontalSpacing = DEFAULT_HORIZONTAL_SPACING;
	this->verticalSpacing = DEFAULT_VERTICAL_SPACING;
	this->cOffsetX = this->horizontalSpacing;
	this->cOffsetY = this->verticalSpacing;
	this->cBiggestOffset = 0.0;
	this->forceChildCursorCheck = false;
}

void ShishaContainer::clearCursors(){
	ShishaElement::clearCursors();
	for(map<int,ShishaElement*>::iterator it=this->children.begin() ; it != this->children.end(); it++)
		((*it).second)->clearCursors();
}

void ShishaContainer::outputCursors(bool verbose){
	ShishaElement::outputCursors(verbose);
	for(map<int,ShishaElement*>::iterator it=this->children.begin() ; it != this->children.end(); it++)
		((*it).second)->outputCursors(verbose);
}

void ShishaContainer::drawChildren(){
	for(map<int,ShishaElement*>::iterator it=this->children.begin() ; it != this->children.end(); it++)
		((*it).second)->draw();
}

void ShishaContainer::updateChildren(){
	for(map<int,ShishaElement*>::iterator it=this->children.begin() ; it != this->children.end(); it++)
		((*it).second)->update();
}

bool ShishaContainer::checkChildrenCursorHover(int x, int y, string cursorID, float w, float h){
	bool check = false;
	for(map<int,ShishaElement*>::iterator it=this->children.begin() ; it != this->children.end(); it++){
		if((*it).second->checkCursorHover(x, y, cursorID, w, h))
			check = true;
	}
	return check;
}

bool ShishaContainer::checkChildrenCursorDrag(int x, int y, int button, string cursorID, float w, float h){
	bool check = false;
	for(map<int,ShishaElement*>::iterator it=this->children.begin() ; it != this->children.end(); it++){
		if((*it).second->checkCursorDrag(x, y, button, cursorID, w, h))
			check = true;
	}
	return check;
}

bool ShishaContainer::checkChildrenCursorPress(int x, int y, int button, string cursorID, float w, float h){
	bool check = false;
	for(map<int,ShishaElement*>::iterator it=this->children.begin() ; it != this->children.end(); it++){
		if((*it).second->checkCursorPress(x, y, button, cursorID, w, h))
			check = true;
	}
	return check;
}

bool ShishaContainer::checkChildrenCursorRelease(int x, int y, int button, string cursorID, float w, float h){
	bool check = false;
	for(map<int,ShishaElement*>::iterator it=this->children.begin() ; it != this->children.end(); it++){
		if((*it).second->checkCursorRelease(x, y, button, cursorID, w, h))
			check = true;
	}
	return check;
}

bool ShishaContainer::checkChildrenKeyPressed(int key){
	bool check = false;
	for(map<int,ShishaElement*>::iterator it=this->children.begin() ; it != this->children.end(); it++){
		if((*it).second->checkKeyPressed(key))
			check = true;
	}
	return check;
}

bool ShishaContainer::checkChildrenKeyReleased(int key){
	bool check = false;
	for(map<int,ShishaElement*>::iterator it=this->children.begin() ; it != this->children.end(); it++){
		if((*it).second->checkKeyReleased(key))
			check = true;
	}
	return check;
}

void ShishaContainer::draw(){
	ShishaElement::draw();
}

void ShishaContainer::draw(float x, float y){
//	this->draw(x, y, this->width, this->height);
	ShishaElement::draw(x, y);
}
/**
void ShishaContainer::draw(float x, float y, float w, float h){
	this->draw(x, y, w, h, this->getTheme()->drawContainerBorders());
}
*/
void ShishaContainer::draw(float x, float y, float w, float h, bool borders){
	ShishaElement::draw(x, y, w, h, borders);
	if(this->getLabel() != ""){
		float labelHeight = this->getTheme()->getTextHeight(this->getLabel());
		this->getTheme()->drawText(this->getLabel(), x, y+labelHeight*1.5, false);
	}
	this->drawChildren();
}

void ShishaContainer::update(){
	ShishaElement::update();
	this->updateChildren();	
}

void ShishaContainer::setLabel(string label){
	float oldLabelWidth = this->getTheme()->getTextWidth(this->getLabel());
	ShishaElement::setLabel(label);
	float newLabelWidth = this->getTheme()->getTextWidth(this->getLabel());
	float labelDiff = newLabelWidth-oldLabelWidth;
	this->onMove(labelDiff, 0.0);
}

bool ShishaContainer::checkCursorHover(int x, int y, string cursorID, float w, float h){
//	this->translateMouseCoords(x, y);
	bool check = ShishaElement::checkCursorHover(x, y, cursorID, w, h);
//	if(check || !check || !this->cropBounds || this->forceChildCursorCheck)
	x += this->scrollOffsetX;
	y += this->scrollOffsetY;
	this->checkChildrenCursorHover(x, y, cursorID, w, h);
	return check;
}

bool ShishaContainer::checkCursorDrag(int x, int y, int button, string cursorID, float w, float h){
//	this->translateMouseCoords(x, y);
	bool check = ShishaElement::checkCursorDrag(x, y, button, cursorID, w, h);
	if(check || !this->cropBounds || this->forceChildCursorCheck){
		x += this->scrollOffsetX;
		y += this->scrollOffsetY;
		this->checkChildrenCursorDrag(x, y, button, cursorID, w, h);
	}
	return check;
}

bool ShishaContainer::checkCursorPress(int x, int y, int button, string cursorID, float w, float h){
//	this->translateMouseCoords(x, y);
	bool check = ShishaElement::checkCursorPress(x, y, button, cursorID, w, h);
	if(check || !this->cropBounds || this->forceChildCursorCheck){
		x += this->scrollOffsetX;
		y += this->scrollOffsetY;
		this->checkChildrenCursorPress(x, y, button, cursorID, w, h);
	}
	return check;
}

bool ShishaContainer::checkCursorRelease(int x, int y, int button, string cursorID, float w, float h){
//	this->translateMouseCoords(x, y);
	bool check = ShishaElement::checkCursorRelease(x, y, button, cursorID, w, h);
	x += this->scrollOffsetX;
	y += this->scrollOffsetY;
	this->checkChildrenCursorRelease(x, y, button, cursorID, w, h);
	return check;
}

bool ShishaContainer::checkKeyPressed(int key){
	bool check = ShishaElement::checkKeyPressed(key);
	check = this->checkChildrenKeyPressed(key);
	return check;
}

bool ShishaContainer::checkKeyReleased(int key){
	bool check = ShishaElement::checkKeyReleased(key);
	check = this->checkChildrenKeyReleased(key);
	return check;
}

void ShishaContainer::incrementOffsets(ShishaElement* lastElement, bool forceBreak){
	if(lastElement == NULL)
		return;
	
	// make sure we are big enough to contain it
	if(lastElement->getWidth() > (this->getWidth() - this->horizontalSpacing*2.0))
		this->setWidth(lastElement->getWidth() + this->horizontalSpacing*2.0);
	if(lastElement->getHeight() > (this->getHeight() - this->verticalSpacing*2.0))
		this->setHeight(lastElement->getHeight() + this->verticalSpacing*2.0);
	
	switch(this->flowMode){
		case SHISHA_FLOW_MODE_HORIZONTAL:
			this->cOffsetX += this->horizontalSpacing + lastElement->getWidth();
			
			// check if we have reached the edge and should start a new row
			if(this->cOffsetX >= this->width || forceBreak){
				if(this->cBiggestOffset == 0)
					this->cBiggestOffset = lastElement->getHeight();
				this->resetHorizontalOffsets();
			}
			
			if(lastElement->getHeight() > this->cBiggestOffset){
				this->cBiggestOffset = lastElement->getHeight();
			}
			
			// check if we are overlapping the edge and need to move our element into a new row
			if((lastElement->getX()+lastElement->getWidth()) > (this->x+this->width-this->horizontalSpacing)){
				lastElement->setLocation(this->x+this->cOffsetX, this->y+this->cOffsetY);
				this->incrementOffsets(lastElement); //, true);
			}
			
			// check if we are overlapping the bottom and need to increase the panel's height
			if((lastElement->getY()+lastElement->getHeight()) > (this->y+this->height-this->verticalSpacing)){
				float heightDiff = (lastElement->getY()+lastElement->getHeight()) - (this->y+this->height-this->verticalSpacing);
				this->setHeight(this->height + heightDiff);
			}
			break;
			
		case SHISHA_FLOW_MODE_VERTICAL:
			this->cOffsetY += this->verticalSpacing + lastElement->getHeight();
			
			// check if we have reached the edge and should start a new row
			if(this->cOffsetY >= this->height || forceBreak){
				if(this->cBiggestOffset == 0)
					this->cBiggestOffset = lastElement->getWidth();
				this->resetVerticalOffsets();
			}
			
			if(lastElement->getWidth() > this->cBiggestOffset){
				this->cBiggestOffset = lastElement->getWidth();
			}
			
			// check if we are overlapping the bottom and need to move our element into a new column
			if((lastElement->getY()+lastElement->getHeight()) > (this->y+this->height-this->verticalSpacing)){
				lastElement->setLocation(this->x+this->cOffsetX, this->y+this->cOffsetY);
				this->incrementOffsets(lastElement); //, true);
			}
			
			// check if we are overlapping the edge and need to increase the panel's width
			if((lastElement->getX()+lastElement->getWidth()) > (this->x+this->width-this->horizontalSpacing)){
				float widthDiff = (lastElement->getX()+lastElement->getWidth()) - (this->x+this->width-this->horizontalSpacing);
				this->setWidth(this->width + widthDiff);
			}
			break;
	}
}

void ShishaContainer::resetHorizontalOffsets(){
	this->cOffsetX = this->horizontalSpacing;
	this->cOffsetY += this->verticalSpacing + this->cBiggestOffset;
	this->cBiggestOffset = 0.0;
}

void ShishaContainer::resetVerticalOffsets(){
	this->cOffsetY = this->verticalSpacing;
	this->cOffsetX += this->horizontalSpacing + this->cBiggestOffset;
	this->cBiggestOffset = 0.0;
}

bool ShishaContainer::prepareForMotion(){
	bool check = true;
	for(map<int,ShishaElement*>::iterator it=this->children.begin() ; it != this->children.end(); it++){
		if(!((*it).second)->prepareForMotion())
			check = false;
	}
	return check;
}

void ShishaContainer::setFlowMode(int flowMode){
	this->flowMode = flowMode;
}

void ShishaContainer::setHorizontalSpacing(float horizontalSpacing){
	int offSetChange = horizontalSpacing - this->horizontalSpacing;
	this->cOffsetX += offSetChange;
	this->horizontalSpacing = horizontalSpacing;
}

void ShishaContainer::setVerticalSpacing(float verticalSpacing){
	int offSetChange = verticalSpacing - this->verticalSpacing;
	this->cOffsetY += offSetChange;
	this->verticalSpacing = verticalSpacing;
}

void ShishaContainer::setTheme(ShishaTheme* theme, ShishaTheme* doNotDelete, bool themeInherited){
	ShishaTheme* oldTheme = this->theme;
	bool oldThemeInherited = this->themeInherited;
	ShishaElement::setTheme(theme, oldTheme, themeInherited);
	if(this->theme != NULL)
		this->drawBorders = this->theme->drawContainerBorders();
	if(this->children.size() > 0){
		for(map<int,ShishaElement*>::iterator it=this->children.begin() ; it != this->children.end(); it++)
			((*it).second)->setTheme(this->theme, oldTheme, true);
	}
	if(oldTheme != NULL && oldTheme != doNotDelete && !oldThemeInherited){
		delete oldTheme;
		oldTheme = NULL;
	}
}

void ShishaContainer::setForceChildCursorCheck(bool forceChildCursorCheck){
	this->forceChildCursorCheck = forceChildCursorCheck;
}

ShishaElement* ShishaContainer::addElement(ShishaElement* element, bool updateLocation, bool inheritTheme){
	int id = element->getElementID();
	if(id < 0){
		id = this->children.size();
		element->setElementID(id);
	}
	if(inheritTheme)
		element->setTheme(this->getTheme(), NULL, true);
	element->setParent(this);
	this->children[id] = element;
	if(updateLocation){
		element->setLocation(this->x+this->cOffsetX, this->y+this->cOffsetY);
		this->incrementOffsets(element);
	}
	return element;
}

ShishaElement* ShishaContainer::dropElement(ShishaElement* element, bool doDelete){
	if(element != NULL){
		for(map<int,ShishaElement*>::iterator it = this->children.begin(); it != this->children.end(); it++){
			if(it->second == element){
				cout << "dropping..." << (it->first) << ":" << (it->second->getName()) << ":" << element->getName() << endl;
				this->children.erase(it);
				if(doDelete){
					delete element;
					element = NULL;
				}
				return element;
			}
		}
	}
	return NULL;
}

void ShishaContainer::clearChildren(bool doDelete){
	for(map<int,ShishaElement*>::iterator it = this->children.begin(); it != this->children.end(); it++){
		if(doDelete){
			delete it->second;
			it->second = NULL;
		}
	}
	this->children.clear();	
}

ShishaElement* ShishaContainer::getElement(string selector){
	return ShishaElement::getElement(selector);
}

ShishaElement* ShishaContainer::getElement(string selector, string& subSelector){
	if(this->selectSelf(selector))
		return this;
	
	string elementName = "";
	int selectParse = selector.find('.');
	if(selectParse != string::npos){
		elementName = selector.substr(0, selectParse);
		subSelector = selector.substr(selectParse+1);
	}
	else{
		elementName = selector;
	}

	ShishaElement* element = NULL;
	if(elementName != ""){
		for(map<int,ShishaElement*>::iterator it = this->children.begin(); it != this->children.end(); it++){
			if(it->second->getName() == elementName){
				element = it->second;
				break;
			}
		}
	}

	return element;
}

bool ShishaContainer::getBool(string selector){
	if(this->selectSelf(selector))
		return ShishaElement::getBool(selector);
	string subSelector = "";
	ShishaElement* element = this->getElement(selector, subSelector);
	if(element != NULL){
		return element->getBool(subSelector);
	}
	return SELECTOR_UNIDENTIFIED_BOOL;
}

int ShishaContainer::getInt(string selector){
	if(this->selectSelf(selector))
		return this->children.size();
	string subSelector = "";
	ShishaElement* element = this->getElement(selector, subSelector);
	if(element != NULL){
		return element->getInt(subSelector);
	}
	return SELECTOR_UNIDENTIFIED_INT;
}

float ShishaContainer::getFloat(string selector){
	if(this->selectSelf(selector))
		return (float)this->children.size();
	string subSelector = "";
	ShishaElement* element = this->getElement(selector, subSelector);
	if(element != NULL){
		return element->getFloat(subSelector);
	}
	return SELECTOR_UNIDENTIFIED_FLOAT;
}

string ShishaContainer::getString(string selector){
	if(this->selectSelf(selector))
		return ShishaElement::getString(selector);
	string subSelector;
	ShishaElement* element = this->getElement(selector, subSelector);
	if(element != NULL){
		return element->getString(subSelector);
	}
	return SELECTOR_UNIDENTIFIED_STRING;
}

float ShishaContainer::calculateContentWidth(){
	float contentWidth = 0.0;
	float cWidth = 0.0;
	float cXOff = 0.0;
	for(map<int,ShishaElement*>::iterator it=this->children.begin() ; it != this->children.end(); it++){
		ShishaElement* cElement = (*it).second;
		if(cElement->getX() < cXOff){
			if(cWidth > contentWidth)
				contentWidth = cWidth;
			cWidth = 0.0;
			cXOff = 0.0;
		}
		if(cXOff == 0.0)
			cXOff = cElement->getX();
		
		float addWidth = cElement->getWidth() + this->horizontalSpacing;
		cWidth += addWidth;
		cXOff += addWidth;
	}
	if(cWidth > contentWidth)
		contentWidth = cWidth;
	this->contentWidth = contentWidth;
	return contentWidth;
}

float ShishaContainer::calculateContentHeight(){
	float contentHeight = 0.0;
	float cHeight = 0.0;
	float cYOff = 0.0;
	for(map<int,ShishaElement*>::iterator it=this->children.begin() ; it != this->children.end(); it++){
		ShishaElement* cElement = (*it).second;
		if(cElement->getY() < cYOff){
			if(cHeight > contentHeight)
				contentHeight = cHeight;
			cHeight = 0.0;
			cYOff = 0.0;
		}
		if(cYOff == 0.0)
			cYOff = cElement->getY();
		
		float addHeight = cElement->getHeight() + this->verticalSpacing;
		cHeight += addHeight;
		cYOff += addHeight;
	}
	if(cHeight > contentHeight)
		contentHeight = cHeight;
	this->contentHeight = contentHeight;
	return contentHeight;
}

ShishaElement* ShishaContainer::selectElement(string selector){
	ShishaElement* element = NULL;
	if(this->selectSelf(selector))
		element = this;
	else{
		string subSelector = "";
		element = this->getElement(selector, subSelector);
		if(element != NULL && subSelector != ""){
			selector = subSelector;
			subSelector = "";
			if(selector.find('.') != string::npos)
				element = element->getElement(selector, subSelector);
		}
	}
	return element;
}

string ShishaContainer::parseSubSelector(string selector, ShishaElement* element){
	string subSelector = selector;
	if(element != NULL){
		if(selector == element->getName()){
			subSelector = "value";
		}
		else if(selector.find(element->getName()) == 0){
			subSelector = selector.substr(element->getName().length()+1);
		}
	}
	
	return subSelector;
}

ShishaElement* ShishaContainer::setBoolValue(string selector, bool value){
	ShishaElement* element = this->selectElement(selector);
	if(element != NULL){
		element->setBool(value);
/**		if(element == this)
			element->setBool(value);
		else
			element->setBoolValue(this->parseSubSelector(selector, element), value);
 */
	}
	return element;
}

ShishaElement* ShishaContainer::setElementValue(string selector, ShishaElement* value){
	ShishaElement* element = this->selectElement(selector);
	if(element != NULL){
		element->setElement(value);
/**		if(element == this)
			element->setElement(value);
		else
			element->setElementValue(this->parseSubSelector(selector, element), value);
 */
	}
	return element;
}

ShishaElement* ShishaContainer::setIntValue(string selector, int value){
	ShishaElement* element = this->selectElement(selector);
	if(element != NULL){
		element->setInt(value);
/**		if(element == this)
			element->setInt(value);
		else
			element->setIntValue(this->parseSubSelector(selector, element), value);
 */
	}
	return element;
}

ShishaElement* ShishaContainer::setFloatValue(string selector, float value){
	ShishaElement* element = this->selectElement(selector);
	if(element != NULL){
		element->setFloat(value);
/**		if(element == this)
			element->setFloat(value);
		else
			element->setFloatValue(this->parseSubSelector(selector, element), value);
 */
	}
	return element;
}

ShishaElement* ShishaContainer::setStringValue(string selector, string value){
	ShishaElement* element = this->selectElement(selector);
	if(element != NULL){
		element->setString(value);
/**		if(element == this)
			element->setString(value);
		else
			element->setStringValue(this->parseSubSelector(selector, element), value);
 */
	}
	return element;
}

void ShishaContainer::onMove(float xMove, float yMove){
	ShishaElement::onMove(xMove, yMove);
	for(map<int,ShishaElement*>::iterator it=this->children.begin() ; it != this->children.end(); it++){
		float newX = ((*it).second)->getX() + xMove;
		float newY = ((*it).second)->getY() + yMove;
		((*it).second)->setPosition(newX, newY);
	}
}
