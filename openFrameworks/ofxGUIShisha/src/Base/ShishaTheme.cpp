/*
 *  ShishaTheme.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 23/04/09.
 *  Copyright 2009 Tangible Interaction. All rights reserved.
 *
 */
#include "ShishaTheme.h"

ShishaTheme::ShishaTheme(){
	this->initted = false;
}

ShishaTheme::~ShishaTheme(){
	this->checkDestroyed();
}

// NOTE: this theme looks best on a black or white background with alpha blending enabled - ofEnableAlphaBlending()
void ShishaTheme::initTheme(){
	this->themeName = "ShishaTheme";
	this->themeColours[SHISHA_COLOUR_DEFAULT] =		new ColourRGBA(255, 255, 255, 255); // should never change this one
	this->themeColours[SHISHA_COLOUR_BACKGROUND] =	new ColourRGBA(192, 128, 0, 64);
	this->themeColours[SHISHA_COLOUR_BORDER] =		new ColourRGBA(96, 96, 96);
	this->themeColours[SHISHA_COLOUR_BORDER_MOUSEOVER] = new ColourRGBA(128, 128, 128);
	this->themeColours[SHISHA_COLOUR_BORDER_MOUSEDOWN] = new ColourRGBA(160, 160, 160);
	this->themeColours[SHISHA_COLOUR_BORDER_MOUSEDRAG] = new ColourRGBA(192, 192, 192);
	this->themeColours[SHISHA_COLOUR_FOREGROUND_1] =	new ColourRGBA(164, 132, 0);
	this->themeColours[SHISHA_COLOUR_FOREGROUND_2] =	new ColourRGBA(255, 192, 0);
	this->themeColours[SHISHA_COLOUR_CURSORS] =		new ColourRGBA(255, 255, 0);
	this->containerBorders = DRAW_BORDERS;
	this->panelBorders = DRAW_BORDERS;
	this->buttonBorders = DRAW_BORDERS;
	this->imageBorders = false;
	this->textOutputBorders = true;
	this->textInputBorders = true;
	this->initted = true;
}

void ShishaTheme::destroyTheme(){
	if(this->initted){
		for(map<int,ColourRGBA*>::iterator it=this->themeColours.begin(); it != this->themeColours.end(); it++){
			delete (*it).second;
			(*it).second = NULL;
		}
		this->themeColours.clear();
		this->initted = false;
	}
}

void ShishaTheme::drawText(string text, float x, float y, bool center){
	if(center){
		x -= text.length() * 5.0;
		y += 5.0;
	}
	if(x < 6.0)
		x = 6.0;
	ofDrawBitmapString(text, x, y);
}

string ShishaTheme::getThemeName(){
	return this->themeName;
}

float ShishaTheme::getTextWidth(string text){
	return text.length() * 8.2;
}

float ShishaTheme::getTextHeight(string text){
	return 10.0;
}

void ShishaTheme::checkInitted(){
	if(!this->initted)
		this->initTheme();
}

bool ShishaTheme::checkDestroyed(){
	if(this->initted){
		this->destroyTheme();
		return true;
	}
	return false;
}

void ShishaTheme::setColour(int componentID, ShishaElement* element){
	ColourRGBA* colour = this->getColour(componentID, element);
	ofSetColor(colour->r, colour->g, colour->b, colour->a);
}

ColourRGBA* ShishaTheme::getColour(int componentID, ShishaElement* element){
#ifdef MOUSE_BORDERS
	if(componentID == SHISHA_COLOUR_BORDER && element != NULL){
		if(element->getGuiState() == GUI_ELEMENT_STATE_MOUSEOVER)
			componentID = SHISHA_COLOUR_BORDER_MOUSEOVER;
		else if(element->getGuiState() == GUI_ELEMENT_STATE_MOUSEDOWN)
			componentID = SHISHA_COLOUR_BORDER_MOUSEDOWN;
		else if(element->getGuiState() == GUI_ELEMENT_STATE_MOUSEDRAG)
			componentID = SHISHA_COLOUR_BORDER_MOUSEDRAG;
	}
#endif
	if(this->themeColours.find(componentID) == this->themeColours.end())
		componentID = SHISHA_COLOUR_DEFAULT;
	return this->themeColours[componentID];
}

bool ShishaTheme::drawContainerBorders(){
	return this->containerBorders;
}

bool ShishaTheme::drawPanelBorders(){
	return this->panelBorders;
}

bool ShishaTheme::drawButtonBorders(){
	return this->buttonBorders;
}

bool ShishaTheme::drawImageBorders(){
	return this->imageBorders;
}

bool ShishaTheme::drawTextInputBorders(){
	return this->textInputBorders;
}

bool ShishaTheme::drawTextOutputBorders(){
	return this->textOutputBorders;
}
