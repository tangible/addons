/*
 *  MultiCursorManager.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 30/03/09.
 *  Copyright 2009 Tangible Interaction. All rights reserved.
 *
 */

#include "MultiCursorManager.h"

MultiCursorManager gsCursors;

MultiCursorManager::MultiCursorManager(){
}

MultiCursorManager::~MultiCursorManager(){
	this->deleteCursors();
}

void MultiCursorManager::deleteCursors(){
	for(map<string,MultiCursor*>::iterator it=this->cursors.begin(); it != this->cursors.end(); it++){
		delete (*it).second;
		(*it).second = NULL;
	}
	this->cursors.clear();
}

void MultiCursorManager::draw(){
	for(map<string, MultiCursor*>::iterator it=this->cursors.begin() ; it != this->cursors.end(); it++)
		(*it).second->draw();
}

int MultiCursorManager::getCount(){
	return this->cursors.size();
}

map<string, MultiCursor*>* MultiCursorManager::getCursors(){
	return &this->cursors;
}

MultiCursor* MultiCursorManager::getCursor(string cursorID, bool makeNew){
	if(this->cursors.find(cursorID) == this->cursors.end()){
		if(!makeNew)
			return NULL;
		this->cursors[cursorID] = new MultiCursor(cursorID);
	}
	return this->cursors[cursorID];
}

void MultiCursorManager::set(string cursorID, float x, float y, float w, float h, int button, bool dragging){
	MultiCursor* cursor = this->getCursor(cursorID);
	if(cursor != NULL){
		cursor->setPosition(x, y);
		cursor->setSize(w, h);
		if(button >= 0){
			cursor->setLastClick(x, y, !dragging);
		}
		cursor->setButton(button);
	}
}

void MultiCursorManager::unset(string cursorID){
	map<string, MultiCursor*>::iterator cursorIT = this->cursors.find(cursorID);
	if(cursorIT != this->cursors.end()){
		delete (*cursorIT).second;
		(*cursorIT).second = NULL;
		this->cursors.erase(cursorIT);
	}
}

bool MultiCursorManager::hasCursor(string cursorID){
	return (this->cursors.find(cursorID) != this->cursors.end());
}

bool MultiCursorManager::addElement(string cursorID, ShishaElement* element){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->addElement(element);
	return false;
}

int MultiCursorManager::dropElement(string cursorID, ShishaElement* element){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->dropElement(element);
	return -1;
}

void MultiCursorManager::dropElementFromAll(ShishaElement* element){
	vector<string> toDelete;
	for(map<string, MultiCursor*>::iterator cursorIT = this->cursors.begin(); cursorIT != this->cursors.end(); cursorIT++){
		if((*cursorIT).second->dropElement(element) <= 0)
			toDelete.push_back((*cursorIT).first);
	}
	for(int i=0; i < toDelete.size(); i++)
		this->unset(toDelete[i]);
}

string MultiCursorManager::hasElement(ShishaElement* element){
	for(map<string, MultiCursor*>::iterator cursorIT = this->cursors.begin(); cursorIT != this->cursors.end(); cursorIT++){
		if((*cursorIT).second->hasElement(element))
			return (*cursorIT).first;
	}
	return "";
}

float MultiCursorManager::getX(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->getX();
	return 0;
}

float MultiCursorManager::getY(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->getY();
	return 0;
}

float MultiCursorManager::getClickX(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->getClickX();
	return 0;
}

float MultiCursorManager::getClickY(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->getClickY();
	return 0;
}

float MultiCursorManager::getLastX(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->getLastX();
	return 0;
}

float MultiCursorManager::getLastY(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->getLastY();
	return 0;
}

float MultiCursorManager::getMoveX(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->getMoveX();
	return 0;
}

float MultiCursorManager::getMoveY(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->getMoveY();
	return 0;
}

float MultiCursorManager::getMoveAngle(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->getMoveAngle();
	return 0.0;
}

float MultiCursorManager::getMoveDistance(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->getMoveDistance();
	return 0.0;
}

float MultiCursorManager::getWidth(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->getWidth();
	return 0.0;
}

float MultiCursorManager::getHeight(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->getHeight();
	return 0.0;
}

int MultiCursorManager::getButton(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->getButton();
	return 0;
}

bool MultiCursorManager::isClicked(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->isClicked();
	return false;
}


void MultiCursorManager::outputCursors(){
	for(map<string, MultiCursor*>::iterator it=this->cursors.begin() ; it != this->cursors.end(); it++){
		cout << "\t";
		(*it).second->output();
	}
}


MultiCursor::MultiCursor(string cursorID){
	this->init(cursorID);
}

MultiCursor::~MultiCursor(){
}

void MultiCursor::init(string cursorID){
	this->cursorID = cursorID;
	this->cursorIntID = 0;
	this->cursorPort = 0;
	if(this->cursorID != "0" && this->cursorID != MOUSE_ID){
		int pos = this->cursorID.find(':');
		if(pos != string::npos){
			this->cursorIntID = atoi(this->cursorID.substr(pos+1).c_str());
			this->cursorPort = atoi(this->cursorID.substr(0, pos).c_str());
		}
	}
	
	this->x = this->y = this->clickX = this->clickY = this->lastClickX = this->lastClickY = this->lastClickFrame = -1;
	this->dx = this->dy = 0;
	this->width = this->height = 1.0;
	this->distanceMoved = this->angleMoved = 0.0;
	this->button = 0;
}

void MultiCursor::refreshMovementVectors(){
	this->dx = this->clickX - this->lastClickX;
	this->dy = this->clickY - this->lastClickY;
	this->distanceMoved = tiDistance(this->dx, this->dy);
	this->angleMoved = tiAngle(this->dx, this->dy);
}

void MultiCursor::draw(){
	float cursorRadius = 10;
	ofLine(this->x, this->y-this->height/2.0, this->x, this->y+this->height/2.0);
	ofLine(this->x-this->width/2.0, this->y, this->x+this->width/2.0, this->y);
}

bool MultiCursor::addElement(ShishaElement* element){
	for(list<ShishaElement*>::iterator it = this->elements.begin(); it != this->elements.end(); it++){
		if((*it) == element)
			return false;
	}
/**	for(int i=0; i < this->elements.size(); i++){
		if(this->elements[i] == element)
			return false;
	}*/
	this->elements.push_back(element);
	return true;
}

int MultiCursor::dropElement(ShishaElement* element){
	bool found = false;
	for(list<ShishaElement*>::iterator it=this->elements.begin(); it != this->elements.end(); it++){
		if((*it) == element){
			found = true;
			this->elements.erase(it);
			break;
		}
	}
	return this->elements.size();
}

bool MultiCursor::hasElement(ShishaElement* element){
	for(list<ShishaElement*>::iterator it=this->elements.begin(); it != this->elements.end(); it++){
		if((*it) == element)
			return true;
	}
	return false;
}

int MultiCursor::getButton(){
	return this->button;
}

bool MultiCursor::isClicked(){
	return (this->button > 0);
}

void MultiCursor::setButton(int button){
	this->button = button;
}

void MultiCursor::setClickLocation(float x, float y){
	this->clickX = x;
	this->clickY = y;
}

void MultiCursor::setLastClick(float x, float y, bool reset){
	if(ofGetFrameNum() == this->lastClickFrame)
		return;
	if(!reset && this->clickX == -1 && this->clickY == -1)
		reset = true;
	if(!reset){
		this->lastClickX = this->clickX;
		this->lastClickY = this->clickY;
		this->lastClickFrame = ofGetFrameNum();
	}
	this->clickX = x;
	this->clickY = y;
	if(reset){
		this->lastClickX = this->clickX;
		this->lastClickY = this->clickY;
		this->lastClickFrame = ofGetFrameNum();
	}
	this->refreshMovementVectors();
}

void MultiCursor::setLastClickLocation(float x, float y){
	this->lastClickX = x;
	this->lastClickY = y;
}

void MultiCursor::setPosition(float x, float y){
	this->x = x;
	this->y = y;
}

void MultiCursor::setSize(float width, float height){
	this->width = width;
	this->height = height;
}

string MultiCursor::getCursorID(){
	return this->cursorID;
}

int MultiCursor::getCursorIntID(){
	return this->cursorIntID;
}

int MultiCursor::getCursorPort(){
	return this->cursorPort;
}

float MultiCursor::getX(){
	return this->x;
}

float MultiCursor::getY(){
	return this->y;
}

float MultiCursor::getClickX(){
	return this->clickX;
}

float MultiCursor::getClickY(){
	return this->clickY;
}

float MultiCursor::getLastX(){
	return this->lastClickX;
}

float MultiCursor::getLastY(){
	return this->lastClickY;
}

float MultiCursor::getMoveX(){
	return this->dx;
}

float MultiCursor::getMoveY(){
	return this->dy;
}

float MultiCursor::getMoveAngle(){
	return this->angleMoved;
}

float MultiCursor::getMoveDistance(){
	return this->distanceMoved;
}

float MultiCursor::getWidth(){
	return this->width;
}

float MultiCursor::getHeight(){
	return this->height;
}

void MultiCursor::output(){
	cout << this->cursorID << ":" << this->getX() << "," << this->getY() << ":" << endl;
//	for(int i=0; i < this->elements.size(); i++){
	int i = 0;
	for(list<ShishaElement*>::iterator it = this->elements.begin(); it != this->elements.end(); it++){
		cout << "\t[" << i++ << "]=" << (*it)->getName() << ":" << endl;
	}
}
