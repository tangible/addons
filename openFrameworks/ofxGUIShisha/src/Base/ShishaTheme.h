/*
 *  ShishaTheme.h
 *  openFrameworks
 *
 *  Created by Pat Long on 23/04/09.
 *  Copyright 2009 Tangible Interaction. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_SHISHA_THEME
#define _OFX_GUISHISHA_SHISHA_THEME

#include "ofxTI_Colours.h"
#include <map>
using namespace std;

class ShishaElement;

//#define DRAW_CURSORS
#define DRAW_BORDERS true
//#define MOUSE_BORDERS
//#define DRAW_HIT_BORDERS

// id's for default colour scheme
#define SHISHA_COLOUR_DEFAULT		-1 // should always be white, and elements will restore to it after drawing (prevents mucking with future draws)
#define SHISHA_COLOUR_BACKGROUND	0
#define SHISHA_COLOUR_BORDER		1
#define SHISHA_COLOUR_FOREGROUND_1	2
#define SHISHA_COLOUR_FOREGROUND_2	3
#define SHISHA_COLOUR_CURSORS		4

#define SHISHA_COLOUR_BORDER_MOUSEOVER 5
#define SHISHA_COLOUR_BORDER_MOUSEDOWN 6
#define SHISHA_COLOUR_BORDER_MOUSEDRAG 7

class ShishaTheme{
	protected:
		string themeName;
		map<int,ColourRGBA*> themeColours;
		bool containerBorders, panelBorders, buttonBorders, imageBorders, textInputBorders, textOutputBorders;
		bool initted;
	
		virtual void initTheme();
		virtual void destroyTheme();
	
	public:
		ShishaTheme();
		~ShishaTheme();
	
		virtual string getThemeName();
	
		virtual void drawText(string text, float x, float y, bool center=true);
		virtual float getTextWidth(string text);
		virtual float getTextHeight(string text);
	
		void checkInitted();
		bool checkDestroyed();
	
		ColourRGBA* getColour(int componentID=SHISHA_COLOUR_DEFAULT, ShishaElement* element=NULL);
		void setColour(int componentID=SHISHA_COLOUR_DEFAULT, ShishaElement* element=NULL);
	
		bool drawContainerBorders();
		bool drawPanelBorders();
		bool drawButtonBorders();
		bool drawImageBorders();
		bool drawTextInputBorders();
		bool drawTextOutputBorders();
	
};

#include "ShishaElement.h"

#endif
