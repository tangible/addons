/*
 *  MultiCursorManager.h
 *  openFrameworks
 *
 *  Created by Pat Long on 30/03/09.
 *  Copyright 2009 Tangible Interaction. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_CURSOR_MANAGER
#define _OFX_GUISHISHA_CURSOR_MANAGER

#include "ofMain.h"
#include "ofxTI_Utils.h"

class ShishaElement;

#define MOUSE_ID "mouse"

// a cursor base class for multi-touch cursors
class MultiCursor{
protected:
	list<ShishaElement*> elements;
	
	string cursorID;
	int cursorIntID, cursorPort;
	float x, y, clickX, clickY, lastClickX, lastClickY, lastClickFrame;
	float dx, dy;
	float distanceMoved, angleMoved, width, height;
	int button;
	
	void init(string cursorID);
	void refreshMovementVectors();
	void refreshReleaseVectors();
	
public:
	MultiCursor(string cursorID);
	~MultiCursor();
	
	virtual void draw();
	
	virtual bool addElement(ShishaElement* element);
	virtual int dropElement(ShishaElement* element);
	virtual bool hasElement(ShishaElement* element);
	
	virtual void setButton(int button);
	virtual void setClickLocation(float x, float y);
	virtual void setLastClick(float x, float y, bool reset=false);
	virtual void setLastClickLocation(float x, float y);
	virtual void setPosition(float x, float y);
	virtual void setSize(float width, float height);
	
	string getCursorID();
	int getCursorIntID();
	int getCursorPort();
	float getX();
	float getY();
	float getClickX();
	float getClickY();
	float getLastX();
	float getLastY();
	float getMoveX();
	float getMoveY();
	float getMoveAngle();
	float getMoveDistance();

	float getWidth();
	float getHeight();
	
	int getButton();
	bool isClicked();
	
	virtual void output();
	
};

// manages a set of cursors for a multi-touchable object
class MultiCursorManager{
protected:
	map<string, MultiCursor*> cursors;
	
	virtual MultiCursor* getCursor(string cursorID, bool makeNew=true);
	
public:
	MultiCursorManager();
	~MultiCursorManager();
	virtual void deleteCursors();
	
	virtual void draw();
	
	virtual void set(string cursorID, float x, float y, float w=1.0, float h=1.0, int button=-1, bool dragging=false);
	virtual void unset(string cursorID);
	
	bool hasCursor(string cursorID);
	
	bool addElement(string cursorID, ShishaElement* element);
	int dropElement(string cursorID, ShishaElement* element);
	void dropElementFromAll(ShishaElement* element);
	string hasElement(ShishaElement* element);
	
	int getCount();
	map<string, MultiCursor*>* getCursors();
	float getX(string cursorID);
	float getY(string cursorID);
	float getClickX(string cursorID);
	float getClickY(string cursorID);
	float getLastX(string cursorID);
	float getLastY(string cursorID);
	float getMoveX(string cursorID);
	float getMoveY(string cursorID);
	float getMoveAngle(string cursorID);
	float getMoveDistance(string cursorID);
	float getWidth(string cursorID);
	float getHeight(string cursorID);
	int getButton(string cursorID);
	
	bool isClicked(string cursorID);
	
	virtual void outputCursors();
};

extern MultiCursorManager gsCursors;

#include "ShishaElement.h"

#endif
