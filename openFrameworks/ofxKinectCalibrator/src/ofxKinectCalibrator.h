/*
 *  ofxKinectCalibrator.h
 *  emptyExample
 *
 *  Created by Pat Long (plong0) on 11-03-02.
 *  Copyright 2011 Tangible Interaction. All rights reserved.
 *
 */
#pragma once

#include "ofMain.h"
#include "ofxKinect.h"
#include "ofxOpenCv.h"

#include "ofxScalableRectangle.h"

enum KinectCalibDrawMode {KC_COLOR, KC_DEPTH};

class ofxKinectCalibrator : public ofxScalableRectangle{
protected:
	ofxKinect* kinect;
	
	ofxCvColorImage baseColor;
	ofxCvGrayscaleImage baseDepth;
	
	ofxCvColorImage	calibratedColor;
	ofxCvGrayscaleImage calibratedDepth;
	
	bool mirrorHorizontal, mirrorVertical;
	
	KinectCalibDrawMode drawMode;
	
	unsigned char* cropPixels(unsigned char* srcPix, int srcW, int srcH, int srcBpp, int targetX, int targetY, int targetW, int targetH);
		
public:
	ofxKinectCalibrator(ofxKinect* kinect=NULL);
		
	virtual void drawContent(float x, float y, float w, float h);
	
	void drawBase();
	void drawBase(float x, float y);
	void drawBase(float x, float y, float w, float h);
	
	void drawBaseDepth();
	void drawBaseDepth(float x, float y);
	void drawBaseDepth(float x, float y, float w, float h);
	
	void drawCalibrated();
	void drawCalibrated(float x, float y);
	void drawCalibrated(float x, float y, float w, float h);
	
	void drawCalibratedDepth();
	void drawCalibratedDepth(float x, float y);
	void drawCalibratedDepth(float x, float y, float w, float h);
	
	void update();
	
	float getBaseWidth();
	float getBaseHeight();
	unsigned char * getBasePixels();
	unsigned char * getBaseDepthPixels();
	
	float getCalibratedWidth();
	float getCalibratedHeight();
	unsigned char * getCalibratedPixels();
	unsigned char * getCalibratedDepthPixels();
	
	KinectCalibDrawMode getDrawMode();
	
	bool isFrameNew();
	void setKinect(ofxKinect* kinect);

	void nextDrawMode();
	void prevDrawMode();
	void setDrawMode(KinectCalibDrawMode drawMode);
	void setMirror(bool mirrorHorizontal=false, bool mirrorVertical=false);
};
