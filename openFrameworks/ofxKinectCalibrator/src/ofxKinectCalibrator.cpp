/*
 *  ofxKinectCalibrator.cpp
 *  emptyExample
 *
 *  Created by Pat Long (plong0) on 11-03-02.
 *  Copyright 2011 Tangible Interaction. All rights reserved.
 *
 */

#include "ofxKinectCalibrator.h"

ofxKinectCalibrator::ofxKinectCalibrator(ofxKinect* kinect){
	init();
	calibratedColor.allocate(640, 480);
	calibratedDepth.allocate(640, 480);
	
	drawMode = KC_COLOR;
	mirrorHorizontal = false;
	mirrorVertical = false;

	this->setKinect(kinect);
}

void ofxKinectCalibrator::drawContent(float x, float y, float w, float h){
	if(this->kinect != NULL){
		if(drawMode == KC_COLOR)
			baseColor.draw(x, y, w, h);
		else
			baseDepth.draw(x, y, w, h);
	}
}

void ofxKinectCalibrator::drawBase(){
	this->drawBase(0.0, 0.0);
}

void ofxKinectCalibrator::drawBase(float x, float y){
	this->drawBase(x, y, 0.0, 0.0);
}

void ofxKinectCalibrator::drawBase(float x, float y, float w, float h){
	if(w <= 0.0) w = baseColor.getWidth();
	if(h <= 0.0) h = baseColor.getHeight();
	baseColor.draw(x, y, w, h);
}

void ofxKinectCalibrator::drawBaseDepth(){
	this->drawBaseDepth(0.0, 0.0);
}

void ofxKinectCalibrator::drawBaseDepth(float x, float y){
	this->drawBaseDepth(x, y, 0.0, 0.0);
}

void ofxKinectCalibrator::drawBaseDepth(float x, float y, float w, float h){
	if(w <= 0.0) w = baseDepth.getWidth();
	if(h <= 0.0) h = baseDepth.getHeight();
	baseDepth.draw(x, y, w, h);
}

void ofxKinectCalibrator::drawCalibrated(){
	this->drawCalibrated(0.0, 0.0);
}

void ofxKinectCalibrator::drawCalibrated(float x, float y){
	this->drawCalibrated(x, y, 0.0, 0.0);
}

void ofxKinectCalibrator::drawCalibrated(float x, float y, float w, float h){
	if(w <= 0.0) w = (this->kinect != NULL)?this->kinect->getWidth():1.0;
	if(h <= 0.0) h = (this->kinect != NULL)?this->kinect->getHeight():1.0;
	if(this->kinect != NULL){
		calibratedColor.draw(x, y, w, h);
	}
}

void ofxKinectCalibrator::drawCalibratedDepth(){
	this->drawCalibratedDepth(0.0, 0.0);
}

void ofxKinectCalibrator::drawCalibratedDepth(float x, float y){
	this->drawCalibratedDepth(x, y, 0.0, 0.0);
}

void ofxKinectCalibrator::drawCalibratedDepth(float x, float y, float w, float h){
	if(w <= 0.0) w = (this->kinect != NULL)?this->kinect->getWidth():1.0;
	if(h <= 0.0) h = (this->kinect != NULL)?this->kinect->getHeight():1.0;
	if(this->kinect != NULL){
		calibratedDepth.draw(x, y, w, h);
	}
}

unsigned char* ofxKinectCalibrator::cropPixels(unsigned char* srcPix, int srcW, int srcH, int srcBpp, int targetX, int targetY, int targetW, int targetH){
	unsigned char*  cropPix = new unsigned char[(int)(targetW*targetH*srcBpp)];
	
	for(int i=targetX; i < targetX+targetW && i < srcW; i++){
		for(int j=targetY; j < targetY+targetH && j < srcH; j++){
			int cX = i - targetX;
			int cY = j - targetY;
			for(int b=0; b < srcBpp; b++){
				cropPix[(cY*targetW+cX)*srcBpp + b] = srcPix[(j*srcW+i)*srcBpp + b];
			}
		}
	}
	
	return cropPix;
}

void ofxKinectCalibrator::update(){
	if(this->kinect != NULL && this->kinect->isFrameNew()){
		baseColor.setFromPixels(kinect->getPixels(), kinect->getWidth(), kinect->getHeight());
		baseDepth.setFromPixels(kinect->getDepthPixels(), kinect->getWidth(), kinect->getHeight());
		
		baseColor.mirror(this->mirrorVertical, this->mirrorHorizontal);
		baseDepth.mirror(this->mirrorVertical, this->mirrorHorizontal);
		
		unsigned char* kinectPixels;
		unsigned char* cropPix;
		
		int kinectW = this->kinect->getWidth();
		int kinectH = this->kinect->getHeight();
		int calibX = normToContentX(scaleBox.x);
		int calibY = normToContentY(scaleBox.y);
		int calibW = normToContentX(scaleBox.width);
		int calibH = normToContentY(scaleBox.height);
		
		// crop colour
		kinectPixels = baseColor.getPixels();
		cropPix = this->cropPixels(kinectPixels, kinectW, kinectH, 3, calibX, calibY, calibW, calibH);
		
		if(!calibratedColor.bAllocated)
			calibratedColor.allocate(calibW, calibH);
		else if(calibratedColor.getWidth() != calibW || calibratedColor.getHeight() != calibH)
			calibratedColor.resize(calibW, calibH);
		
		calibratedColor.setFromPixels(cropPix, calibW, calibH);
		delete [] cropPix;
		cropPix = NULL;
		
		// crop depth
		kinectPixels = baseDepth.getPixels();
		cropPix = this->cropPixels(kinectPixels, kinectW, kinectH, 1, calibX, calibY, calibW, calibH);
		
		if(!calibratedDepth.bAllocated)
			calibratedDepth.allocate(calibW, calibH);
		else if(calibratedDepth.getWidth() != calibW || calibratedDepth.getHeight() != calibH)
			calibratedDepth.resize(calibW, calibH);
		
		calibratedDepth.setFromPixels(cropPix, calibW, calibH);
		delete [] cropPix;
		cropPix = NULL;
		
		bIsContentChanged = true;
	}
}

float ofxKinectCalibrator::getBaseWidth(){
	return baseColor.getWidth();
}

float ofxKinectCalibrator::getBaseHeight(){
	return baseColor.getHeight();
}

unsigned char * ofxKinectCalibrator::getBasePixels(){
	return baseColor.getPixels();
}

unsigned char * ofxKinectCalibrator::getBaseDepthPixels(){
	return baseDepth.getPixels();
}

float ofxKinectCalibrator::getCalibratedWidth(){
	return normToContentX(scaleBox.width);
}

float ofxKinectCalibrator::getCalibratedHeight(){
	return normToContentY(scaleBox.height);
}

unsigned char * ofxKinectCalibrator::getCalibratedPixels(){
	return this->calibratedColor.getPixels();
}

unsigned char * ofxKinectCalibrator::getCalibratedDepthPixels(){
	return this->calibratedDepth.getPixels();
}

KinectCalibDrawMode ofxKinectCalibrator::getDrawMode(){
	return this->drawMode;
}

bool ofxKinectCalibrator::isFrameNew(){
	return (this->kinect != NULL)?this->kinect->isFrameNew():false;
}

void ofxKinectCalibrator::setKinect(ofxKinect* kinect){
	this->kinect = kinect;
	if(this->kinect != NULL){
		baseColor.allocate(kinect->getWidth(), kinect->getHeight());
		baseDepth.allocate(kinect->getWidth(), kinect->getHeight());
		this->setContentSize(this->kinect->getWidth(), this->kinect->getHeight());
		this->scaleContent();
	}
}

void ofxKinectCalibrator::nextDrawMode(){
	if(this->drawMode == KC_COLOR)
		this->setDrawMode(KC_DEPTH);
	else
		this->setDrawMode(KC_COLOR);
}

void ofxKinectCalibrator::prevDrawMode(){
	if(this->drawMode == KC_COLOR)
		this->setDrawMode(KC_DEPTH);
	else
		this->setDrawMode(KC_COLOR);
}

void ofxKinectCalibrator::setDrawMode(KinectCalibDrawMode drawMode){
	this->drawMode = drawMode;
}

void ofxKinectCalibrator::setMirror(bool mirrorHorizontal, bool mirrorVertical){
	this->mirrorHorizontal = mirrorHorizontal;
	this->mirrorVertical = mirrorVertical;
}

