/*
 *  ofxScalableImage.cpp
 *  emptyExample
 *
 *  Created by Pat Long (plong0) on 11-03-22.
 *  Copyright 2011 Tangible Interaction. All rights reserved.
 *
 */

#include "ofxScalableImage.h"

ofxScalableImage::ofxScalableImage(){
	init();
}

ofxScalableImage::ofxScalableImage(float x, float y, float w, float h):ofxScalableRectangle(x, y, w, h){
	init();
}

ofxScalableImage::~ofxScalableImage(){
}

void ofxScalableImage::init(){
	ofxScalableRectangle::init();
	this->setImage();
}

void ofxScalableImage::drawContent(float x, float y, float w, float h){
	if(image != NULL)
		image->draw(x, y, w, h);
}

void ofxScalableImage::drawScaledContent(float x, float y, float w, float h){
	scaledImage.draw(x, y, w, h);
}

void ofxScalableImage::cropImage(ofImage* sourceImage, ofImage* targetImage, ofRectangle cropBounds){
	if(sourceImage != NULL && targetImage != NULL && cropBounds.width > 0.0 && cropBounds.height > 0.0){
		int srcW = sourceImage->width;
		int srcH = sourceImage->height;
		int bpp = sourceImage->bpp / 8;
		int tarW = cropBounds.width;
		int tarH = cropBounds.height;
		int tarX = cropBounds.x;
		int tarY = cropBounds.y;
		unsigned char* srcPix = sourceImage->getPixels();
		unsigned char* cropPix = new unsigned char[(tarW*tarH*bpp)];
		
		for(int i=tarX; i < tarX+tarW && i < srcW; i++){
			for(int j=tarY; j < tarY+tarH && j < srcH; j++){
				int cX = i - tarX;
				int cY = j - tarY;
				for(int b=0; b < bpp; b++){
					cropPix[(cY*tarW+cX)*bpp + b] = srcPix[(j*srcW+i)*bpp + b];
				}
			}
		}
		
		targetImage->allocate(tarW, tarH, sourceImage->type);
		targetImage->setFromPixels(cropPix, tarW, tarH, sourceImage->type);
		delete [] cropPix;
		cropPix = NULL;
	}
}

void ofxScalableImage::scaleContent(){
	ofxScalableRectangle::scaleContent();
	ofRectangle cropBounds = this->getScaledBounds();
	cropImage(this->image, &scaledImage, cropBounds);
}

ofImage* ofxScalableImage::getImage(){
	return this->image;
}

void ofxScalableImage::setImage(ofImage* image){
	this->image = image;
	if(image != NULL)
		this->setContentSize(image->getWidth(), image->getHeight());
	this->scaleContent();
}
