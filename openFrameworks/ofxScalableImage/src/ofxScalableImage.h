/*
 *  ofxScalableImage.h
 *  emptyExample
 *
 *  Created by Pat Long (plong0) on 11-03-22.
 *  Copyright 2011 Tangible Interaction. All rights reserved.
 *
 */
#pragma once

#include "ofMain.h"
#include "ofxScalableRectangle.h"

class ofxScalableImage : public ofxScalableRectangle{
protected:
	ofImage* image;
	ofImage scaledImage;
	
	virtual void init();
	
	void cropImage(ofImage* sourceImage, ofImage* targetImage, ofRectangle cropBounds);
	
public:
	ofxScalableImage();
	ofxScalableImage(float x, float y, float w, float h);
	~ofxScalableImage();
	
	virtual void drawContent(float x, float y, float w, float h);
	virtual void drawScaledContent(float x, float y, float w, float h);
	
	virtual void scaleContent();
	
	ofImage* getImage();
	void setImage(ofImage* image=NULL);
};
