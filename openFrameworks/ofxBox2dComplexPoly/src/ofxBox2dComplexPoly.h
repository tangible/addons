/*
 *  ofxBox2dComplexPoly.h
 *  emptyExample
 *
 *	Original code by Akira Hayasaka. (http://vimeo.com/9951522)
 *  Wrapped into addon by Pat Long (plong0) on 11-02-14.
 *  Copyright 2011 Tangible Interaction. All rights reserved.
 *
 */
#pragma once

#include "ofxBox2d.h"
#include "ofxTriangle.h"

class ofxBox2dComplexPoly : public ofxBox2dBaseShape{
	protected:
	
	
		
	public:
		ofxBox2dComplexPoly(){
		}
		
		~ofxBox2dComplexPoly(){
		}
	
		//--------------------------------------------------------------
		void setup(b2World * b2dworld, float x, float y, vector<ofxTriangleData> triangles, ofPoint scale=ofPoint(1.0,1.0), bool isFixed=false){
			if(b2dworld == NULL) {
				ofLog(OF_LOG_NOTICE, "- must have a valid world -");
				return;
			}
			
			world				= b2dworld;
			bIsFixed			= isFixed;
			
			if(!bIsFixed){
				if(mass <= 0.0 && bounce <= 0.0 && friction <= 0.0)
					setPhysics(1.0, 0.75, 0.25);
			}
			
			this->body = getComplexBody(triangles, x, y, scale);
		}
	
		virtual void setPhysics(float m, float bnc, float frc) {
			ofxBox2dBaseShape::setPhysics(m, bnc, frc);
			b2Body*	cBody = this->body;
			if(cBody != NULL){
				b2Shape* cShape = cBody->GetShapeList();
				while(cShape != NULL){
					cShape->SetDensity(m);
					cShape->SetRestitution(bnc);
					cShape->SetFriction(frc);
					cShape = cShape->GetNext();
				}
				cBody->SetMassFromShapes();
			}
		}
	
		//--------------------------------------------------------------
		b2Body* getComplexBody(vector<ofxTriangleData> triangles, float x, float y, ofPoint scale=ofPoint(1.0,1.0)){
			b2BodyDef* bodyDef = new b2BodyDef();
			bodyDef->position.Set(x/OFX_BOX2D_SCALE, y/OFX_BOX2D_SCALE);
			bodyDef->linearDamping = 1.0;
			bodyDef->angularDamping = 1.0;
			
			b2Body* body = world->CreateBody(bodyDef);
			makeComplexBody(body, triangles, scale);
			
			return body;
		}
		
		//--------------------------------------------------------------
		//	http://www.psyked.co.uk/box2d/simple-box2d-custom-polygon-creation.htm
		void makeComplexBody(b2Body* body, vector<ofxTriangleData> triangles, ofPoint scale=ofPoint(1.0,1.0)) {	
			for(int i = 0; i < triangles.size(); i++){
				b2PolygonDef* shapeDef = new b2PolygonDef();
				
				if(bIsFixed) {
					shapeDef->density = 0.0;
					shapeDef->restitution = 0.0;
					shapeDef->friction = 0.0;
				}
				else{
					shapeDef->density = mass;
					shapeDef->restitution = bounce;
					shapeDef->friction = friction;
				}
				
				shapeDef->vertexCount = 3;
				shapeDef->vertices[0].Set(triangles[i].a.x*scale.x/OFX_BOX2D_SCALE, triangles[i].a.y*scale.y/OFX_BOX2D_SCALE);
				shapeDef->vertices[1].Set(triangles[i].b.x*scale.x/OFX_BOX2D_SCALE, triangles[i].b.y*scale.y/OFX_BOX2D_SCALE);
				shapeDef->vertices[2].Set(triangles[i].c.x*scale.x/OFX_BOX2D_SCALE, triangles[i].c.y*scale.y/OFX_BOX2D_SCALE);
				
				body->CreateShape(shapeDef);
			}
			
			body->SetMassFromShapes();
		}
	
		virtual void draw(){
			if(body != NULL){
				vector<b2Vec2> verts;
				const b2XForm& xf = body->GetXForm();
				b2Shape* shape = body->GetShapeList();
				
				while(shape != NULL){
					b2PolygonShape* poly = (b2PolygonShape*)shape;
					
					const b2Vec2* localVertices = poly->GetVertices();
					int count = poly->GetVertexCount();
					
					for(int j = 0; j < count; j++) {
						verts.push_back(b2Mul(xf, localVertices[j]));
					}
					
					shape = shape->GetNext();
				}
				
				glBegin(GL_TRIANGLES); 
				for(int j = 0; j < verts.size(); j++) {
					glVertex3f(verts[j].x*OFX_BOX2D_SCALE, verts[j].y*OFX_BOX2D_SCALE, 0);
				}
				glEnd();
			}
		}
};
