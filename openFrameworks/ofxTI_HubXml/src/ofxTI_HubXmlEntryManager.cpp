/*
 *  ofxTI_HubXmlEntryManager.cpp
 *  VoiceIt_Kiosk
 *
 *  Created by Pat Long (plong0) on 11-02-24.
 *  Copyright 2011 Tangible Interaction. All rights reserved.
 *
 */

#include "ofxTI_HubXmlEntryManager.h"

ofxTI_HubXmlEntryManager::ofxTI_HubXmlEntryManager(HubEntryApproveMode approveMode, string approvedDir, string deniedDir, string pendingDir, string updatedDir){
	this->setApproveMode(approveMode);
	this->setApprovedDir(approvedDir);
	this->setDeniedDir(deniedDir);
	this->setPendingDir(pendingDir);
	this->setUpdatedDir(updatedDir);
}

ofxTI_HubXmlEntryManager::~ofxTI_HubXmlEntryManager(){
}

void ofxTI_HubXmlEntryManager::update(){
	/**
	 HubPad will retrieve list of approved and denied entries
	 HubPad will move pending entries to approved or denied directories
	 */
	
	checkForUpdated();
	
	if(this->approveMode == DENY_UNTIL_APPROVED){
		// Copy "pending" entries to the Denied folder
		copyNewPendingToDenied();
	}
	else if(this->approveMode == APPROVE_UNTIL_DENIED){
		// Copy "pending" entries to the Approved folder
		copyNewPendingToApproved();
	}
}

string ofxTI_HubXmlEntryManager::getNextNewApproved(){
	string result = "";
	
	if(newApproved.size() > 0){
		result = newApproved.front();
		newApproved.erase(newApproved.begin());
	}
	
	return result;
}

string ofxTI_HubXmlEntryManager::getNextNewDenied(){
	string result = "";
	
	if(newDenied.size() > 0){
		result = newDenied.front();
		newDenied.erase(newDenied.begin());
	}
	
	return result;
}

HubEntryApproveMode ofxTI_HubXmlEntryManager::getApproveMode(){
	return this->approveMode;
}

bool ofxTI_HubXmlEntryManager::isInApproved(string entryName){
	bool result = false;
	
	if(entryName != "" && this->approvedDir != ""){
		stringstream path;
		path << ofToDataPath(this->approvedDir, true) << "/" << entryName;
		result = tiFileExists(path.str(), true);
	}
	
	return result;
}

bool ofxTI_HubXmlEntryManager::isInDenied(string entryName){
	bool result = false;
	
	if(entryName != "" && this->deniedDir != ""){
		stringstream path;
		path << ofToDataPath(this->deniedDir, true) << "/" << entryName;
		result = tiFileExists(path.str(), true);
	}
	
	return result;
}

bool ofxTI_HubXmlEntryManager::isInPending(string entryName){
	bool result = false;
	
	if(entryName != "" && this->pendingDir != ""){
		stringstream path;
		path << ofToDataPath(this->pendingDir, true) << "/" << entryName;
		result = tiFileExists(path.str(), true);
	}
	
	return result;
}

void ofxTI_HubXmlEntryManager::removeFromApproved(string entryName){
	if(entryName != "" && this->approvedDir != ""){
		stringstream path;
		path << ofToDataPath(this->approvedDir, true) << "/" << entryName;
		tiRemoveFile(path.str(), true);
	}
}

void ofxTI_HubXmlEntryManager::removeFromDenied(string entryName){
	if(entryName != "" && this->deniedDir != ""){
		stringstream path;
		path << ofToDataPath(this->deniedDir, true) << "/" << entryName;
		tiRemoveFile(path.str(), true);
	}
}

void ofxTI_HubXmlEntryManager::removeFromPending(string entryName){
	if(entryName != "" && this->pendingDir != ""){
		stringstream path;
		path << ofToDataPath(this->pendingDir, true) << "/" << entryName;
		tiRemoveFile(path.str(), true);
	}
}

void ofxTI_HubXmlEntryManager::removeFromUpdated(string entryName){
	if(entryName != "" && this->updatedDir != ""){
		stringstream path;
		path << ofToDataPath(this->updatedDir, true) << "/" << entryName;
		tiRemoveFile(path.str(), true);
	}
}

int ofxTI_HubXmlEntryManager::scanApprovedDir(){
	int result = 0;
	if(this->approvedDir != ""){
		ofxDirList dirList;
		int dirCount = dirList.listDir(this->approvedDir);
		for(int i=0; i < dirCount; i++){
			this->newApproved.push_back(dirList.getName(i));
			result++;
		}
	}
	return result;
}

int ofxTI_HubXmlEntryManager::scanDeniedDir(){
	int result = 0;
	if(this->deniedDir != ""){
		ofxDirList dirList;
		int dirCount = dirList.listDir(this->deniedDir);
		for(int i=0; i < dirCount; i++){
			this->newDenied.push_back(dirList.getName(i));
			result++;
		}
	}
	return result;
}

int ofxTI_HubXmlEntryManager::checkForUpdated(){
	int result = 0;
	
	if(updatedDir != ""){
		ofxDirList dir;
		int dirCount = dir.listDir(updatedDir);
		
		for(int i=0; i < dirCount; i++){
			string cName = dir.getName(i);
			
			if(this->isInPending(cName))
				this->removeFromPending(cName);
								
			if(this->isInApproved(cName))
				newApproved.push_back(cName);
			
			if(this->isInDenied(cName))
				newDenied.push_back(cName);
			
			this->removeFromUpdated(cName);
				
			result++;
		}
	}
	
	return result;
}

int ofxTI_HubXmlEntryManager::copyNewPendingToApproved(){
	int result = 0;
	
	if(pendingDir != "" && approvedDir != ""){
		ofxDirList pending;
		int count = pending.listDir(pendingDir);
		for(int i=0; i < count; i++){
			string cName = pending.getName(i);
			if(this->isInDenied(cName))
				this->removeFromDenied(cName);
			
			if(!this->isInApproved(cName)){
				string oldPath = pending.getPath(i);
				stringstream newPath;
				newPath << ofToDataPath(approvedDir, true) << "/" << cName;
				tiCopyFile(oldPath, newPath.str(), true);
				newApproved.push_back(cName);
			}
		}
	}
	
	return result;
}

int ofxTI_HubXmlEntryManager::copyNewPendingToDenied(){
	int result = 0;
	
	if(pendingDir != "" && deniedDir != ""){
		ofxDirList pending;
		int count = pending.listDir(pendingDir);
		for(int i=0; i < count; i++){
			string cName = pending.getName(i);
			if(this->isInApproved(cName))
			   this->removeFromApproved(cName);
			
			if(!this->isInDenied(cName)){
				string oldPath = pending.getPath(i);
				stringstream newPath;
				newPath << ofToDataPath(deniedDir, true) << "/" << cName;
				tiCopyFile(oldPath, newPath.str(), true);
				newDenied.push_back(cName);
			}
		}
	}
	
	return result;
}

void ofxTI_HubXmlEntryManager::setApproveMode(HubEntryApproveMode approveMode){
	this->approveMode = approveMode;
}

void ofxTI_HubXmlEntryManager::setApprovedDir(string approvedDir){
	this->approvedDir = approvedDir;
	this->scanApprovedDir();
}

void ofxTI_HubXmlEntryManager::setDeniedDir(string deniedDir){
	this->deniedDir = deniedDir;
	this->scanDeniedDir();
}

void ofxTI_HubXmlEntryManager::setPendingDir(string pendingDir){
	this->pendingDir = pendingDir;
}

void ofxTI_HubXmlEntryManager::setUpdatedDir(string updatedDir){
	this->updatedDir = updatedDir;
}
