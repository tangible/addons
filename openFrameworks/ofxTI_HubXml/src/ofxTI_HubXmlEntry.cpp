/*
 *  ofxTI_HubXmlEntry.cpp
 *  emptyExample
 *
 *  Created by Pat Long (plong0) on 11-01-26.
 *  Copyright 2011 Tangible Interaction. All rights reserved.
 *
 */

#include "ofxTI_HubXmlEntry.h"

ofxTI_HubXmlEntry::ofxTI_HubXmlEntry(ofxTI_HubXml* hub, string xmlFileName){
	this->setHub(hub);
	this->loadEntryXml(xmlFileName);
}

ofxTI_HubXmlEntry::~ofxTI_HubXmlEntry(){
}

bool ofxTI_HubXmlEntry::loadEntryXml(string xmlFileName){
	bool result = false;
	if(xmlFileName != ""){
		result = this->xml.loadFile(ofToDataPath(xmlFileName));
		if(result)
			this->xmlFileName = xmlFileName;
	}
	return result;
}

bool ofxTI_HubXmlEntry::saveEntryXml(string xmlFileName){
	bool result = false;
	if(xmlFileName == "")
		xmlFileName = this->getXmlFileName();
	
	if(xmlFileName != ""){
		this->pushEntryTag();
		if(!this->xml.tagExists("installation_id") && this->hub != NULL)
			this->xml.addValue("installation_id", this->hub->getInstallationID());
		this->popEntryTag();
		
		this->xml.saveFile(xmlFileName);
	}
	
	return result;
}

void ofxTI_HubXmlEntry::addTextNode(string value, string name){
	this->pushEntryTag();
	int nodeIndex = this->xml.addValue("text", value);
	if(name != "")
		this->xml.addAttribute("text", "name", name, nodeIndex);
	this->popEntryTag();
}

void ofxTI_HubXmlEntry::addImageNode(string fileName, string name){
	if(fileName == "")
		return; // no dice.
	
	int slashPos = fileName.rfind("/");
	string originalPath = "";
	string imagePath = "";
	string imageName = fileName;
	ofxImage theImage;
	bool loadedImage = false;
	
	if(fileName != ""){	
		loadedImage = theImage.loadImage(fileName);
		if(slashPos != string::npos && slashPos > 0){
			originalPath = fileName.substr(0, slashPos+1);
			imageName = fileName.substr(slashPos+1);
		}
	}
	
	if(loadedImage){
		if(this->hub != NULL)
			imagePath = this->hub->getSavePath();
		bool inHubPath = (originalPath == imagePath);
		if(!inHubPath){
			stringstream fullPath;
			fullPath << imagePath << imageName;
			inHubPath = theImage.saveImage(fullPath.str());
			fullPath.str("");
		}
		if(inHubPath){
			this->pushEntryTag();
			int nodeIndex = this->xml.addValue("image", imageName);
			if(name != "")
				this->xml.addAttribute("image", "name", name, nodeIndex);
			this->popEntryTag();
		}
	}
}

void ofxTI_HubXmlEntry::addImageNode(ofxImage image, string name){
	if(image.getFileName() == "" && this->hub != NULL){
		stringstream savePath;
		savePath << this->hub->getSavePath();
		savePath << this->hub->getNewImageName();
		image.saveImage(savePath.str());
		savePath.str("");
	}
	
	if(image.getFileName() != "")
		this->addImageNode(image.getFileName(), name);
}

void ofxTI_HubXmlEntry::pushEntryTag(){
	if(!this->xml.tagExists("entry"))
		this->xml.addTag("entry");
	this->xml.pushTag("entry");
}

void ofxTI_HubXmlEntry::popEntryTag(){
	this->xml.popTag();
}

string ofxTI_HubXmlEntry::getXmlFileName(){
	if(this->xmlFileName == "" && this->hub != NULL)
		this->xmlFileName = this->hub->getNewEntryName();
	return this->xmlFileName;
}

void ofxTI_HubXmlEntry::setHub(ofxTI_HubXml* hub){
	this->hub = hub;
	
	int installationID = ((this->hub != NULL)?this->hub->getInstallationID():-1);
	this->pushEntryTag();
	if(!this->xml.tagExists("installation_id"))
		this->xml.addValue("installation_id", installationID);
	else
		this->xml.setValue("installation_id", installationID, 0);
	this->popEntryTag();
}
