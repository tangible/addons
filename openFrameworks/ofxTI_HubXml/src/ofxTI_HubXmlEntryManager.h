/*
 *  ofxTI_HubXmlEntryManager.h
 *  VoiceIt_Kiosk
 *
 *  Created by Pat Long (plong0) on 11-02-24.
 *  Copyright 2011 Tangible Interaction. All rights reserved.
 *
 */
#ifndef TI_HUBXML_ENTRY_MANAGER
#define TI_HUBXML_ENTRY_MANAGER

#include "ofxTI_HubXmlEntry.h"
#include "ofxXmlSettings.h"
#include "ofxDirList.h"
#include "ofxTI_Utils.h"

enum HubEntryApproveMode {DENY_UNTIL_APPROVED, APPROVE_UNTIL_DENIED};

class ofxTI_HubXmlEntryManager{
protected:	
	HubEntryApproveMode approveMode;
	string approvedDir, deniedDir, pendingDir, updatedDir;
	vector<string> newApproved;
	vector<string> newDenied;
	
	bool isInApproved(string entryName);
	bool isInDenied(string entryName);
	bool isInPending(string entryName);
	
	void removeFromApproved(string entryName);
	void removeFromDenied(string entryName);
	void removeFromPending(string entryName);
	void removeFromUpdated(string entryName);
	
	int scanApprovedDir();
	int scanDeniedDir();
	
	int checkForUpdated();
	
	int copyNewPendingToApproved();
	int copyNewPendingToDenied();
	
public:
	ofxTI_HubXmlEntryManager(HubEntryApproveMode approveMode=DENY_UNTIL_APPROVED, string approvedDir="", string deniedDir="", string pendingDir="", string updatedDir="");
	~ofxTI_HubXmlEntryManager();
	
	virtual void update();
	
	string getNextNewApproved();
	string getNextNewDenied();
	
	HubEntryApproveMode getApproveMode();
	
	void setApproveMode(HubEntryApproveMode approveMode);
	void setApprovedDir(string approvedDir);
	void setDeniedDir(string deniedDir);
	void setPendingDir(string pendingDir);
	void setUpdatedDir(string updatedDir);
};

#endif
