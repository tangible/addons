/*
 *  ofxTI_HubXmlEntry.h
 *  emptyExample
 *
 *  Created by Pat Long (plong0) on 11-01-26.
 *  Copyright 2011 Tangible Interaction. All rights reserved.
 *
 */
#ifndef TI_HUBXML_ENTRY
#define TI_HUBXML_ENTRY

#include "ofxTI_HubXml.h"
#include "ofxXmlSettings.h"
#include "ofxImage.h"

class ofxTI_HubXmlEntry{
	protected:
		ofxTI_HubXml* hub;
		ofxXmlSettings xml;
		string xmlFileName;
	
		void pushEntryTag();
		void popEntryTag();
		
	public:
		ofxTI_HubXmlEntry(ofxTI_HubXml* hub, string xmlFileName="");
		~ofxTI_HubXmlEntry();
	
		bool loadEntryXml(string xmlFileName);
		bool saveEntryXml(string xmlFileName="");
	
		void addTextNode(string value, string name="");
		void addImageNode(string fileName, string name="");
		void addImageNode(ofxImage image, string name="");
	
		string getXmlFileName();
	
		void setHub(ofxTI_HubXml* hub);
};

#endif
