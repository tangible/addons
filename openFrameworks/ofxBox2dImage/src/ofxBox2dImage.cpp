/*
 *  ofxBox2dImage.cpp
 *  voiceIt
 *
 *  Created by Pat Long (plong0) on 10-12-30.
 *  Copyright 2010 Tangible Interaction. All rights reserved.
 *
 */

#include "ofxBox2dImage.h"

ofxBox2dImage::ofxBox2dImage(string bgTexture, string bgMask, ofxBox2d* b2d){
	this->setOffset(0.0, 0.0, false);
	this->setScale(1.0, 1.0, false);
	this->setBox2d(b2d);
	this->loadTexture(bgTexture);
	this->loadMask(bgMask);
	this->setDrawShapes(false);
}

ofxBox2dImage::~ofxBox2dImage(){
	this->clearShapes();
}

void ofxBox2dImage::setBox2d(ofxBox2d* b2d){
	this->b2d = b2d;
}

bool ofxBox2dImage::loadMask(string bgMask){
	bool result = false;
	if(bgMask != ""){
		if(this->mask.loadImage(bgMask)){
			this->mask.setImageType(OF_IMAGE_GRAYSCALE);
			
			ofxCvGrayscaleImage cvMask;
			cvMask.allocate(this->mask.getWidth(), this->mask.getHeight());
			cvMask.setFromPixels(this->mask.getPixels(), this->mask.getWidth(), this->mask.getHeight());
			contour.findContours(cvMask, 20, cvMask.getWidth()*cvMask.getHeight(), 10, true);	
			this->loadSceneShapes();
			result = true;
		}
	}
	return result;
}

void ofxBox2dImage::loadSceneShapes(){
	if(this->b2d != NULL){
		for(int i=0; i < contour.blobs.size(); i++){
			ofxBox2dLine* cLine = new ofxBox2dLine();
			
			for(int j=contour.blobs[i].nPts-1; j>=0; j--)
				cLine->addPoint(offset.x+contour.blobs[i].pts[j].x*scale.x, offset.y+contour.blobs[i].pts[j].y*scale.y);
			cLine->addPoint(offset.x+contour.blobs[i].pts[contour.blobs[i].nPts-1].x*scale.x, offset.y+contour.blobs[i].pts[contour.blobs[i].nPts-1].y*scale.y);
			
			cLine->setPhysics(0.0, 0.0, 0.0);
			cLine->setWorld(this->b2d->getWorld());
			cLine->createShape();
			
			this->sceneShapes.push_back(cLine);
			
			/**				ofxBox2dPolygon* cPoly = new ofxBox2dPolygon();
			 ofPoint loc;
			 loc.x = 0.0;
			 loc.y = 0.0;
			 for(int j=contour.blobs[i].nPts-1; j>=0; j--){
			 ofPoint cPt = contour.blobs[i].pts[j];
			 if(cPt.x < loc.x) loc.x = cPt.x;
			 if(cPt.y < loc.y) loc.y = cPt.y;
			 cPoly->addVertex(cPt.x, cPt.y);
			 }
			 //				cPoly->addVertex(contour.blobs[i].pts[contour.blobs[i].nPts-1].x, contour.blobs[i].pts[contour.blobs[i].nPts-1].y);
			 
			 cPoly->setPhysics(10.0, 10.0, 0.0);
			 //				cPoly->setWorld(b2d->getWorld());
			 cPoly->createShape(b2d->getWorld(), loc.x, loc.y);
			 
			 this->sceneShapes.push_back(cPoly);
			 */
		}
	}
}

void ofxBox2dImage::unloadSceneShapes(){
	for(int i=0; i < this->sceneShapes.size(); i++){
		this->sceneShapes[i]->destroyShape();
		delete this->sceneShapes[i];
		this->sceneShapes[i] = NULL;
	}
	this->sceneShapes.clear();
}

bool ofxBox2dImage::loadTexture(string bgTexture){
	return ((bgTexture != "")?this->texture.loadImage(bgTexture):false);
}

vector<ofxBox2dLine*> ofxBox2dImage::getSceneShapes(){
	return this->sceneShapes;
}

void ofxBox2dImage::draw(){
	this->draw(0, 0);
}

void ofxBox2dImage::draw(float x, float y){
	ofSetColor(255, 255, 255);
	
	float w = this->texture.getWidth()*scale.x;
	float h = this->texture.getHeight()*scale.y;
	this->texture.draw(x + offset.x, y + offset.y, w, h);
	
	if(drawShapes){
		for(int i=0; i < sceneShapes.size(); i++)
			sceneShapes[i]->draw();
	}
}

void ofxBox2dImage::drawOutline(float x, float y){
	float w = this->texture.getWidth()*scale.x;
	float h = this->texture.getHeight()*scale.y;
	ofNoFill();
	ofSetColor(0, 255, 0);
	ofRect(x + offset.x, y + offset.y, w, h);
	ofSetColor(255, 255, 255);
	ofFill();
}

void ofxBox2dImage::update(){
	bool rebuild = false;
	for(int i=0; i < sceneShapes.size(); i++){
		ofPoint pt = sceneShapes[i]->getPosition();
		if(pt.x != pt.x || pt.y != pt.y)
			rebuild = true;
	}
	if(rebuild){
		this->unloadSceneShapes();
		this->loadSceneShapes();
	}
}

void ofxBox2dImage::clearShapes(){
	for(int i=0; i < this->sceneShapes.size(); i++){
		this->sceneShapes[i]->destroyShape();
		delete this->sceneShapes[i];
		this->sceneShapes[i] = NULL;
	}
	this->sceneShapes.clear();
}

void ofxBox2dImage::refreshShapes(){
	this->clearShapes();
	this->loadSceneShapes();
}

void ofxBox2dImage::setDrawShapes(bool drawShapes){
	this->drawShapes = drawShapes;
}

void ofxBox2dImage::setOffset(float offsetX, float offsetY, bool doShapeRefresh){
	this->offset.x = offsetX;
	this->offset.y = offsetY;
	if(doShapeRefresh)
		this->refreshShapes();
}

void ofxBox2dImage::setScale(float scaleX, float scaleY, bool doShapeRefresh){
	this->scale.x = scaleX;
	this->scale.y = scaleY;
	if(doShapeRefresh)
		this->refreshShapes();
}
