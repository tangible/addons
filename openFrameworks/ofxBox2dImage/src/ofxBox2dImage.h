/*
 *  ofxBox2dImage.h
 *  voiceIt
 *
 *  Created by Pat Long (plong0) on 10-12-30.
 *  Copyright 2010 Tangible Interaction. All rights reserved.
 *
 */
#ifndef OFX_BOX2D_IMAGE
#define OFX_BOX2D_IMAGE

#include "ofMain.h"
#include "ofxBox2d.h"
#include "ofxOpenCv.h"

class ofxBox2dImage{
	protected:
		ofImage texture;
		ofImage mask;
		ofxCvContourFinder contour;
		ofxBox2d* b2d;
		vector<ofxBox2dLine*> sceneShapes;
		ofPoint offset;
		ofPoint scale;
	
		bool drawShapes;
	
		void loadSceneShapes();
		void unloadSceneShapes();
		
	public:
		ofxBox2dImage(string bgTexture="", string bgMask="", ofxBox2d* b2d=NULL);
		~ofxBox2dImage();
	
		void setBox2d(ofxBox2d* b2d);
		bool loadMask(string bgMask);
		bool loadTexture(string bgTexture);
	
//		vector<ofxBox2dPolygon*> getSceneShapes();
		vector<ofxBox2dLine*> getSceneShapes();
	
		virtual void draw();
		virtual void draw(float x, float y);
		void drawOutline(float x, float y);
		virtual void update();
	
		void clearShapes();
		void refreshShapes();
	
		void setDrawShapes(bool drawShapes);
		void setOffset(float offsetX=0.0, float offsetY=0.0, bool doShapeRefresh=true);
		void setScale(float scaleX=1.0, float scaleY=1.0, bool doShapeRefresh=true);
};

#endif
