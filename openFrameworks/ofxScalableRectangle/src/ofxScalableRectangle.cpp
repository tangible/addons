/*
 *  ofxScalableRectangle.cpp
 *  emptyExample
 *
 *  Created by Pat Long (plong0) on 11-03-22.
 *  Copyright 2011 Tangible Interaction. All rights reserved.
 *
 */

#include "ofxScalableRectangle.h"

ofxScalableRectangle::ofxScalableRectangle(){
	init();
}

ofxScalableRectangle::ofxScalableRectangle(float x, float y, float w, float h):ofRectangle(x, y, w, h){
	init();
}

ofxScalableRectangle::~ofxScalableRectangle(){
}

void ofxScalableRectangle::init(){
	setContentSize();
	setScaledSize();
	centerDragEnabled = false;
	guiBoundsWidth = 10.0;
	guiDragTop = guiDragRight = guiDragBottom = guiDragLeft = guiDragCenter = false;
	scaling = false;
	scaleRealTime = false;
	bIsContentChanged = false;
	bIsScaleChanged = false;
}

void ofxScalableRectangle::draw(){
	this->draw(this->x, this->y);
}

void ofxScalableRectangle::draw(float x, float y){
	this->draw(x, y, this->width, this->height);
}

void ofxScalableRectangle::draw(float x, float y, float w, float h){
	ofSetColor(255, 255, 255);
	
	if(this->x != x) this->x = x;
	if(this->y != y) this->y = y;
	if(this->width != w) this->width = w;
	if(this->height != h) this->height = h;
	
	drawContent(x, y, w, h);
	
	ofNoFill();
	ofSetColor(0, 255, 255);
	ofRect(x, y, w, h);
	
	if(scaling){
		ofRectangle scaledGUI = this->getGUIScaled();
		
		ofNoFill();
		ofSetColor(0, 255, 0);
		
		ofRectangle guiBox;
		
		if(centerDragEnabled){
			if(guiDragCenter) ofFill();
			guiBox = getGUIBoxCenter(scaledGUI);
			ofRect(guiBox.x, guiBox.y, guiBox.width, guiBox.height);
			if(guiDragCenter) ofNoFill();
		}
		
		if(guiDragTop) ofFill();
		guiBox = getGUIBoxTop(scaledGUI);
		ofRect(guiBox.x, guiBox.y, guiBox.width, guiBox.height);
		if(guiDragTop) ofNoFill();
		
		if(guiDragRight) ofFill();
		guiBox = getGUIBoxRight(scaledGUI);
		ofRect(guiBox.x, guiBox.y, guiBox.width, guiBox.height);
		if(guiDragRight) ofNoFill();
		
		if(guiDragBottom) ofFill();
		guiBox = getGUIBoxBottom(scaledGUI);
		ofRect(guiBox.x, guiBox.y, guiBox.width, guiBox.height);
		if(guiDragBottom) ofNoFill();
		
		if(guiDragLeft) ofFill();
		guiBox = getGUIBoxLeft(scaledGUI);
		ofRect(guiBox.x, guiBox.y, guiBox.width, guiBox.height);
		if(guiDragLeft) ofNoFill();
		
		ofSetColor(255, 255, 0);
		ofRect(scaledGUI.x, scaledGUI.y, scaledGUI.width, scaledGUI.height);
	}
	
	ofFill();
	ofSetColor(255, 255, 255);
}

void ofxScalableRectangle::drawContent(){
	this->drawContent(this->x, this->y);
}

void ofxScalableRectangle::drawContent(float x, float y){
	this->drawContent(x, y, this->width, this->height);
}

void ofxScalableRectangle::drawContent(float x, float y, float w, float h){
}

void ofxScalableRectangle::drawScaledContent(){
	this->drawScaledContent(this->x, this->y);
}

void ofxScalableRectangle::drawScaledContent(float x, float y){
	this->drawScaledContent(x, y, this->width, this->height);
}

void ofxScalableRectangle::drawScaledContent(float x, float y, float w, float h){
}

void ofxScalableRectangle::scaleContent(){
	bIsContentChanged = true;
}

bool ofxScalableRectangle::isContentChanged(){
	bool result = this->bIsContentChanged;
	this->bIsContentChanged = false;
	return result;
}

bool ofxScalableRectangle::isScaleChanged(){
	bool result = this->bIsScaleChanged;
	this->bIsScaleChanged = false;
	return result;
}

bool ofxScalableRectangle::isCenterDragEnabled(){
	return this->centerDragEnabled;
}

void ofxScalableRectangle::toggleCenterDrag(){
	enableCenterDrag(!centerDragEnabled);
}

void ofxScalableRectangle::enableCenterDrag(bool centerDragEnabled){
	this->centerDragEnabled = centerDragEnabled;
	if(!centerDragEnabled){
		if(guiDragCenter && !this->isScaleRealTime())
			this->scaleContent();		
		guiDragCenter = false;
	}
}

bool ofxScalableRectangle::isScaling(){
	return this->scaling;
}

void ofxScalableRectangle::toggleScaling(){
	this->setScaling(!this->scaling);
}

void ofxScalableRectangle::setScaling(bool scaling){
	this->scaling = scaling;
}

bool ofxScalableRectangle::isScaleRealTime(){
	return this->scaleRealTime;
}

void ofxScalableRectangle::toggleScaleRealTime(){
	this->setScaleRealTime(!this->scaleRealTime);
}

void ofxScalableRectangle::setScaleRealTime(bool scaleRealTime){
	this->scaleRealTime = scaleRealTime;
}

ofRectangle ofxScalableRectangle::getScaledBounds(){
	ofRectangle result;
	result.x = normToContentX(scaleBox.x);
	result.y = normToContentY(scaleBox.y);
	result.width = normToContentX(scaleBox.width);
	result.height = normToContentY(scaleBox.height);
	return result;
}

ofRectangle ofxScalableRectangle::getScaledNormals(){
	return this->scaleBox;
}

void ofxScalableRectangle::resetScale(){
	this->setScaledSize();
	this->scaleContent();
}

void ofxScalableRectangle::setContentSize(float w, float h){
	if(w == -1.0) w = this->width;
	if(h == -1.0) h = this->height;
	contentSize.x = w;
	contentSize.y = h;
}

float ofxScalableRectangle::contentToGuiX(float value){
	return normToGuiX(contentToNormX(value));
}

float ofxScalableRectangle::contentToGuiY(float value){
	return normToGuiY(contentToNormY(value));
}

float ofxScalableRectangle::contentToNormX(float value){
	if(this->contentSize.x > 0.0)
		return (value / this->contentSize.x);
	return value;
}

float ofxScalableRectangle::contentToNormY(float value){
	if(this->contentSize.y > 0.0)
		return (value / this->contentSize.y);
	return value;
}

float ofxScalableRectangle::guiToContentX(float value){
	return normToContentX(guiToNormX(value));
}

float ofxScalableRectangle::guiToContentY(float value){
	return normToContentY(guiToNormY(value));
}

float ofxScalableRectangle::guiToNormX(float value){
	return (value / this->width);
}

float ofxScalableRectangle::guiToNormY(float value){
	return (value / this->height);
}

float ofxScalableRectangle::normToContentX(float value){
	if(this->contentSize.x >= 0.0)
		return (value * this->contentSize.x);
	return value;
}

float ofxScalableRectangle::normToContentY(float value){
	if(this->contentSize.y >= 0.0)
		return (value * this->contentSize.y);
	return value;
}

float ofxScalableRectangle::normToGuiX(float value){
	return (value * this->width);
}

float ofxScalableRectangle::normToGuiY(float value){
	return (value * this->height);
}

void ofxScalableRectangle::pixelAdjustCenter(ofPoint pixelDelta){
	ofRectangle scaledGuiBounds = getGUIScaledLocal();
	
	scaledGuiBounds.x += pixelDelta.x;
	scaledGuiBounds.y += pixelDelta.y;
	
	if(scaledGuiBounds.x < 0.0)
		scaledGuiBounds.x = 0.0;
	else if((scaledGuiBounds.x + scaledGuiBounds.width) > this->width)
		scaledGuiBounds.x = this->width - scaledGuiBounds.width;
	
	if(scaledGuiBounds.y < 0.0)
		scaledGuiBounds.y = 0.0;
	else if((scaledGuiBounds.y + scaledGuiBounds.height) > this->height)
		scaledGuiBounds.y = this->height - scaledGuiBounds.height;
	
	setScaledBoundsX(guiToNormX(scaledGuiBounds.x));
	setScaledBoundsY(guiToNormY(scaledGuiBounds.y));
}

void ofxScalableRectangle::pixelAdjustTop(float pixelDelta){
	ofRectangle scaledGuiBounds = getGUIScaledLocal();
	float dy = pixelDelta;
	if((scaledGuiBounds.y + dy) < 0.0)
		dy -= (scaledGuiBounds.y+dy);
	else if(guiToContentY(scaledGuiBounds.height - dy) < 1.0)
		dy -= contentToGuiY(1.0 - guiToContentY(scaledGuiBounds.height - dy));
	scaledGuiBounds.y += dy;
	scaledGuiBounds.height -= dy;
	setScaledBoundsY(guiToNormY(scaledGuiBounds.y));
	setScaledBoundsH(guiToNormY(scaledGuiBounds.height));
}

void ofxScalableRectangle::pixelAdjustLeft(float pixelDelta){
	ofRectangle scaledGuiBounds = getGUIScaledLocal();
	float dx = pixelDelta;	
	if((scaledGuiBounds.x + dx) < 0.0)
		dx -= (scaledGuiBounds.x+dx);
	else if(guiToContentX(scaledGuiBounds.width - dx) < 1.0)
		dx -= contentToGuiX(1.0 - guiToContentX(scaledGuiBounds.width - dx));
	scaledGuiBounds.x += dx;
	scaledGuiBounds.width -= dx;
	setScaledBoundsX(guiToNormX(scaledGuiBounds.x));
	setScaledBoundsW(guiToNormX(scaledGuiBounds.width));
}

void ofxScalableRectangle::pixelAdjustBottom(float pixelDelta){
	ofRectangle scaledGuiBounds = getGUIScaledLocal();
	float dy = pixelDelta;
	if(guiToContentY(scaledGuiBounds.height + dy) < 1.0)
		dy += (1.0 - guiToContentY(scaledGuiBounds.height + dy));
	scaledGuiBounds.height += dy;
	setScaledBoundsH(guiToNormY(scaledGuiBounds.height));
}

void ofxScalableRectangle::pixelAdjustRight(float pixelDelta){
	ofRectangle scaledGuiBounds = getGUIScaledLocal();
	float dx = pixelDelta;
	if(guiToContentX(scaledGuiBounds.width + dx) < 1.0)
		dx += (1.0 - guiToContentX(scaledGuiBounds.width + dx));
	scaledGuiBounds.width += dx;
	setScaledBoundsW(guiToNormX(scaledGuiBounds.width));
}

void ofxScalableRectangle::setScaledSize(float x, float y, float w, float h){
	if(w <= 0.0 || normToContentX(w) <= 1.0) w = contentToNormX(1.0);
	else if((x + w) > 1.0) w = 1.0 - x;
	
	if(h <= 0.0 || normToContentY(h) <= 1.0) h = contentToNormY(1.0);
	else if ((y + h) > 1.0) h = 1.0 - y;
	
	if(x < 0.0) x = 0.0;
	if(y < 0.0) y = 0.0;
	
	scaleBox.x = x;
	scaleBox.y = y;
	scaleBox.width = w;
	scaleBox.height = h;
	
	bIsScaleChanged = true;
	
	if(this->isScaleRealTime())
		this->scaleContent();
}

float ofxScalableRectangle::setScaledBoundsX(float x){
	if(x < 0.0) x = 0.0;
	this->setScaledSize(x, this->scaleBox.y, this->scaleBox.width, this->scaleBox.height);
	return this->scaleBox.x;
}

float ofxScalableRectangle::setScaledBoundsY(float y){
	if(y < 0.0) y = 0.0;
	this->setScaledSize(this->scaleBox.x, y, this->scaleBox.width, this->scaleBox.height);
	return this->scaleBox.y;
}

float ofxScalableRectangle::setScaledBoundsW(float w){
	if(w <= 0.0) w = contentToNormX(1.0);
	else if((this->scaleBox.x + w) > 1.0) w = 1.0 - this->scaleBox.x;

	this->setScaledSize(this->scaleBox.x, this->scaleBox.y, w, this->scaleBox.height);
	return this->scaleBox.width;
}

float ofxScalableRectangle::setScaledBoundsH(float h){
	if(h <= 0.0) h = contentToNormY(1.0);
	else if((this->scaleBox.y + h) > 1.0) h = 1.0 - this->scaleBox.y;
	
	this->setScaledSize(this->scaleBox.x, this->scaleBox.y, this->scaleBox.width, h);
	return this->scaleBox.height;
}

ofRectangle ofxScalableRectangle::getGUIScaled(){
	ofRectangle result = getGUIScaledLocal();
	result.x += this->x;
	result.y += this->y;
	return result;
}

ofRectangle ofxScalableRectangle::getGUIScaledLocal(){
	ofRectangle result;
	result.x = normToGuiX(scaleBox.x);
	result.y = normToGuiY(scaleBox.y);
	result.width = normToGuiX(scaleBox.width);
	result.height = normToGuiY(scaleBox.height);
	return result;
}

ofRectangle ofxScalableRectangle::getGUIBoxCenter(ofRectangle scaledGUI){
	ofRectangle result;
	result.x = scaledGUI.x + (scaledGUI.width - guiBoundsWidth) / 2.0;
	result.y = scaledGUI.y + (scaledGUI.height - guiBoundsWidth) / 2.0;
	result.width = guiBoundsWidth;
	result.height = guiBoundsWidth;
	return result;
}

ofRectangle ofxScalableRectangle::getGUIBoxTop(ofRectangle scaledGUI){
	ofRectangle result;
	result.x = scaledGUI.x - guiBoundsWidth;
	result.y = scaledGUI.y - guiBoundsWidth;
	result.width = scaledGUI.width + guiBoundsWidth*2.0;
	result.height = guiBoundsWidth;
	return result;
}

ofRectangle ofxScalableRectangle::getGUIBoxBottom(ofRectangle scaledGUI){
	ofRectangle result;
	result.x = scaledGUI.x - guiBoundsWidth;
	result.y = scaledGUI.y + scaledGUI.height;
	result.width = scaledGUI.width + guiBoundsWidth*2.0;
	result.height = guiBoundsWidth;	
	return result;
}

ofRectangle ofxScalableRectangle::getGUIBoxLeft(ofRectangle scaledGUI){
	ofRectangle result;
	result.x = scaledGUI.x - guiBoundsWidth;
	result.y = scaledGUI.y - guiBoundsWidth;
	result.width = guiBoundsWidth;
	result.height = scaledGUI.height + guiBoundsWidth*2.0;
	return result;
}

ofRectangle ofxScalableRectangle::getGUIBoxRight(ofRectangle scaledGUI){
	ofRectangle result;
	result.x = scaledGUI.x + scaledGUI.width;
	result.y = scaledGUI.y - guiBoundsWidth;
	result.width = guiBoundsWidth;
	result.height = scaledGUI.height + guiBoundsWidth*2.0;
	return result;
}

void ofxScalableRectangle::mouseMoved(int x, int y ){
	if(!this->isScaling())
		return;
}

void ofxScalableRectangle::mouseDragged(int x, int y, int button){
	if(!this->isScaling())
		return;
	
	ofPoint mouseDrag;
	mouseDrag.x = x - this->guiClick.x;
	mouseDrag.y = y - this->guiClick.y;
	
	this->guiClick.x = x;
	this->guiClick.y = y;
	
	if(this->centerDragEnabled){
		if(this->guiDragCenter)
			pixelAdjustCenter(mouseDrag);
	}
	
	if(this->guiDragTop)
		pixelAdjustTop(mouseDrag.y);
	
	if(this->guiDragRight)
		pixelAdjustRight(mouseDrag.x);
	
	if(this->guiDragBottom)
		pixelAdjustBottom(mouseDrag.y);
	
	if(this->guiDragLeft)
		pixelAdjustLeft(mouseDrag.x);
}

void ofxScalableRectangle::mousePressed(int x, int y, int button){
	if(!this->isScaling())
		return;
	
	ofRectangle scaledGUI = this->getGUIScaled();
	ofRectangle guiCheck;
	
	if(centerDragEnabled){
		guiCheck = this->getGUIBoxCenter(scaledGUI);
		if(x >= guiCheck.x && x <= (guiCheck.x+guiCheck.width) && y >= guiCheck.y && y <= (guiCheck.y+guiCheck.height))
			this->guiDragCenter = true;
	}
	
	if(!centerDragEnabled || !this->guiDragCenter){
		guiCheck = this->getGUIBoxTop(scaledGUI);
		if(x >= guiCheck.x && x <= (guiCheck.x+guiCheck.width) && y >= guiCheck.y && y <= (guiCheck.y+guiCheck.height))
			this->guiDragTop = true;
		
		guiCheck = this->getGUIBoxRight(scaledGUI);
		if(x >= guiCheck.x && x <= (guiCheck.x+guiCheck.width) && y >= guiCheck.y && y <= (guiCheck.y+guiCheck.height))
			this->guiDragRight = true;
		
		guiCheck = this->getGUIBoxBottom(scaledGUI);
		if(x >= guiCheck.x && x <= (guiCheck.x+guiCheck.width) && y >= guiCheck.y && y <= (guiCheck.y+guiCheck.height))
			this->guiDragBottom = true;
		
		guiCheck = this->getGUIBoxLeft(scaledGUI);
		if(x >= guiCheck.x && x <= (guiCheck.x+guiCheck.width) && y >= guiCheck.y && y <= (guiCheck.y+guiCheck.height))
			this->guiDragLeft = true;
	}
	
	this->guiClick.x = x;
	this->guiClick.y = y;
}

void ofxScalableRectangle::mouseReleased(int x, int y, int button){
	if(!this->isScaling())
		return;
	
	bool wasDragged = (guiDragTop || guiDragRight || guiDragBottom || guiDragLeft || guiDragCenter);
	guiDragTop = guiDragRight = guiDragBottom = guiDragLeft = guiDragCenter = false;
	if(wasDragged && !this->isScaleRealTime())
		this->scaleContent();
}
