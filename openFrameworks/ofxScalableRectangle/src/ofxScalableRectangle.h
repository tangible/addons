/*
 *  ofxScalableRectangle.h
 *  emptyExample
 *
 *  Created by Pat Long (plong0) on 11-03-22.
 *  Copyright 2011 Tangible Interaction. All rights reserved.
 *
 */
#pragma once

#include "ofMain.h"

class ofxScalableRectangle : public ofRectangle {
protected:
	float guiBoundsWidth;
	bool guiDragTop, guiDragRight, guiDragBottom, guiDragLeft, guiDragCenter;
	ofPoint guiClick;
	
	bool centerDragEnabled;
	
	ofPoint contentSize;
	ofRectangle scaleBox;
	bool scaling, scaleRealTime;
	
	bool bIsContentChanged, bIsScaleChanged;
	
	ofRectangle getGUIScaled();
	ofRectangle getGUIScaledLocal();
	
	ofRectangle getGUIBoxCenter(ofRectangle scaledGUI);
	ofRectangle getGUIBoxTop(ofRectangle scaledGUI);
	ofRectangle getGUIBoxBottom(ofRectangle scaledGUI);
	ofRectangle getGUIBoxLeft(ofRectangle scaledGUI);
	ofRectangle getGUIBoxRight(ofRectangle scaledGUI);
	
	float contentToGuiX(float value);
	float contentToGuiY(float value);
	float contentToNormX(float value);
	float contentToNormY(float value);
	
	float guiToContentX(float value);
	float guiToContentY(float value);
	float guiToNormX(float value);
	float guiToNormY(float value);
	
	float normToContentX(float value);
	float normToContentY(float value);
	float normToGuiX(float value);
	float normToGuiY(float value);
	
	void pixelAdjustCenter(ofPoint pixelDelta);
	void pixelAdjustTop(float pixelDelta);
	void pixelAdjustLeft(float pixelDelta);
	void pixelAdjustBottom(float pixelDelta);
	void pixelAdjustRight(float pixelDelta);
	
	float setScaledBoundsX(float x);
	float setScaledBoundsY(float y);
	float setScaledBoundsW(float w);
	float setScaledBoundsH(float h);
	
	virtual void init();
	
public:
	ofxScalableRectangle();
	ofxScalableRectangle(float x, float y, float w, float h);
	~ofxScalableRectangle();
	
	virtual void draw();
	virtual void draw(float x, float y);
	virtual void draw(float x, float y, float w, float h);
	
	virtual void drawContent();
	virtual void drawContent(float x, float y);
	virtual void drawContent(float x, float y, float w, float h);
	
	virtual void drawScaledContent();
	virtual void drawScaledContent(float x, float y);
	virtual void drawScaledContent(float x, float y, float w, float h);
	
	ofRectangle getScaledBounds();
	ofRectangle getScaledNormals();

	void resetScale();
	void setContentSize(float w=-1.0, float h=-1.0);
	void setScaledSize(float x=0.0, float y=0.0, float w=1.0, float h=1.0);
	
	virtual void scaleContent();
	
	bool isContentChanged();
	bool isScaleChanged();
	
	bool isCenterDragEnabled();
	void toggleCenterDrag();
	void enableCenterDrag(bool centerDragEnabled=true);
	
	bool isScaling();
	void toggleScaling();
	void setScaling(bool scaling=true);
	
	bool isScaleRealTime();
	void toggleScaleRealTime();
	void setScaleRealTime(bool scaleRealTime=true);
	
	void mouseMoved(int x, int y );
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
};
