/*
 *  ofxBox2dPVContactListener.h
 *  ofxKinect
 *
 *  Created by Pat Long (plong0) on 11-02-14.
 *  Copyright 2011 Tangible Interaction. All rights reserved.
 *
 */
#pragma once

#include "ofxBox2d.h"
#include "ofxOpenCv.h"
#include "ofxCvOpticalFlowLK.h"

enum b2dPVType {SLAVE, MASTER, STATIC};

class ofxBox2dPVUserData {
public:
	b2dPVType myType;
	ofPoint frameTotalForce;
	ofPoint avgForcePoint;
	
	ofxBox2dPVUserData(b2dPVType myType){
		this->myType = myType;
		this->frameTotalForce.x = this->frameTotalForce.y = 0.0;
		this->avgForcePoint.x = this->avgForcePoint.y = 0.0;
	}
};


class ofxBox2dPVContactListener : public ofxBox2dContactListener{
public:	
	ofxBox2d*				box2d;
	ofxCvOpticalFlowLK*		opticalFlow;
	ofxCvGrayscaleImage* 	depthDiff;
	ofxCvGrayscaleImage* 	depthDiffPrev;
	int frameCount;
	
	int optFlowSize;
	float optFlowForceMultiplier;
	float optFlowForceMax;
	
	struct collidePoint{
		b2Body* ptShapeBody;
		b2Body* collidedBody;
		int x;
		int y;
	};
	vector<collidePoint> procPoints;
	
	ofxBox2dPVContactListener() {
		this->box2d = NULL;
		this->frameCount = 0;
		this->opticalFlow = new ofxCvOpticalFlowLK();
		this->depthDiff = NULL;
		this->depthDiffPrev = NULL;
		this->optFlowSize = 24;
		this->optFlowForceMultiplier = 2.0;
		this->optFlowForceMax = 10.0;
	}
	
	~ofxBox2dPVContactListener(){
		if(opticalFlow != NULL){
			delete opticalFlow;
			opticalFlow = NULL;
		}
	}
	
	void setBox2d(ofxBox2d* box2d, bool selfRegister=true){
		if(box2d != NULL){
			this->box2d = box2d;
			if(selfRegister)
				box2d->setContactListener(this);
		}
	}
	
	void setDepthDiff(ofxCvGrayscaleImage* depthDiff){
		this->depthDiff = depthDiff;
	}
	
	void setDepthDiffPrev(ofxCvGrayscaleImage* depthDiffPrev){
		this->depthDiffPrev = depthDiffPrev;
	}
	
	void update(){
		this->frameCount++;
		
		if(frameCount > 5){
			for(int i=0; i < procPoints.size(); i++){
				this->handlePointShapeContact(procPoints[i].ptShapeBody, procPoints[i].collidedBody, procPoints[i].x, procPoints[i].y);
			}
		}
		procPoints.clear();
	}
	
	
	/**
	 struct b2ContactPoint
	 {
	 b2Shape* shape1;		///< the first shape
	 b2Shape* shape2;		///< the second shape
	 b2Vec2 position;		///< position in world coordinates
	 b2Vec2 velocity;		///< velocity of point on body2 relative to point on body1 (pre-solver)
	 b2Vec2 normal;			///< points from shape1 to shape2
	 float32 separation;		///< the separation is negative when shapes are touching
	 float32 friction;		///< the combined friction coefficient
	 float32 restitution;	///< the combined restitution coefficient
	 b2ContactID id;			///< the contact id identifies the features in contact
	 };
	 */
	virtual void Add(const b2ContactPoint* point) {
		// point of collision
		b2Vec2 p = point->position;
		p *= OFX_BOX2D_SCALE;
		
		//
		b2Shape* shape1 = point->shape1;		///< the first shape
		b2Shape* shape2 = point->shape2;		///< the second shape
		
		b2Body* body1 = (shape1 != NULL)?shape1->GetBody():NULL;
		b2Body* body2 = (shape2 != NULL)?shape2->GetBody():NULL;
		
		if(body1 != NULL && body2 != NULL){
			ofxBox2dPVUserData* ud1 = (ofxBox2dPVUserData*)body1->GetUserData();
			ofxBox2dPVUserData* ud2 = (ofxBox2dPVUserData*)body2->GetUserData();
			
			bool ptShape1 = (ud1 != NULL)?(ud1->myType == MASTER):false;
			bool ptShape2 = (ud2 != NULL)?(ud2->myType == MASTER):false;
			
			bool staticShape1 = (ud1 != NULL)?(ud1->myType == STATIC):true;
			bool staticShape2 = (ud2 != NULL)?(ud2->myType == STATIC):true;
			
			if(ptShape1 && !ptShape2 && !staticShape1 && !staticShape2){
				collidePoint pt;
				pt.ptShapeBody = body1;
				pt.collidedBody = body2;
				pt.x = point->position.x;
				pt.y = point->position.y;
				procPoints.push_back(pt);
			}
			else if(ptShape2 && !ptShape1 && !staticShape1 && !staticShape2){
				collidePoint pt;
				pt.ptShapeBody = body2;
				pt.collidedBody = body1;
				pt.x = point->position.x;
				pt.y = point->position.y;
				procPoints.push_back(pt);
			}
		}
		
		contactAdd(ofPoint(p.x, p.y));
	}
	
	virtual void Remove(const b2ContactPoint* point) {
	}
	
	virtual void draw(){		
		ofSetColor(255, 255, 0, 64);
		depthDiff->draw(0, 0);
		ofSetColor(0, 255, 0, 64);
		depthDiffPrev->draw(0, 0);
		
		ofSetColor(0, 255, 255);
		opticalFlow->draw();
		
		ofSetColor(255, 255, 255);
	}
	
	virtual void handlePointShapeContact(b2Body* ptShapeBody, b2Body* collidedBody, int ptX, int ptY){ //const b2ContactPoint* point){
		if(frameCount > 5 && opticalFlow != NULL && depthDiff != NULL && depthDiffPrev != NULL){	
			float scale = 1.0;
			if(box2d != NULL)
				scale = box2d->scale;
			
			int w = this->optFlowSize;
			int h = this->optFlowSize;
			int x = ptX*scale - (w/2); //point->position.x*scale - (w/2);
			int y = ptY*scale - (h/2); //point->position.y*scale - (h/2);
			
			if(x < 0) x = 0;
			else if(x+w > depthDiff->getWidth()) x = depthDiff->getWidth()-w;
			
			if(y < 0) y = 0;
			else if(y+h > depthDiff->getHeight()) y = depthDiff->getHeight()-h;
			
			depthDiffPrev->setROI(x, y, w, h);
			depthDiff->setROI(x, y, w, h);
			
			opticalFlow->allocate(w, h);
			opticalFlow->calc(*depthDiffPrev, *depthDiff, 11);
			
			cvSmooth(opticalFlow->vel_x, opticalFlow->vel_x, CV_BLUR , 15);
			cvSmooth(opticalFlow->vel_y, opticalFlow->vel_y, CV_BLUR , 15);
			
			float speed;
			float avgSpeed = 0.0;
			float avgDx = 0.0;
			float avgDy = 0.0;
			int count = 0;
			int dx, dy;
			for ( y = 0; y < opticalFlow->captureHeight; y += opticalFlow->captureRowsStep ){
				for ( x = 0; x < opticalFlow->captureWidth; x += opticalFlow->captureColsStep ){
					dx = (int)cvGetReal2D( opticalFlow->vel_x, y, x );
					dy = (int)cvGetReal2D( opticalFlow->vel_y, y, x );
					speed = sqrt(dx * dx + dy * dy);
					avgSpeed += speed;
					avgDx += dx;
					avgDy += dy;
					count++;
				}
			}
			if(count > 0){
				avgSpeed /= (float)count;
				avgDx /= (float)count;
				avgDy /= (float)count;
			}
			
//			cout << "avgSpeed:" << count << ":" << avgSpeed << endl;
			
			float multi = this->optFlowForceMultiplier;
			float maxVec = this->optFlowForceMax;
			if(avgDx != 0.0 || avgDy != 0.0){
				b2Vec2 imp;
				imp.x = avgDx*multi;
				imp.y = avgDy*multi;
				
				if(imp.x > maxVec) imp.x = maxVec;
				else if(imp.x < -maxVec) imp.x = -maxVec;
				if(imp.y > maxVec) imp.y = maxVec;
				else if(imp.y < -maxVec) imp.y = -maxVec;
				
//				cout << "impulse:" << imp.x << "," << imp.y << endl;
				b2Vec2 pt;
				pt.x = ptX;
				pt.y = ptY;
//				collidedBody->ApplyImpulse(imp, pt);
				collidedBody->ApplyForce(imp, pt);
				
				ofxBox2dPVUserData* ud = (ofxBox2dPVUserData*)collidedBody->GetUserData();
				if(ud != NULL){
					ud->frameTotalForce.x += imp.x;
					ud->frameTotalForce.y += imp.y;
					
					ud->avgForcePoint.x = (ud->avgForcePoint.x + ptX) / 2.0;
					ud->avgForcePoint.y = (ud->avgForcePoint.y + ptY) / 2.0;
				}
			}
			
			depthDiffPrev->resetROI();
			depthDiff->resetROI();
			opticalFlow->reset();
		}
	}
	
	virtual void contactAdd(ofPoint p) {
	}
	virtual void contactRemove(ofPoint p) {
	}
};
