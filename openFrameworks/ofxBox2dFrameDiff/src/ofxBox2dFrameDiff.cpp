/*
 *  ofxBox2dFrameDiff.cpp
 *  VoiceIt
 *
 *  Created by Pat Long (plong0) on 11-02-16.
 *  Copyright 2011 Tangible Interaction. All rights reserved.
 *
 */

#include "ofxBox2dFrameDiff.h"

ofxBox2dFrameDiff::ofxBox2dFrameDiff(ofxBox2d* box2d){
	this->init(box2d, NULL);
}

ofxBox2dFrameDiff::~ofxBox2dFrameDiff(){
	clearComplexPolys();
	
	if(contactListener != NULL){
		delete contactListener;
		contactListener = NULL;
	}
}

void ofxBox2dFrameDiff::init(ofxBox2d* box2d, ofxCvGrayscaleImage*	grayImage){
	this->box2d = box2d;
	this->grayImage = grayImage;
	
	cpMass = 1.0;
	cpRestitution = 1.0;
	cpFriction = 0.75;
	
	contactListener = new ofxBox2dPVContactListener();
	contactListener->setBox2d(box2d);
	contactListener->setDepthDiff(&depthDiff);
	contactListener->setDepthDiffPrev(&depthDiffPrev);
	
	if(grayImage != NULL)
		this->setOutputBounds(0.0, 0.0, grayImage->getWidth(), grayImage->getHeight());
	else
		this->setOutputBounds(0.0, 0.0, ofGetWidth(), ofGetHeight());
	
	this->setTriRes(50);
}

//--------------------------------------------------------------
void ofxBox2dFrameDiff::update(){
	clearComplexPolys();
	updateComplexPolys(grayImage);
	
	if(grayImage != NULL){
		ofxCvGrayscaleImage grayScale;
		grayScale.allocate(grayImage->getWidth(), grayImage->getHeight());
		grayScale = *grayImage;
		grayScale.resize(outputBounds.width, outputBounds.height);
		
		depthDiff = grayScale;
		if(contactListener != NULL)
			contactListener->update();
		depthDiffPrev = depthDiff;
	}
}

//--------------------------------------------------------------
void ofxBox2dFrameDiff::draw(){
	ofSetColor(0, 255, 0, 128);
	for(int i = 0; i < complexPolys.size(); i++){
		complexPolys[i]->draw();
	}
}

//--------------------------------------------------------------
void ofxBox2dFrameDiff::drawContours(float x, float y, float w, float h){
	contourFinder.draw(x, y, w, h);
}

//--------------------------------------------------------------
void ofxBox2dFrameDiff::drawDiff(float x, float y, float w, float h){
	if(grayImage != NULL)
		grayImage->draw(x, y, w, h);
}

//--------------------------------------------------------------
void ofxBox2dFrameDiff::updateComplexPolyPhysics(){
	for(int i=0; i < complexPolys.size(); i++){
		b2Body*	cBody = complexPolys[i]->body;
		if(cBody != NULL){
			b2Shape* cShape = cBody->GetShapeList();
			while(cShape != NULL){
				cShape->SetDensity(cpMass);
				cShape->SetRestitution(cpRestitution);
				cShape->SetFriction(cpFriction);
				cShape = cShape->GetNext();
			}
			cBody->SetMassFromShapes();
		}
	}
}

//--------------------------------------------------------------
void ofxBox2dFrameDiff::updateComplexPolys(ofxCvGrayscaleImage* diffImage){	
	if(box2d != NULL && diffImage != NULL){
		ofPoint scale;		
		scale.x = (float)outputBounds.width / (float)diffImage->getWidth();
		scale.y = (float)outputBounds.height / (float)diffImage->getHeight();
		
		ofPoint offset;
		offset.x = outputBounds.x;
		offset.y = outputBounds.y;
		
		contourFinder.findContours(*diffImage, 100, diffImage->getWidth()*diffImage->getHeight(), 10, true);
		for(int i = 0; i < contourFinder.nBlobs; i++){
			if(contourFinder.blobs[i].boundingRect.width >= 25.0 && contourFinder.blobs[i].boundingRect.height >= 25.0){
				ofxBox2dComplexPoly* newShape = NULL;
				ofxTriangle tri;
				tri.triangulate(contourFinder.blobs[i], triRes);
				
				if(tri.triangles.size() > 0){
					newShape = new ofxBox2dComplexPoly();
					newShape->setPhysics(cpMass, cpRestitution, cpFriction);
					newShape->setup(box2d->getWorld(), offset.x, offset.y, tri.triangles, scale); //contourFinder.blobs[i].centroid.x, contourFinder.blobs[i].centroid.y, tri.triangles);
					
					if(newShape->body != NULL){
						newShape->body->SetUserData(new ofxBox2dPVUserData(MASTER));
					}
					
					complexPolys.push_back(newShape);
				}
			}
		}
	}
}

//--------------------------------------------------------------
void ofxBox2dFrameDiff::clearComplexPolys(){
	for(int i=0; i < complexPolys.size(); i++){
		if(complexPolys[i]->body != NULL){
			ofxBox2dPVUserData* ud = (ofxBox2dPVUserData*)complexPolys[i]->body->GetUserData();
			if(ud != NULL){
				delete ud;
				ud = NULL;
				complexPolys[i]->body->SetUserData(NULL);
			}
		}
		
		complexPolys[i]->destroyShape();
		delete complexPolys[i];
		complexPolys[i] = NULL;
	}
	complexPolys.clear();
}

//--------------------------------------------------------------
void ofxBox2dFrameDiff::setComplexPolyPhysics(float mass, float restitution, float friction){
	this->cpMass = mass;
	this->cpRestitution = restitution;
	this->cpFriction = friction;
	updateComplexPolyPhysics();
}

//--------------------------------------------------------------
void ofxBox2dFrameDiff::setCLOptFlowSize(int optFlowSize){
	if(this->contactListener != NULL){
		this->contactListener->optFlowSize = optFlowSize;
	}
}

//--------------------------------------------------------------
void ofxBox2dFrameDiff::setCLOptFlowForceMax(float optFlowForceMax){
	if(this->contactListener != NULL)
		this->contactListener->optFlowForceMax = optFlowForceMax;
	
}

//--------------------------------------------------------------
void ofxBox2dFrameDiff::setCLOptFlowForceMultiplier(float optFlowForceMultiplier){
	if(this->contactListener != NULL)
		this->contactListener->optFlowForceMultiplier = optFlowForceMultiplier;
}

//--------------------------------------------------------------
void ofxBox2dFrameDiff::setOutputBounds(float outputX, float outputY, float outputWidth, float outputHeight){
	this->outputBounds.x = outputX;
	this->outputBounds.y = outputY;
	this->outputBounds.width = outputWidth;
	this->outputBounds.height = outputHeight;
	
	depthDiff.clear();
	depthDiffPrev.clear();
	
	if(!depthDiff.bAllocated)
		depthDiff.allocate(outputWidth, outputHeight);
	else
		depthDiff.resize(outputWidth, outputHeight);
	
	if(!depthDiffPrev.bAllocated)
		depthDiffPrev.allocate(outputWidth, outputHeight);
	else
		depthDiffPrev.resize(outputWidth, outputHeight);
}

//--------------------------------------------------------------
void ofxBox2dFrameDiff::setTriRes(int triRes){
	this->triRes = triRes;
}
