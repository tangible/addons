/*
 *  ofxBox2dFrameDiff.h
 *  VoiceIt
 *
 *  Created by Pat Long (plong0) on 11-02-16.
 *  Copyright 2011 Tangible Interaction. All rights reserved.
 *
 */
#pragma once

#include "ofxBox2d.h"
#include "ofxBox2dComplexPoly.h"
#include "ofxBox2dPVContactListener.h"
#include "ofxOpenCv.h"

class ofxBox2dFrameDiff {
	protected:
		ofxBox2d* box2d;
		ofxBox2dPVContactListener*		contactListener;
		vector<ofxBox2dComplexPoly*>	complexPolys;

		ofxCvGrayscaleImage*	grayImage;
		ofxCvGrayscaleImage 	depthDiff;
		ofxCvGrayscaleImage 	depthDiffPrev;
		
		ofxCvContourFinder		contourFinder;
		int						triRes;
	
		float cpMass, cpRestitution, cpFriction;
		
		ofRectangle outputBounds;
	
		void updateComplexPolyPhysics();
		void updateComplexPolys(ofxCvGrayscaleImage* diffImage);
		void clearComplexPolys();
		
	public:
		ofxBox2dFrameDiff(ofxBox2d* box2d=NULL);
		~ofxBox2dFrameDiff();
	
		virtual void init(ofxBox2d* box2d, ofxCvGrayscaleImage*	grayImage);
		virtual void update();
		virtual void draw();
		void drawContours(float x, float y, float w, float h);
		void drawDiff(float x, float y, float w, float h);
	
		void setComplexPolyPhysics(float mass, float restitution, float friction);
	
		void setCLOptFlowSize(int optFlowSize);
		void setCLOptFlowForceMax(float optFlowForceMax);
		void setCLOptFlowForceMultiplier(float optFlowForceMultiplier);
	
		void setOutputBounds(float outputX, float outputY, float outputWidth, float outputHeight);
		void setTriRes(int triRes);
};
