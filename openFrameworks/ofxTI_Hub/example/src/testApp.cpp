#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){
	ofSetFrameRate(60);
	ofSetLogLevel(OF_LOG_NOTICE);
	vidGrab.initGrabber(640, 480);
	
	if(hub.loadConfiguration("hub_settings.xml")){
		cout << "Loaded hub settings from hub_settings.xml!" << endl;
	//	hub.setPostURL("http://www.postbin.org/1e6kxq0");
	}
	else{
		hub.setSavePath("test");
		hub.setInstallationID(80110524);
		hub.setApiKey("8e4cd611523fe7c7020a488b3ab2232b9bae4f85a9e6c4d402e31c936ef41fbe");
	}
}

//--------------------------------------------------------------
void testApp::update(){
	vidGrab.update();
	if(vidGrab.isFrameNew()){
		cImage.setFromPixels(vidGrab.getPixels(), vidGrab.getWidth(), vidGrab.getHeight(), OF_IMAGE_COLOR);
		cImage.mirror(true, false);
	}
	
	hub.update();
}

//--------------------------------------------------------------
void testApp::draw(){
	ofBackground(0, 0, 0);
	
	ofSetColor(255, 255, 255);
	
	float ratio = cImage.getWidth()/cImage.getHeight();
	float dH = ofGetHeight()-20.0;
	float dW = dH*ratio;
	ofRectangle image(10, 10, dW, dH);
	cImage.draw(image.x, image.y, image.width, image.height);
	ofNoFill();
	ofRect(image.x, image.y, image.width, image.height);
	ofFill();	
	
	string status = "";
	if(hub.getStatus() == STATUS_IDLE){
		ofSetColor(255, 192, 0);
		status = "Idle";
	}
	else if(hub.getStatus() == STATUS_SENDING){
		ofSetColor(0, 255-(ofGetFrameNum()/42 %128), 0);
		status = "Sending";
	}
	else if(hub.getStatus() == STATUS_SUCCESS){
		ofSetColor(0, 255, 0);
		status = "Success";
	}
	else if(hub.getStatus() == STATUS_ERROR){
		ofSetColor(255, 0, 0);
		status = "Error";
	}
	
	float availWidth = ofGetWidth()-image.width+image.x*2.0;
	float circleR = ofGetHeight()/3.0;
	if(circleR > availWidth) circleR = availWidth;
	float circleX = image.width+image.x*3.0 + (availWidth-circleR)/2.0;
	
	ofCircle(circleX, ofGetHeight()/2.0, circleR);
	
	if(status != ""){
		ofSetColor(0, 0, 0);
		ofDrawBitmapString(status, circleX-(status.length()*4.1), ofGetHeight()/2.0+4.0);
	}
	
	ofSetColor(255, 255, 255);
	string label = "Cache: ";
	ofDrawBitmapString(label, circleX-circleR, ofGetHeight()-12.0);
	
	if(hub.getCacheStatus() == STATUS_IDLE){
		ofSetColor(255, 192, 0);
		status = "Idle";
	}
	else if(hub.getCacheStatus() == STATUS_SENDING){
		ofSetColor(0, 255-(ofGetFrameNum()/42 %128), 0);
		status = "Sending";
	}
	else if(hub.getCacheStatus() == STATUS_SUCCESS){
		ofSetColor(0, 255, 0);
		status = "Success";
	}
	else if(hub.getCacheStatus() == STATUS_ERROR){
		ofSetColor(255, 0, 0);
		status = "Error";
	}
	
	ofRect(circleX-circleR+label.length()*8.2, ofGetHeight()-22.0, 12.0, 12.0); //circleR*2.0-label.length()*8.2, 12.0);

}

void testApp::sendEntry(){	
	ofxTI_HubEntry* newEntry = hub.getNewEntry();
	newEntry->addTextNode("testing 123");
	newEntry->addTextNode(hub.getTimeStamp(TS_HUMAN), "time");
	newEntry->addImageNode(cImage, "video_capture");
	cImage.resize(cImage.getWidth()*0.25, cImage.getHeight()*0.25);
	newEntry->addImageNode(cImage, "video_capture_thumb");
	hub.postEntry(newEntry);
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){

}

//--------------------------------------------------------------
void testApp::keyReleased(int key){
	if(key == ' ')
		this->sendEntry();
}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

