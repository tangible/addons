/*
 *  ofxTI_Hub.h
 *  openFrameworks
 *
 *  Created by Pat Long (plong0) on 11-05-24.
 *  Copyright 2011 Tangible Interaction Inc. All rights reserved.
 *
 */
#ifndef _OFX_TI_HUB
#define _OFX_TI_HUB

#include "ofMain.h"
#include "ofxDateTime.h"
#include "ofxDirList.h"
#include "ofxHttpUtils.h"
#include "ofxTI_Utils.h"

#include "ofxTI_HubEntry.h"

extern int currentLogLevel;

#define OFX_TI_HUB_POST_URL	"http://hub.tangibleinteraction.com/api/entries.xml" // "http://localhost/upload.php"
#define OFX_TI_HUB_DEFAULT_IMAGE_EXTENSION	"jpg"

#define OFX_TI_HUB_CACHE_SCAN_DELAY_MIN		250 // if we scan too often, it breaks stuff

enum TimeStampMode {TS_HUMAN, TS_EPOCH};
enum ofxTI_HubStatus {STATUS_IDLE, STATUS_SUCCESS, STATUS_ERROR, STATUS_SENDING};

class ofxTI_Hub{
protected:
	ofxHttpUtils httpUtils;
	
	ofxTI_HubStatus status;
	ofxTI_HubStatus cacheStatus;
	
	string apiKey;
	int installationID;
	
	string postURL;
	vector<ofxTI_HubEntry*> postQueue;
	ofxTI_HubEntry* activeEntry;
	
	string savePath;
	string cachePath;
	string defaultImageExtension;
	TimeStampMode tsMode;
	
	int cacheScanDelay;
	int lastCacheScan;
	
	bool checkCacheDirectory();
	void cacheEntry(ofxTI_HubEntry* entry);
	void processCache();

	void killActiveEntry();
	
public:
	ofxTI_Hub();
	~ofxTI_Hub();
	
	virtual void update();
	
	string getApiKey();
	int getInstallationID();
	virtual string getNewEntryName();
	virtual string getNewImageName(string extension="");
	string getSavePath();
	ofxTI_HubStatus getStatus();
	ofxTI_HubStatus getCacheStatus();
	virtual string getTimeStamp(TimeStampMode tsMode);
	
	ofxTI_HubEntry* getNewEntry(string entryName="");
	
	bool isSending();
	
	bool inPostQueue(ofxTI_HubEntry* entry);
	bool isPosting(ofxTI_HubEntry* entry);
	void postEntry(ofxTI_HubEntry* entry);
	bool doPostEntry(ofxTI_HubEntry* entry);
	virtual void newResponse(ofxHttpResponse & response);
	virtual void newError(string & error);
	
	virtual bool loadConfiguration(string fromFile="");
	
	void setActiveEntry(ofxTI_HubEntry* activeEntry=NULL);
	void setApiKey(string apiKey="");
	void setInstallationID(int installationID=0);
	void setPostURL(string postURL=OFX_TI_HUB_POST_URL);
	void setSavePath(string savePath="");
	void setCacheScanDelay(int cacheScanDelay=1000);
	void setDefaultImageExtension(string defaultImageExtension=OFX_TI_HUB_DEFAULT_IMAGE_EXTENSION);
	void setStatus(ofxTI_HubStatus status=STATUS_IDLE);
	void setCacheStatus(ofxTI_HubStatus cacheStatus=STATUS_IDLE);
	void setTimeStampMode(TimeStampMode tsMode=TS_HUMAN);
	
};

#endif
