/*
 *  ofxTI_HubEntry.cpp
 *  openFrameworks
 *
 *  Created by Pat Long (plong0) on 11-05-24.
 *  Copyright 2011 Tangible Interaction Inc. All rights reserved.
 *
 */

#include "ofxTI_HubEntry.h"

ofxTI_HubEntry::ofxTI_HubEntry(ofxTI_Hub* hub, string entryName){
	this->setHub(hub);
	this->xmlFileName = "";
	this->saved = false;
	this->cached = false;
	if(entryName != "")
		this->loadXml(entryName);
}

ofxTI_HubEntry::~ofxTI_HubEntry(){
}

ofxTI_Hub* ofxTI_HubEntry::getHub(){
	return this->hub;
}

string ofxTI_HubEntry::getXmlFileName(){
	return this->xmlFileName;
}

vector<ofxTI_HubEntryNode> ofxTI_HubEntry::getNodes(){
	return this->nodes;
}

bool ofxTI_HubEntry::isCached(){
	return this->cached;
}

bool ofxTI_HubEntry::isSaved(){
	return this->saved;
}

void ofxTI_HubEntry::addTextNode(string value, string name){
	ofxTI_HubEntryNode newNode;
	newNode.name = name;
	newNode.value = value;
	newNode.isImage = false;
	this->nodes.push_back(newNode);
	this->saved = false;
}

void ofxTI_HubEntry::addImageNode(string fileName, string name){
	ofxTI_HubEntryNode newNode;
	newNode.name = name;
	newNode.value = fileName;
	newNode.isImage = true;
	this->nodes.push_back(newNode);
	this->saved = false;
}

void ofxTI_HubEntry::addImageNode(ofxImage image, string name){
	if(image.getFileName() == "" && this->hub != NULL)
		image.saveImage(this->hub->getNewImageName());
	
	if(image.getFileName() != "")
		this->addImageNode(image.getFileName(), name);
}

void ofxTI_HubEntry::addService(string service){
	this->services.push_back(service);
}

bool ofxTI_HubEntry::loadXml(string xmlFileName, bool cached){
	bool result = false;
	ofxXmlSettings xml;
	
	if(xmlFileName == "" && this->xmlFileName != "")
		xmlFileName = this->xmlFileName;
	
	if(xmlFileName != "" && xml.loadFile(xmlFileName)){
		if(xml.tagExists("entry")){
			xml.pushTag("entry");
			
			this->nodes.clear();
			
			int cIndex = 0;
			string cName = "";
			string cValue = "";
			while(xml.tagExists("text", cIndex)){
				cValue = xml.getValue("text", cIndex);
				cName = xml.getAttribute("text", "name", "", cIndex);
				this->addTextNode(cValue, cName);
				cIndex++;
			}
			
			int slashPos = xmlFileName.rfind('/');
			if(slashPos == string::npos) slashPos = 0;
			else slashPos++;
			string imageBase = xmlFileName.substr(0, slashPos);

			cIndex = 0;
			while(xml.tagExists("image", cIndex)){
				cValue = xml.getValue("image", "", cIndex);
				cName = xml.getAttribute("image", "name", "", cIndex);
				stringstream imageName;
				imageName << imageBase << cValue;
				this->addImageNode(imageName.str(), cName);
				cIndex++;
			}
			
			xml.popTag(); // entry
			result = true;
		}
	}
	
	if(result){
		this->xmlFileName = xmlFileName;
		this->saved = true;
		this->cached = cached;
	}
	
	return result;
}

bool ofxTI_HubEntry::saveXml(string xmlFileName){
	bool result = false;
	ofxXmlSettings xml;
	
	if(xmlFileName == "" && this->xmlFileName != "")
		xmlFileName = this->xmlFileName;
	
	if(xmlFileName == "" && this->hub != NULL)
		xmlFileName = this->hub->getNewEntryName();
	
	if(xmlFileName != ""){
		xml.addTag("entry");
		xml.pushTag("entry");
		
		if(this->hub != NULL)
			xml.addValue("installation_id", this->hub->getInstallationID());
		
		stringstream serviceStr;
		for(int i=0; i < this->services.size(); i++)
			serviceStr << ((i > 0)?", ":"") << this->services[i];
		if(serviceStr.str() != ""){
			int nodeIndex = xml.addValue("text", serviceStr.str());
			xml.addAttribute("text", "name", "services", nodeIndex);
		}
		
		for(int i=0; i < this->nodes.size(); i++){
			string nodeType = (this->nodes[i].isImage?"image":"text");
			string nodeValue = this->nodes[i].value;
			int slashPos = nodeValue.rfind('/');
			if(slashPos != string::npos) nodeValue = nodeValue.substr(slashPos+1);
			int nodeIndex = xml.addValue(nodeType, nodeValue);
			if(this->nodes[i].name != "")
				xml.addAttribute(nodeType, "name", this->nodes[i].name, nodeIndex);
		}
		
		xml.popTag(); // entry
		
		xml.saveFile(xmlFileName);
		result = true;
		
		if(result){
			this->xmlFileName = xmlFileName;
			this->saved = true;
		}
	}
	
	return result;
}

bool ofxTI_HubEntry::copyFileToPath(string sourcePath, string targetPath){
	int slashPos = sourcePath.rfind('/');
	if(slashPos == string::npos)
		slashPos = 0;
	else
		slashPos++;
	
	stringstream targetName;
	targetName << targetPath;
	if(targetPath.length() > 0 && targetPath.at(targetPath.length()-1) != '/')
		targetName << "/";
	targetName << sourcePath.substr(slashPos);
	
	return tiCopyFile(sourcePath, targetName.str());	
}

bool ofxTI_HubEntry::copyToPath(string targetPath){
	bool result = true;
	if(tiDirectoryExists(targetPath)){
		if(!this->saved)
			this->saveXml();
		
		if(this->saved){
			if(!this->copyFileToPath(this->xmlFileName, targetPath))
				result = false;
			for(int i=0; i < this->nodes.size(); i++){
				if(this->nodes[i].isImage){
					if(!this->copyFileToPath(this->nodes[i].value, targetPath))
						result = false;
				}
			}
		}
	}
	return result;
}

bool ofxTI_HubEntry::removeFile(string fileName){
	return tiRemoveFile(fileName);
}

bool ofxTI_HubEntry::removeFiles(){
	bool result = true;
	if(!this->removeFile(this->xmlFileName))
		result = false;
	for(int i=0; i < this->nodes.size(); i++){
		if(this->nodes[i].isImage){
			if(!this->removeFile(this->nodes[i].value))
				result = false;
		}
	}
	return result;
}

void ofxTI_HubEntry::setCached(bool cached){
	this->cached = cached;
}

void ofxTI_HubEntry::setHub(ofxTI_Hub* hub){
	this->hub = hub;
}
