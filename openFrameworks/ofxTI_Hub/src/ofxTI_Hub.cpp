/*
 *  ofxTI_Hub.cpp
 *  openFrameworks
 *
 *  Created by Pat Long (plong0) on 11-05-24.
 *  Copyright 2011 Tangible Interaction Inc. All rights reserved.
 *
 */

#include "ofxTI_Hub.h"

ofxTI_Hub::ofxTI_Hub(){
	this->setApiKey();
	this->setInstallationID();
	this->setPostURL();
	
	this->cachePath = "";
	this->lastCacheScan = -1;
	this->setSavePath();
	this->setCacheScanDelay();
	this->setDefaultImageExtension();
	this->setTimeStampMode();
	
	this->setStatus();
	this->setCacheStatus();
	this->setActiveEntry(NULL);
	
	ofAddListener(httpUtils.newResponseEvent,this,&ofxTI_Hub::newResponse);
	httpUtils.setTimeoutSeconds(15);
	httpUtils.start();
	
//	ofxHttpUtil.setTimeoutSeconds(15);
//	ofxHttpEvents.addListener(this);
}

ofxTI_Hub::~ofxTI_Hub(){
}

void ofxTI_Hub::update(){
	if(this->postQueue.size() > 0 && !this->isSending()){
		ofxTI_HubEntry* entry = this->postQueue[0];
		if(this->doPostEntry(entry)){
			this->postQueue.erase(this->postQueue.begin());
		}
	}
	int cTime = ofGetElapsedTimeMillis();
	if(this->lastCacheScan == -1 || (cTime-this->lastCacheScan) >= this->cacheScanDelay){
		this->processCache();
	}
}

string ofxTI_Hub::getApiKey(){
	return this->apiKey;
}

int ofxTI_Hub::getInstallationID(){
	return this->installationID;
}

string ofxTI_Hub::getNewEntryName(){
	stringstream nameBuilder;
	nameBuilder << this->getSavePath() << "entry_" << getTimeStamp(this->tsMode);
	if(this->tsMode == TS_HUMAN) nameBuilder << "_" << ofGetElapsedTimeMillis();
	nameBuilder << ".xml";
	return nameBuilder.str();
}

string ofxTI_Hub::getNewImageName(string extension){
	if(extension == "")
		extension = this->defaultImageExtension;
	stringstream nameBuilder;
	nameBuilder << this->getSavePath() << "image_" << getTimeStamp(this->tsMode);
	if(this->tsMode == TS_HUMAN) nameBuilder << "_" << ofGetElapsedTimeMillis();
	nameBuilder << "." << extension;
	return nameBuilder.str();
}

string ofxTI_Hub::getSavePath(){
	string savePath = this->savePath;
	if(savePath.length() > 0 && savePath.at(savePath.length()-1) != '/')
		savePath += '/';
	return savePath;
}

ofxTI_HubStatus ofxTI_Hub::getStatus(){
	return this->status;
}

ofxTI_HubStatus ofxTI_Hub::getCacheStatus(){
	return this->cacheStatus;
}

string ofxTI_Hub::getTimeStamp(TimeStampMode tsMode){
	stringstream timeStamp;
	if(tsMode == TS_HUMAN)
		timeStamp << ((ofGetYear() < 10)?"0":"") << ofGetYear() << ((ofGetMonth() < 10)?"0":"") << ofGetMonth() << ((ofGetDay() < 10)?"0":"") << ofGetDay() << "_" << ((ofGetHours() < 10)?"0":"") << ofGetHours() << ((ofGetMinutes() < 10)?"0":"") << ofGetMinutes() << ((ofGetSeconds() < 10)?"0":"") << ofGetSeconds();
	else {
		ofxDateTime cTime;
		timeStamp << cTime.getEpoch();
	}
	
	return timeStamp.str();
}

ofxTI_HubEntry* ofxTI_Hub::getNewEntry(string entryName){
	return new ofxTI_HubEntry(this, entryName);
}

bool ofxTI_Hub::isSending(){
	return (this->status == STATUS_SENDING || this->cacheStatus == STATUS_SENDING);
}

bool ofxTI_Hub::inPostQueue(ofxTI_HubEntry* entry){
	for(int i=0; i < this->postQueue.size(); i++){
		if(postQueue[i] == entry || (entry->getXmlFileName() != "" && postQueue[i]->getXmlFileName() == entry->getXmlFileName()))
		   return true;
	}
	return false;
}

bool ofxTI_Hub::isPosting(ofxTI_HubEntry* entry){
	if(this->activeEntry != NULL && (this->activeEntry == entry || (entry->getXmlFileName() != "" && this->activeEntry->getXmlFileName() == entry->getXmlFileName())))
		return true;
	return false;
}

void ofxTI_Hub::postEntry(ofxTI_HubEntry* entry){
	if(!isPosting(entry) && !inPostQueue(entry))
		this->postQueue.push_back(entry);
}

bool ofxTI_Hub::doPostEntry(ofxTI_HubEntry* entry){
	bool result = false;
	
	if(entry != NULL && this->activeEntry == NULL){
		if(!entry->isSaved())
			entry->saveXml();
		
		ofxHttpForm hubForm;
		hubForm.name = "Hub";
		hubForm.action = this->postURL;
		hubForm.method = OFX_HTTP_POST;
//		hubForm.multipart = true;
		
//		cout << "installation_id:" << ofToString(this->installationID,0) << endl;
//		cout << "api_key:" << this->apiKey << endl;
		
		hubForm.addFormField("installation_id", ofToString(this->installationID,0));
		hubForm.addFormField("api_key", this->apiKey);
		
//		cout << "xmlFile:" << ofToDataPath(entry->getXmlFileName(), true) << endl;
//		hubForm.addFormField("xmlFile", ofToDataPath(entry->getXmlFileName(), true), true);
		hubForm.addFile("xmlFile", ofToDataPath(entry->getXmlFileName(), true));
		
		vector<ofxTI_HubEntryNode> nodes = entry->getNodes();
		int count = 0;
		for(int i=0; i < nodes.size(); i++){
			if(nodes[i].isImage){
				string name = nodes[i].name;
				name = "image_"+ofToString(count++, 0);
				hubForm.addFile(name, ofToDataPath(nodes[i].value, true));
//				hubForm.addFormField(name, ofToDataPath(nodes[i].value, true), true);
			}
		}
		if(!entry->isCached())
			this->setStatus(STATUS_SENDING);
		else
			this->setCacheStatus(STATUS_SENDING);

		this->setActiveEntry(entry);
		httpUtils.addForm(hubForm);
		result = true;
	}
	
	return result;
}

void ofxTI_Hub::newResponse(ofxHttpResponse & response){
	if(response.status == 200 || response.status == 201){
		if(this->activeEntry->isCached()){
			this->setCacheStatus(STATUS_SUCCESS);
			this->activeEntry->removeFiles();
		}
		else
			this->setStatus(STATUS_SUCCESS);
		
		stringstream logBuilder;
		logBuilder << "ofxTI_Hub: successfully posted entry #" << response.responseBody;
		if(this->activeEntry != NULL) logBuilder << " [source:'" << this->activeEntry->getXmlFileName() << "']";
		ofLog(OF_LOG_NOTICE, logBuilder.str());
		logBuilder.str("");

		this->killActiveEntry();
	}
	else{
		string error = ofToString(response.status, 0) + ": "+response.reasonForStatus;
		newError(error);
	}
/**	cout << "\tURL:    " << response.url << endl;
	cout << "\tstatus: " << response.status << endl;
	cout << "\treason: " << response.reasonForStatus << endl;
	cout << "\tcontent:" << endl;
	cout << "\t\t" << response.responseBody << endl << endl;*/
}

void ofxTI_Hub::newError(string & error){
	
	if(!this->activeEntry->isCached()){
		this->setStatus(STATUS_ERROR);
		
		stringstream logBuilder;
		logBuilder << "ofxTI_Hub: posting failed: '" << error << "'";
		if(this->activeEntry != NULL) logBuilder << " [source:'" << this->activeEntry->getXmlFileName() << "']";
		ofLog(OF_LOG_NOTICE, logBuilder.str());
		logBuilder.str("");
		
		this->cacheEntry(this->activeEntry);
	}
	else
		this->setCacheStatus(STATUS_ERROR);
	
	this->killActiveEntry();
}

bool ofxTI_Hub::checkCacheDirectory(){
	stringstream cacheDir;
	cacheDir << this->getSavePath() << "cache/";
	bool result = tiDirectoryExists(cacheDir.str(), true);
	if(result)
		this->cachePath = cacheDir.str();
	cacheDir.str("");
	return result;
}

void ofxTI_Hub::cacheEntry(ofxTI_HubEntry* entry){
	if(entry != NULL){
//		if(this->checkCacheDirectory()){
			entry->copyToPath(this->cachePath);
//		}
	}
}

void ofxTI_Hub::processCache(){
//	if(checkCacheDirectory()){
		ofxDirList cacheList;
		cacheList.allowExt("xml");
		
		int logLevel = currentLogLevel;
		ofSetLogLevel(OF_LOG_ERROR);
		int numCached = cacheList.listDir(this->cachePath);
		ofSetLogLevel(logLevel);
		
		if(numCached > 0){
			ofxTI_HubEntry* entry = this->getNewEntry(cacheList.getPath(0));
			entry->setCached(true);
			this->postEntry(entry);
		}
		else
			this->setCacheStatus(STATUS_IDLE);
		
		this->lastCacheScan = ofGetElapsedTimeMillis();
//	}
}

void ofxTI_Hub::killActiveEntry(){
	if(this->activeEntry != NULL){
		delete this->activeEntry;
		this->activeEntry = NULL;
	}
}

bool ofxTI_Hub::loadConfiguration(string fromFile){
	bool result = false;
	ofxXmlSettings xml;
	if(xml.loadFile(fromFile)){
		int installationID = -1;
		string apiKey = "";
		string uploadURL = "";
		string savePath = "";
		
		if(xml.tagExists("Hub")){
			xml.pushTag("Hub");
			installationID = xml.getValue("installation_id", installationID);
			apiKey = xml.getValue("api_key", apiKey);
			uploadURL = xml.getValue("upload_url", uploadURL);
			savePath = xml.getValue("save_path", savePath);
			this->setCacheScanDelay(xml.getValue("cache_scan_delay", this->cacheScanDelay));
			xml.popTag(); // Hub
		}
		
		if(installationID >= 0 && apiKey != "" && uploadURL != ""){
			this->setInstallationID(installationID);
			this->setApiKey(apiKey);
			this->setPostURL(uploadURL);
			this->setSavePath(savePath);
			result = true;
		}
	}
	return result;
}

void ofxTI_Hub::setActiveEntry(ofxTI_HubEntry* activeEntry){
	this->activeEntry = activeEntry;
}

void ofxTI_Hub::setApiKey(string apiKey){
	this->apiKey = apiKey;
}

void ofxTI_Hub::setInstallationID(int installationID){
	this->installationID = installationID;
}

void ofxTI_Hub::setPostURL(string postURL){
	this->postURL = postURL;
}

void ofxTI_Hub::setSavePath(string savePath){
	if(tiDirectoryExists(ofToDataPath(savePath, true), true)){
		this->savePath = savePath;
		this->cachePath = "";
		this->checkCacheDirectory();
	}
}

void ofxTI_Hub::setCacheScanDelay(int cacheScanDelay){
	if(cacheScanDelay < OFX_TI_HUB_CACHE_SCAN_DELAY_MIN)
		cacheScanDelay = OFX_TI_HUB_CACHE_SCAN_DELAY_MIN;
	this->cacheScanDelay = cacheScanDelay;
}
	
void ofxTI_Hub::setDefaultImageExtension(string defaultImageExtension){
	if(defaultImageExtension == "")
		defaultImageExtension = OFX_TI_HUB_DEFAULT_IMAGE_EXTENSION;
	this->defaultImageExtension = defaultImageExtension;
}

void ofxTI_Hub::setStatus(ofxTI_HubStatus status){
	this->status = status;
}

void ofxTI_Hub::setCacheStatus(ofxTI_HubStatus cacheStatus){
	this->cacheStatus = cacheStatus;
}

void ofxTI_Hub::setTimeStampMode(TimeStampMode tsMode){
	this->tsMode = tsMode;
}
