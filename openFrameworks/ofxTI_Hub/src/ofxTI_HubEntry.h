/*
 *  ofxTI_HubEntry.h
 *  openFrameworks
 *
 *  Created by Pat Long (plong0) on 11-05-24.
 *  Copyright 2011 Tangible Interaction Inc. All rights reserved.
 *
 */
#ifndef _OFX_TI_HUB_ENTRY
#define _OFX_TI_HUB_ENTRY

#include "ofMain.h"
#include "ofxImage.h"
#include "ofxXmlSettings.h"

class ofxTI_Hub;

struct ofxTI_HubEntryNode{
	string name;
	string value;
	bool isImage;
	
	ofxTI_HubEntryNode(){
		name = "";
		value = "";
		isImage = false;
	}
};

class ofxTI_HubEntry{
protected:
	ofxTI_Hub* hub;
	vector<ofxTI_HubEntryNode> nodes;
	vector<string> services;
	string xmlFileName;
	bool saved;
	bool cached;
	
	bool copyFileToPath(string sourcePath, string targetPath);
	bool removeFile(string fileName);
	
public:
	ofxTI_HubEntry(ofxTI_Hub* hub=NULL, string entryName="");
	~ofxTI_HubEntry();
	
	ofxTI_Hub* getHub();
	
	string getXmlFileName();
	vector<ofxTI_HubEntryNode> getNodes();
	
	bool isCached();
	bool isSaved();
	
	void addTextNode(string value, string name="");
	void addImageNode(string fileName, string name="");
	void addImageNode(ofxImage image, string name="");
	void addService(string service);
	
	bool loadXml(string xmlFileName, bool cached=false);
	bool saveXml(string xmlFileName="");
	bool copyToPath(string targetPath);
	bool removeFiles();
	
	void setCached(bool cached);
	void setHub(ofxTI_Hub* hub=NULL);
	
};

#include "ofxTI_Hub.h"

#endif
