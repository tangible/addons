/*
 *  ofxKinectFrameDiff.h
 *  VoiceIt
 *
 *  Created by Pat Long (plong0) on 11-02-16.
 *  Copyright 2011 Tangible Interaction. All rights reserved.
 *
 */
#pragma once

#include "ofxKinect.h"
#include "ofxKinectCalibrator.h"
#include "ofxOpenCv.h"

class ofxKinectFrameDiff {
	protected:
		ofxKinect*				kinect;
		ofxKinectCalibrator*	kinectCalibrator;
	
		ofxCvColorImage			colorImg;
		ofxCvGrayscaleImage 	grayDepth;
		ofxCvGrayscaleImage 	grayImage;
		ofxCvGrayscaleImage 	grayThresh;
		ofxCvGrayscaleImage 	grayThreshFar;
	
		bool					bThreshWithOpenCV;
		int						nearThreshold;
		int						farThreshold;
		
		bool	mirrorHorizontal;
		bool	mirrorVertical;
	
		bool	bHasFrame;
	
		virtual void initSettings(int nearThreshold=0, int farThreshold=180, bool bThreshWithOpenCV=true);
		
	public:
		ofxKinectFrameDiff();
		ofxKinectFrameDiff(ofxKinect* kinect);
		ofxKinectFrameDiff(ofxKinectCalibrator* kinectCalibrator);
		~ofxKinectFrameDiff();
	
		virtual void init(ofxKinect* kinect, int nearThreshold=0, int farThreshold=180, bool bThreshWithOpenCV=true);
		virtual void init(ofxKinectCalibrator* kinectCalibrator, int nearThreshold=0, int farThreshold=180, bool bThreshWithOpenCV=true);
		virtual void update();
		virtual void draw();
		void drawColor(float x, float y, float w, float h);
		void drawDepth(float x, float y, float w, float h);
	
		bool hasFrame();
	
		ofxCvGrayscaleImage* getGrayImage();
	
		void setMirror(bool mirrorHorizontal=false, bool mirrorVertical=false);
		void setThreshold(int nearThreshold, int farThreshold);
		void setThresholdFar(int farThreshold);
		void setThresholdNear(int nearThreshold);
		void setThresholdWithOpenCV(bool bThreshWithOpenCV);
};
