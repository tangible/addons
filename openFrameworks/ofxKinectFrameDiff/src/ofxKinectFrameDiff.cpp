/*
 *  ofxKinectFrameDiff.cpp
 *  VoiceIt
 *
 *  Created by Pat Long (plong0) on 11-02-16.
 *  Copyright 2011 Tangible Interaction. All rights reserved.
 *
 */

#include "ofxKinectFrameDiff.h"

ofxKinectFrameDiff::ofxKinectFrameDiff(){
	this->kinect = NULL;
	this->kinectCalibrator = NULL;
	this->bHasFrame = false;
	this->initSettings();
}

ofxKinectFrameDiff::ofxKinectFrameDiff(ofxKinect* kinect){
	this->init(kinect);
}

ofxKinectFrameDiff::ofxKinectFrameDiff(ofxKinectCalibrator* kinectCalibrator){
	this->init(kinectCalibrator);
}

ofxKinectFrameDiff::~ofxKinectFrameDiff(){
}

void ofxKinectFrameDiff::init(ofxKinect* kinect, int nearThreshold, int farThreshold, bool bThreshWithOpenCV){
	this->kinect = kinect;
	this->kinectCalibrator = NULL;
	this->bHasFrame = false;
	this->initSettings(nearThreshold, farThreshold, bThreshWithOpenCV);
}

void ofxKinectFrameDiff::init(ofxKinectCalibrator* kinectCalibrator, int nearThreshold, int farThreshold, bool bThreshWithOpenCV){
	this->kinect = NULL;
	this->kinectCalibrator = kinectCalibrator;
	this->bHasFrame = false;
	this->initSettings(nearThreshold, farThreshold, bThreshWithOpenCV);
}

void ofxKinectFrameDiff::initSettings(int nearThreshold, int farThreshold, bool bThreshWithOpenCV){
	if(kinectCalibrator != NULL){
		colorImg.allocate(kinectCalibrator->getCalibratedWidth(), kinectCalibrator->getCalibratedHeight());
		grayDepth.allocate(kinectCalibrator->getCalibratedWidth(), kinectCalibrator->getCalibratedHeight());
		grayImage.allocate(kinectCalibrator->getCalibratedWidth(), kinectCalibrator->getCalibratedHeight());
		grayThresh.allocate(kinectCalibrator->getCalibratedWidth(), kinectCalibrator->getCalibratedHeight());
		grayThreshFar.allocate(kinectCalibrator->getCalibratedWidth(), kinectCalibrator->getCalibratedHeight());
	}
	else if(kinect != NULL){
		colorImg.allocate(kinect->getWidth(), kinect->getHeight());
		grayDepth.allocate(kinect->getWidth(), kinect->getHeight());
		grayImage.allocate(kinect->getWidth(), kinect->getHeight());
		grayThresh.allocate(kinect->getWidth(), kinect->getHeight());
		grayThreshFar.allocate(kinect->getWidth(), kinect->getHeight());
	}
	
	this->nearThreshold = nearThreshold;
	this->farThreshold  = farThreshold;
	this->bThreshWithOpenCV = bThreshWithOpenCV;
	
	this->setMirror();
}

void ofxKinectFrameDiff::update(){
	bool hasNewFrame = false;
	
	if(kinectCalibrator != NULL){
		if(kinectCalibrator->isFrameNew()){
			if(!this->bHasFrame)
				this->bHasFrame = true;
			
			if(grayDepth.getWidth() != kinectCalibrator->getCalibratedWidth() || grayDepth.getHeight() != kinectCalibrator->getCalibratedHeight())
				grayDepth.resize(kinectCalibrator->getCalibratedWidth(), kinectCalibrator->getCalibratedHeight());
			grayDepth.setFromPixels(kinectCalibrator->getCalibratedDepthPixels(), kinectCalibrator->getCalibratedWidth(), kinectCalibrator->getCalibratedHeight());
			grayDepth.mirror(mirrorVertical, mirrorHorizontal);
			
			if(grayImage.getWidth() != kinectCalibrator->getCalibratedWidth() || grayImage.getHeight() != kinectCalibrator->getCalibratedHeight())
				grayImage.resize(kinectCalibrator->getCalibratedWidth(), kinectCalibrator->getCalibratedHeight());
			grayImage.setFromPixels(kinectCalibrator->getCalibratedDepthPixels(), kinectCalibrator->getCalibratedWidth(), kinectCalibrator->getCalibratedHeight());
			grayImage.mirror(mirrorVertical, mirrorHorizontal);
			
			if(colorImg.getWidth() != kinectCalibrator->getCalibratedWidth() || colorImg.getHeight() != kinectCalibrator->getCalibratedHeight())
				colorImg.resize(kinectCalibrator->getCalibratedWidth(), kinectCalibrator->getCalibratedHeight());
			colorImg.setFromPixels(kinectCalibrator->getCalibratedPixels(), kinectCalibrator->getCalibratedWidth(), kinectCalibrator->getCalibratedHeight());
			colorImg.mirror(mirrorVertical, mirrorHorizontal);
			
			if(grayThresh.getWidth() != kinectCalibrator->getCalibratedWidth() || grayThresh.getHeight() != kinectCalibrator->getCalibratedHeight())
				grayThresh.resize(kinectCalibrator->getCalibratedWidth(), kinectCalibrator->getCalibratedHeight());
			
			if(grayThreshFar.getWidth() != kinectCalibrator->getCalibratedWidth() || grayThreshFar.getHeight() != kinectCalibrator->getCalibratedHeight())
				grayThreshFar.resize(kinectCalibrator->getCalibratedWidth(), kinectCalibrator->getCalibratedHeight());
			
			hasNewFrame = true;
		}
	}
	else if(kinect != NULL){
		if(kinect->isFrameNew()){
			if(!this->bHasFrame)
				this->bHasFrame = true;
			
			if(grayDepth.getWidth() != kinect->getWidth() || grayDepth.getHeight() != kinect->getHeight())
				grayDepth.resize(kinect->getWidth(), kinect->getHeight());
			grayDepth.setFromPixels(kinect->getDepthPixels(), kinect->getWidth(), kinect->getHeight());
			grayDepth.mirror(mirrorVertical, mirrorHorizontal);
			
			if(grayImage.getWidth() != kinect->getWidth() || grayImage.getHeight() != kinect->getHeight())
				grayImage.resize(kinect->getWidth(), kinect->getHeight());
			grayImage.setFromPixels(kinect->getDepthPixels(), kinect->getWidth(), kinect->getHeight());
			grayImage.mirror(mirrorVertical, mirrorHorizontal);
			
			if(colorImg.getWidth() != kinect->getWidth() || colorImg.getHeight() != kinect->getHeight())
				colorImg.resize(kinect->getWidth(), kinect->getHeight());
			colorImg.setFromPixels(kinect->getPixels(), kinect->width, kinect->height);
			colorImg.mirror(mirrorVertical, mirrorHorizontal);
			
			if(grayThresh.getWidth() != kinect->getWidth() || grayThresh.getHeight() != kinect->getHeight())
				grayThresh.resize(kinect->getWidth(), kinect->getHeight());
			
			if(grayThreshFar.getWidth() != kinect->getWidth() || grayThreshFar.getHeight() != kinect->getHeight())
				grayThreshFar.resize(kinect->getWidth(), kinect->getHeight());
			
			hasNewFrame = true;
		}
	}
	
	if(hasNewFrame){
		//we do two thresholds - one for the far plane and one for the near plane
		//we then do a cvAnd to get the pixels which are a union of the two thresholds.	
		if( bThreshWithOpenCV ){
			grayThreshFar = grayImage;
			grayThresh = grayImage;
			grayThreshFar.threshold(farThreshold, true);
			grayThresh.threshold(nearThreshold);
			cvAnd(grayThresh.getCvImage(), grayThreshFar.getCvImage(), grayImage.getCvImage(), NULL);
		}else{
			//or we do it ourselves - show people how they can work with the pixels
			
			unsigned char * pix = grayImage.getPixels();
			int numPixels = grayImage.getWidth() * grayImage.getHeight();
			
			for(int i = 0; i < numPixels; i++){
				if( pix[i] > nearThreshold && pix[i] < farThreshold ){
					pix[i] = 255;
				}else{
					pix[i] = 0;
				}
			}
		}
		
		grayImage.blurHeavily();
		
		//update the cv image
		grayImage.flagImageChanged();
	}
}

void ofxKinectFrameDiff::draw(){
	
}

void ofxKinectFrameDiff::drawColor(float x, float y, float w, float h){
	colorImg.draw(x, y, w, h);
}

void ofxKinectFrameDiff::drawDepth(float x, float y, float w, float h){
	grayDepth.draw(x, y, w, h);
}

bool ofxKinectFrameDiff::hasFrame(){
	return this->bHasFrame;
}

ofxCvGrayscaleImage* ofxKinectFrameDiff::getGrayImage(){
	return &grayImage;
}

void ofxKinectFrameDiff::setMirror(bool mirrorHorizontal, bool mirrorVertical){
	this->mirrorHorizontal = mirrorHorizontal;
	this->mirrorVertical = mirrorVertical;
}

void ofxKinectFrameDiff::setThreshold(int nearThreshold, int farThreshold){
	this->setThresholdNear(nearThreshold);
	this->setThresholdFar(farThreshold);
}

void ofxKinectFrameDiff::setThresholdFar(int farThreshold){
	this->farThreshold  = farThreshold;
}

void ofxKinectFrameDiff::setThresholdNear(int nearThreshold){
	this->nearThreshold = nearThreshold;
}

void ofxKinectFrameDiff::setThresholdWithOpenCV(bool bThreshWithOpenCV){
	this->bThreshWithOpenCV = bThreshWithOpenCV;
}

